<?php

  /*
  *
  *	Template Name: 404
  *	Filename: 404.php
  *
  */

  get_header();

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "error-404";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- Content (ACF)
  $error_404 = get_field("error_404", "options") ?: [];
  $cols = $error_404["cols"] ?? "col-12 col-lg-8 offset-lg-2";
  $container = $error_404["container"] ?? "container";
  $heading = $error_404["heading_content"] ?? "Error 404";
  $heading_size = $error_404["heading_size"] ?? "lg";
  $heading_style = $error_404["heading_style"] ?? "primary";
  $message = $error_404["message_content"] ?? "<p>Page or Content Not Found.</p>";
  $message_size = $error_404["message_size"] ?? "sm";
  $message_style = $error_404["message_style"] ?? "primary";

?>

<section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
	<div class="<?= $block_name; ?>__main">
		<?= $THEME->render_bs_container( "open", $cols, $container ); ?>
      <div class="<?= $block_name; ?>__content">
			  <h1 class="<?= $block_name; ?>__heading heading--<?= $heading_style; ?> heading--<?= $heading_size; ?>"><?= $heading; ?></h1>
        <div class="<?= $block_name; ?>__message body-copy--<?= $message_style; ?> body-copy--<?= $message_size; ?>"><?= $message; ?></div>
      </div>
		<?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
	</div>
</section>

<?php get_footer(); ?>
