<?php

/*
*
*	Custom WYSIWYG Styles
*
*/

//////////////////////////////////////////////////////////
////  Add Style Select Buttons
//////////////////////////////////////////////////////////

function add_style_select_buttons ( $buttons ) {

    array_unshift( $buttons, 'styleselect' );

    return $buttons;

}

// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//////////////////////////////////////////////////////////
////  Add Custom Styles
//////////////////////////////////////////////////////////

function custom_format_settings( $title = '', $classes = '', $styles = [] ) {
  return [
    'title' => $title,
    'inline' => 'span',
    'classes' => $classes,
    "wrapper" => true,
    "styles" => $styles,
  ];
}

function my_custom_styles( $init_array ) {

    $style_formats = [
      custom_format_settings( "BC Eric Machat Script", "font-family--bc-eric-machat-script", [ 'fontFamily' => "bc-eric-machat-script, sans-serif" ] ),
      custom_format_settings( "Neue Haas Grotesk Display", "font-family--neue-haas-grotesk-display", [ 'fontFamily' => "neue-haas-grotesk-display, sans-serif" ] ),
      custom_format_settings( "Not Sorry Serif", "font-family--not-sorry-serif", [ 'fontFamily' => "'Not Sorry Serif', serif" ] ),
      custom_format_settings( "Sorry Sans", "font-family--sorry-sans", [ 'fontFamily' => "'Sorry Sans', sans-serif" ] ),
      custom_format_settings( 'Font Size XL', 'font-size--xl', [ 'fontSize' => '160%' ] ),
      custom_format_settings( 'Font Size LG', 'font-size--lg', [ 'fontSize' => '140%' ] ),
      custom_format_settings( 'Font Size MD', 'font-size--md', [ 'fontSize' => '120%' ] ),
      custom_format_settings( 'Line Height 150', '', [ 'display' => 'block', 'lineHeight' => '1.5' ] ),
      custom_format_settings( 'Line Height 145', '', [ 'display' => 'block', 'lineHeight' => '1.45' ] ),
      custom_format_settings( 'Line Height 140', '', [ 'display' => 'block', 'lineHeight' => '1.4' ] ),
      custom_format_settings( 'Line Height 135', '', [ 'display' => 'block', 'lineHeight' => '1.35' ] ),
      custom_format_settings( 'Line Height 130', '', [ 'display' => 'block', 'lineHeight' => '1.3' ] ),
      custom_format_settings( 'Line Height 125', '', [ 'display' => 'block', 'lineHeight' => '1.25' ] ),
      custom_format_settings( 'Line Height 120', '', [ 'display' => 'block', 'lineHeight' => '1.2' ] ),
      custom_format_settings( 'Line Height 115', '', [ 'display' => 'block', 'lineHeight' => '1.15' ] ),
      custom_format_settings( 'Line Height 110', '', [ 'display' => 'block', 'lineHeight' => '1.1' ] ),
      custom_format_settings( 'Line Height 105', '', [ 'display' => 'block', 'lineHeight' => '1.05' ] ),
      custom_format_settings( 'Line Height 100', '', [ 'display' => 'block', 'lineHeight' => '1.0' ] ),
      custom_format_settings( 'Non Bulleted List', 'non-bulleted-list' ),
      custom_format_settings( 'Uppercase', 'text--uppercase', [ 'textTransform' => 'uppercase' ] ),
    ];

    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );
