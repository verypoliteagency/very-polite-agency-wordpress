<?php

class CustomThemeBase {

  /*
  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////
  */

  private $name = 'Custom Theme Base';
  private $version = '2.0';

  public $custom_image_title = 'custom-image-size';
  public $custom_image_sizes = [ 1, 10, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

  /*
  //////////////////////////////////////////////////////////
  ////  Methods
  //////////////////////////////////////////////////////////
  */

  // ---------------------------------------- Featured Image
  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  // ---------------------------------------- Get Link
  public function get_link( $params = [] ) {

    extract(array_merge(
      [
        'link_anchor' => '',
        'link_category' => '',
        'link_external' => '',
        'link_page' => '',
        'type' => '',
      ],
      $params
    ));

    switch( $type ) {
      case 'anchor': {
        $link = $link_anchor ?: '';
        break;
      }
      case 'category': {
        $link = get_category_link( $link_category ) ?: '';
        break;
      }
      case 'external': {
        $link = $link_external ?: '';
        break;
      }
      case 'page': {
        $link = get_permalink( $link_page ) ?: '';
        break;
      }
      default: {
        $link = '';
        break;
      }
    }

    return $link;

  }

  // ---------------------------------------- Google Maps Directions Link
  public function get_google_maps_directions_link( $params = [] ) {

    $html = '';
    $base = 'https://www.google.com/maps/search/?api=1&query=';

    if ( $params ) {

      // default data
      $name = $city = $country = $region = $postal = $address = $address_2 = false;

      // extract $params
      extract( $params );

      $html .= ( $address ) ? $address : '';
      $html .= ( $city ) ? ' ' . $city : '';
      $html .= ( $region ) ? ' ' . $region : '';
      $html .= ( $postal ) ? ' ' . $postal : '';
      $html .= ( $country ) ? ' ' . $country : '';
      $html .= ( $name ) ? ' ' . $name : '';

      if ( $address_2 ) {
        $html = $address_2 . '–' . $html;
      }

      if ( $html ) {
        $html = $base . trim($html);
      }

    }

    return $html;

  }

  // ---------------------------------------- Theme Classes
  public function get_theme_classes() {

    global $post;
    global $template;

    $classes = "";

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$template = basename( $template, ".php" );

  		$classes .= "post-type--{$post_type}";
  		$classes .= " {$post_type}--{$post_slug}";
  		$classes .= " page-id--{$post_ID}";
  		$classes .= " template--{$template}";
  		$classes .= " {$template}";

      $classes .= is_front_page() ? " is-front-page" : "";
      $classes .= is_page() ? " is-page" : "";
      $classes .= is_single() ? " is-single" : "";
      $classes .= is_archive() ? " is-archive" : "";
      $classes .= is_category() ? " is-category" : "";
      $classes .= is_singular([ 'post' ]) || is_page_template( 'page-templates/page--longevity-club.php' ) ? " longevity-club" : "";

      $classes .= Tools::is_development() ? " is-development" : " is-production";

  	}

    $classes .= is_404() ? " is-404" : "";

  	return esc_attr(trim($classes));

  }


  // ---------------------------------------- Theme Directory
  public function get_theme_directory( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  // ---------------------------------------- Theme Info
  public function get_theme_info( $param = 'version' ) {

    global $post;
    $html = '';

    switch ( $param ) {
      case 'is_front_page':
        return is_front_page();
        break;
      case 'is_single':
        return is_single();
        break;
      case 'object_ID':
				$html = get_queried_object_id();
				break;
      case 'php_version':
				$html = phpversion();
				break;
      case 'post_ID':
				$html = ( $post ) ? $post->ID : 'no-post-id';
				break;
			case 'post_type':
				$html = get_post_type( $post->ID );
				break;
      case 'template':
				$html = basename( get_page_template(), ".php" );
				break;
      case 'version':
				$html = $this->version;
				break;
      case 'vitals':
        $html .= "<!-- PHP Version : " . $this->get_theme_info( 'php_version' ) . " -->";
			  $html .= "<!-- WP Version : " . $this->get_theme_info( 'wp_version' ) . " -->";
			  $html .= "<!-- Current Template : " . $this->get_theme_info( 'template' ) . " -->";
			  $html .= "<!-- Post ID : " . $this->get_theme_info( 'post_ID' ) . " -->";
        break;
      case 'wp_version':
				$html = get_bloginfo( "version" );
        break;
    }

    return $html;

  }

  // --------------------------- Get Unique ID
  public function get_unique_id( $prefix = "id--" ) {
    return trim( $prefix . md5(uniqid(rand(), true)) );
  }

  // --------------------------- Anchor
  public function render_anchor( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [ 'block_name' => '', 'id' => '' ],
      $params
    ));

    return $block_name && $id ? "<div class='{$block_name}__anchor anchor' id='{$id}'></div>" : "";

  }

  // --------------------------- AOS Attributes
  public function render_aos_attrs( $params = [] ) {

    /*
    *  Note:
    *  AOS library (https://www.npmjs.com/package/aos) needs to be installed
    *  and initialized. JS and CSS files are required for anything to happen.
    *
    *  Timing:
    *  Both delay and duration must be increments of 50
    *
    *  Bugs:
    *  Offset and Mirror are buggy. Disable them for now.
    */

    // ---------------------------------------- Defaults
    $settings = array_merge([
        "anchor" => "",                           // element id, without hash
        "anchor_placement" => "top-bottom",
        "delay" => 0,
        "duration" => 650,
        "easing" => "ease-in-out",
        // "mirror" => "false",                   // DEFINITELY buggy, temporarily hide for now
        "offset" => 175,
        "once" => "true",
        "transition" => "fade-in",
      ], $params
    );

    $html = "";

    foreach ( $settings as $key => $value ) {
      $value = esc_attr( $value );
      switch ( $key ) {
        case "anchor": {
          $html .= " data-aos-anchor='#{$value}'";
          break;
        }
        case "anchor_placement": {
          $html .= " data-aos-anchor-placement='{$value}'";
          break;
        }
        case "transition": {
          $html .= " data-aos='{$value}'";
          break;
        }
        default:
          $html .= " data-aos-{$key}='{$value}'";
      }
    }

    return $html;

  }

  // --------------------------- Container
  public function render_bs_container( $state = 'open', $col = 'col-12', $container = 'container' ) {
    if ( "full-width" !== $container ) {
      return "open" !== $state ? "</div></div></div>" : "<div class='{$container}'><div class='row'><div class='{$col}'>";
    }
    return;
  }

  // --------------------------- Call to Action
  public function render_cta( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'cta',
        'classes' => '',
        'html' => '',
        'link' => '',
        'link_block_name' => 'link',
        'link_category' => '',
        'link_classes' => '',
        'link_external' => '',
        'link_page' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
        'type' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ?: $block_name;

    switch( $type ) {
      case 'category': {
        $link = get_category_link( $link_category ) ?: '';
        break;
      }
      case 'external': {
        if ( $link_external ) {
          $link = $link_external;
          $rel = 'noopener';
          $target = '_blank';
        }
        break;
      }
      case 'page': {
        $link = get_permalink( $link_page ) ?: '';
        break;
      }
    }

    if ( $title ) {
      $html .= '<div class="' . $block_classes . '">';
        $html .= $this->render_link([
          'block_name' => $link_block_name,
          'classes' => $link_classes,
          'link' => $link,
          'title' => $title,
          'style' => $style,
          'target' => $target,
          'rel' => $rel
        ]);
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Element Styles
  public function render_element_styles( $params = [] ) {

    extract(array_merge(
      [
        'background' => '',
        'id' => '',
        'padding_bottom' => 0,
        'padding_top' => 0,
        'text_colour' => '',
      ],
      $params
    ));

    $background = $background ?: 'white';
    $background = ( 'none' === $background ) ? "" : "background: var(--theme-colour--{$background});";
    $text_colour = $text_colour ?: 'black';

    if ( $id ) {
      return "
        #{$id} {
          color: var(--theme-colour--{$text_colour});
          {$background}
          padding-top: calc({$padding_top}px * 0.75);
          padding-bottom: calc({$padding_bottom}px  * 0.75);
        }
        @media screen and (min-width: 992px) {
          #{$id} {
            padding-top: {$padding_top}px;
            padding-bottom: {$padding_bottom}px;
          }
        }
      ";
    }

  }

  // ---------------------------------------- Form Input
  public function render_form_input( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'classes' => '',
        'hidden' => false,
        'name' => '',
        'placeholder' => '',
        'readonly' => false,
        'required' => false,
        'type' => 'text',
        'value' => '',
      ],
      $params
    ));

    if ( $name ) {

      $classes .= $hidden ? ' d-none' : '';
      $classes .= $required ? ' required' : '';

      $html = '<input
        ' . ( $classes ? 'class="' . $classes . '"' : '' ) . '
        type="' . $type . '"
        name="' . $name . '"
        autocomplete="' . $type . '"
        ' . ( $value ? 'value="' . $value . '"' : '' ) . '
        ' . ( $placeholder ? 'placeholder="' . $placeholder . '"' : '' ) . '
        ' . ( $readonly ? 'readonly' : '' ) . '
        ' . ( $type === 'email' ? 'spellcheck="false" autocapitalize="off"' : '' ) . '
        ' . ( $type === 'tel' ? 'pattern="[0-9\-]*"' : '' ) . '
      >';

    }

    return $html;

  }

  // ---------------------------------------- Form Label
  public function render_form_label( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'classes' => '',
        'value' => '',
      ],
      $params
    ));

    return $value ? '<label ' . ( $classes ? 'class="' . $classes . '"' : '' ) . '>' . $value . '</label>' : '';

  }

  // ---------------------------------------- Lazyload Embed
  public function render_lazyload_embed( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'embed',
        'classes' => '',
        'controls' => false,
        'delay' => 0,
        'duration' => 750,
        'html' => '',
        'id' => '', // 160730254, 163590531, 221632885
        'preload' => false,
        'source' => 'vimeo',
      ],
      $params
    ));

    $block_classes = "{$block_name} lazyload lazyload-item lazyload-item--{$block_name}";
    $block_classes .= $preload ? " lazypreload" : "";
    $block_classes = $classes ? "{$classes} {$block_classes}" : $block_classes;

    switch ( $source ) {
      case 'vimeo': {
        $embed_source = 'https://player.vimeo.com/video/' . $id;
        $embed_source .= $controls ? '?controls=1&keyboard=1&background=0&loop=0' : '?controls=0&keyboard=0&background=1&loop=1';
        break;
      }
      case 'youtube': {
        $embed_source = 'https://www.youtube.com/embed/' . $id;
        $embed_source .= $controls ? '?controls=1&disablekb=0&loop=0' : '?controls=0&disablekb=1&loop=1';
        break;
      }
      default: {
        $embed_source = '';
        break;
      }
    }

    if ( $embed_source && $id ) {
      $html = "
        <iframe
          class='{$block_classes}'
          data-src='{$embed_source}'
          data-transition-delay='{$delay}'
          data-transition-duration='{$duration}'
          frameborder='0'
          allow='autoplay; encrypted-media'
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ";
    }

    return $html;

  }

  // ---------------------------------------- Lazyload iframe
  public function render_lazyload_iframe( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'classes' => '',
        'controls' => true,
        'delay' => 0,
        'duration' => 300,
        'html' => '',
        'id' => '', // 160730254, 163590531, 221632885
        'type' => '', // vimeo, youtube
      ],
      $params
    ));

    $base_classes = 'lazyload lazyload-item lazyload-item--iframe lazypreload';
    $classes = ( $classes ?? '' ) . ' ' . $base_classes;
    $embedded_src = ( 'vimeo' == $type ? 'https://player.vimeo.com/video/' : 'https://www.youtube.com/embed/' ) . $id;
    $embedded_src .= $controls ? '?autoplay=0&loop=1&autopause=0&muted=0&background=0' : '?autoplay=1&loop=1&autopause=0&muted=1&background=1';

    if ( $embedded_src && $id ) {
      $html = '
        <iframe
          class="' . trim($base_classes) . '"
          data-src="' . $embedded_src . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          frameborder="0"
          allow="autoplay; encrypted-media"
          webkitallowfullscreen mozallowfullscreen allowfullscreen
        ></iframe>
      ';
    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image Legacy
  public function render_lazyload_image_legacy( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'alt_text' => '',
        'classes' => '',
        'custom_sizes_title' => $this->custom_image_title,
        'custom_sizes' => $this->custom_image_sizes,
        'delay' => 0,
        'duration' => 300,
        'html' => '',
        'image' => [],
      ],
      $params
    ));

    $base_classes = 'lazyload lazyload-item lazyload-item--image lazypreload';
    $classes = ( $classes ?? '' ) . ' ' . $base_classes;
    $img_attrs = '';
    $img_srcset = '';

    if ( !empty($image) ) {

      $img_type = $image['subtype'] ?? 'no-set';
      $img_src = $image['url'] ?? '';

      $img_attrs .= 'class="' . trim($classes) . '"';
      $img_attrs .= $image['width'] ? ' width="' . $image['width'] . '"' : '';
      $img_attrs .= $image['height'] ? ' height="' . $image['height'] . '"' : '';
      $img_attrs .= ' alt="' . ( $alt_text ?: ( $image['alt'] ?? get_bloginfo('name') . ' Photography' ) ) . '"';
      $img_attrs .= $img_src ? ' data-src="' . $img_src . '"' : '';
      $img_attrs .= ' data-transition-delay="' . $delay . '"';
      $img_attrs .= ' data-transition-duration="' . $duration . '"';

      switch ( $img_type ) {
        case 'svg+xml': {
          $img_attrs .= $img_src ? ' src="' . $img_src . '"' : '';
          break;
        }
        default: {
          if ( isset($image['sizes']) && !empty($image['sizes']) ) {
            foreach ( $custom_sizes as $i => $size ) {
              $img_srcset .= ( $i > 0 ) ? ', ' : '';
              $img_srcset .= $image['sizes'][$custom_sizes_title . '-' . $size] . ' ' . $size . 'w';
            }
            $img_attrs .= $img_srcset ? ' data-sizes="auto" data-srcset="' . $img_srcset . '"' : '';
          }
          break;
        }

      }

      $html = '<img ' . $img_attrs . ' />';

    }

    return $html;

  }

  // ---------------------------------------- Lazyload Image
  public function render_lazyload_image( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        "alt_text" => "",
        "block_name" => "image",
        "classes" => "",
        "custom_sizes_title" => $this->custom_image_title,
        "custom_sizes" => $this->custom_image_sizes,
        "delay" => 0,
        "duration" => 300,
        "image" => [],
      ],
      $params
    ));

    $block_classes = "{$block_name} lazyload lazypreload lazyload-item lazyload-item--{$block_name}";
    $block_classes = $classes ? "{$classes} {$block_classes}" : $block_classes;

    $img_alt = $alt_text ?: ( isset($image["alt"]) && !empty($image["alt"]) ? $image["alt"] : get_bloginfo("name") . " Photograph" );
    $img_sizes = $image["sizes"] ?? [];
    $img_src = $image["url"] ?? "";
    $img_srcset = "";
    $img_type = $image["subtype"] ?? "";
    $img_height = $image["height"] ?? "";
    $img_width = $image["width"] ?? "";

    if ( "svg+xml" !== $img_type ) {
      if ( !empty($img_sizes) ) {
        foreach ( $custom_sizes as $i => $size ) {
          $img_srcset .= ( $i > 0 ) ? ", " : "";
          $img_srcset_key = "{$custom_sizes_title}-{$size}";
          $img_srcset .= $img_sizes[$img_srcset_key] . " {$size}w";
          if ( 10 === $size ) {
            $img_src = $img_sizes[$img_srcset_key];
          }
        }
      }
    }

    return $img_src ? "
      <img
        alt='{$img_alt}'
        class='{$block_classes}'
        data-src='{$img_src}'
        data-transition-delay='{$delay}'
        data-transition-duration='{$duration}'
        src='{$img_src}'
        " . ( $img_width ? "width='{$img_width}'" : "" ) . "
        " . ( $img_height ? "height='{$img_height}'" : "" ) . "
        " . ( "svg+xml" !== $img_type && $img_srcset ? "data-sizes='auto' data-srcset='{$img_srcset}'" : "" ) . "
      />
    " : "";

  }

  // ---------------------------------------- Lazyload Video
  public function render_lazyload_video( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'background' => false,
        'classes' => '',
        'delay' => 0,
        'duration' => 300,
        'html' => '',
        'mime_type' => '',
        'preload' => true,
        'video' => [],
        'video_url' => '',
      ],
      $params
    ));

    if ( !empty($video) ) {

      $video_classes = 'lazyload lazyload-item lazyload-item--video';
      $video_classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
      $video_classes .= $preload ? ' lazypreload' : '';
      $video_classes .= $classes ? ' ' . $classes : '';

      $video_url = $video['url'] ?? '';
      $mime_type = $video['mime_type'] ?? '';

      if ( $video_url && $mime_type ) {
        $html = '<video
          class="' . $video_classes . '"
          src="' . $video_url . '"
          data-transition-delay="' . $delay . '"
          data-transition-duration="' . $duration . '"
          preload="none"
          muted=""
          autoplay
          loop
          playsinline
          muted
        >';
          $html .= '<source src="' . $video_url . '" type="' . $mime_type . '">';
        $html .= '</video>';
      }

    }

    return $html;

  }

  // ---------------------------------------- Link
  public function render_link( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'active' => false,
        'block_name' => 'link',
        'classes' => '',
        'html' => '',
        'link' => '',
        'rel' => '',
        'style' => '',
        'target' => '_self',
        'title' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;
    $block_classes .= $style ? ' ' . $block_name . '--' . $style : '';
    $block_classes .= $active ? ' active' : '';
    $block_classes .= !$link ? ' no-link' : '';

    // ---------------------------------------- Template
    if ( $title ) {
      $html .= '<a';
      $html .= ' class="' . $block_classes . '"';
      $html .= ' href="' . $link . '"';
      $html .= ' target="' . $target . '"';
      $html .= ' title="' . str_replace( '<br>', ', ', $title ) . '"';
      $html .= $rel ? ' rel="' . $rel . '"' : '';
      $html .= '>';
        $html .= '<span class="' . $block_name . '__title">' . $title . '</span>';
        switch( $style ) {
          case 'text-with-arrow': {
            $html .= '<span class="' . $block_name . '__icon">' . $this->render_svg([ 'type' => 'icon.arrow' ]) . '</span>';
            break;
          }
        }
      $html .= '</a>';
    }

    return $html;

  }

  public function render_nu_link( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'active' => false,
        'block_name' => 'link',
        'classes' => '',
        'html' => '',
        'rel' => '',
        'title' => '',
        'url' => ''
      ],
      $params
    ));

    $block_classes = $classes ?: $block_name;
    $block_classes .= $active ? " active" : "";
    $target = str_contains( $url, $_SERVER['SERVER_NAME'] ) ? "_self" : "_blank";

    if ( $title && $url ) {
      return "
        <a class='{$block_classes}' href='{$url}' target='{$target}' title='{$title}'>
          <span class='{$block_name}__title'>{$title}</span>
        </a>
      ";
    }

    return;

  }

}
