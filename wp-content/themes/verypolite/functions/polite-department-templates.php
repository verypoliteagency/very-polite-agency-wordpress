<?php

class CustomThemeTemplates extends CustomThemeBase {

  //////////////////////////////////////////////////////////
  ////  Properties
  //////////////////////////////////////////////////////////

  private $name = 'Custom Theme Templates';
  private $version = '2.0';

  //////////////////////////////////////////////////////////
  ////  Methods
  //////////////////////////////////////////////////////////

  public function render_address( $address = [], $format = 'human' ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'street' => '',
        'city' => '',
        'country' => '',
        'google_maps_query' => 'https://www.google.com/maps/search/?api=1&query=',
        'html' => '',
        'name' => 'Very Polite Agency',
        'postal' => '',
        'region' => '',
      ],
      $address
    ));

    $html = $street ?: '';
    $html .= $city ? '<br>' . $city : '';
    $html .= $region ? ' ' . $region : '';

    switch ( $format ) {
      case 'human': {
        $html .= $postal ? ' ' . $postal : '';
        break;
      }
      case 'human-full': {
        $html .= $country ?  '<br>' . $country : '';
        $html .= $postal ? ' ' . $postal : '';
        break;
      }
    }

    if ( 'directions' === $format ) {
      $html .= $name ? ', ' . $name : '';
      $html = str_replace( '<br>', ' ', $html );
      $html = $google_maps_query . urlencode(trim($html));
    }

    return $html;

  }

  // ---------------------------------------- Article Preview
  public function render_article_preview( $id = false, $params = [] ) {

    $html = '';

    if ( $id ) {

      // default data
      $title = false;

      // default params
      $count = 'Count not set!';

      if ( $params ) {
        extract( $params );
      }

      // get data
      if ( get_the_title( $id ) ) {
        $title = get_the_title( $id );
      }

      $html .= '<article class="article article--preview" data-post-id="' . $id . '">';
        $html .= '<div class="article__main rte">';
          $html .= '<p>' . $count . ' | ' . $title . ' | ' . $id . '</p>';
        $html .= '</div>';
      $html .= '</article>';

    }

    return $html;

  }

  // ---------------------------------------- Button
  public function render_button( $params = [] ) {

    // default data
    $html = '';
    $block_name = 'button';
    $columns = 'col-12';
    $image = $inset = $link = false;

    extract( $params );

    $columns .= ( $inset ) ? ' col-lg-8 offset-lg-2' : ' col-lg-10 offset-lg-1';

    if ( $image && $link ) {
      $html .= '<div class="' . $block_name . '">';
        $html .= $this->render_bs_container( 'open', $columns );
          $html .= '<div class="' . $block_name . '__main">';

            // $link defaults
            $url = false;
            $external = $internal = $type = false;
            $target = '_self';
            $rel = '';

            // extract $link
            extract( $link );

            switch( $type ) {
              case 'external':
                $url = $external;
                $rel = 'noopener';
                $target = '_blank';
                break;
              case 'internal':
                $url = $internal;
                break;
            }

            $html .= '<a
              class="' . $block_name . '__link"
              href="' . $url . '"
              target="' . $target . '"
              ' . ( $rel ? 'rel="' . $rel . '"' : '' ) . '
            >';

              // $image defaults
              $type = false;
              $animated = $static = $static_rollover = false;

              // extract $link
              extract( $image );

              switch( $type ) {
                case 'animated':
                  $html .= ( $animated ) ? '<div class="' . $block_name . '__animated">' . $animated . '</div>' : '';
                  break;
                case 'static':
                  $args = [ 'delay' => 0 ];
                  if ( $static ) {
                    $args['image'] = $static;
                    $html .= '<div class="' . $block_name . '__image static">' . $this->render_lazyload_image( $args ) . '</div>';
                  }
                  if ( $static ) {
                    $args['image'] = $static_rollover;
                    $html .= '<div class="' . $block_name . '__image rollover">' . $this->render_lazyload_image( $args ) . '</div>';
                  }
                  break;
              }

            $html .= '</a>';


          $html .= '</div>';
        $html .= $this->render_bs_container( 'close' );
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Caption
  public function render_caption( $caption = '', $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'aos' => '',
        'block_name' => 'caption',
        'cols' => 'col-12',
        'container' => 'container',
        'classes' => '',
        'html' => '',
        'width' => 'standard',
      ],
      $params
    ));

    if ( $caption ) {

      $block_classes = $classes ? $classes . ' ' . $block_name : $block_name;

      switch ( $width ) {
        case 'compact': {
          $cols .= ' col-lg-8 offset-lg-2';
          break;
        }
        case 'standard': {
          $cols .= ' col-lg-10 offset-lg-1';
          break;
        }
      }

      $html .= '<div class="' . $block_classes . '"' . ( $aos ?: '' ) . '>';
        $html .= $this->render_bs_container( 'open', $cols, $container );
          $html .= '<div class="' . $block_name . '__content">' . $caption . '</div>';
        $html .= $this->render_bs_container( 'closed', $cols, $container );
      $html .= '</div>';

    }

    return $html;

  }

  // ---------------------------------------- Card Post
  public function render_card_post( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'card-post',
        'classes' => '',
        'html' => '',
        'id' => false,
        'style' => '',
        'text_colour' => '',
      ],
      $params
    ));

    if ( $id ) {

      // ---------------------------------------- Block
      $block_classes = $classes ? "{$classes} {$block_name}" : "{$block_name}";
      $block_classes .= " {$style}";
      $block_id = "{$block_name}--{$id}";

      // ---------------------------------------- AOS
      $aos_id = $block_id;
      $aos_delay = 150;
      $aos_increment = 150;

      // ---------------------------------------- Content (Post)
      $body_copy_size = "featured" == $style ? "md" : "sm";
      $categories = get_the_category( $id ) ?: [];
      $cta_title = "featured" == $style ? "Read This" : "Read That";
      $excerpt = get_the_excerpt( $id ) ?: '';
      $featured_image = $this->get_featured_image_by_post_id( $id ) ?: [];
      $permalink = get_permalink( $id ) ?: '';
      $title = get_the_title( $id ) ?: '';

      // ---------------------------------------- Content (Post Date)
      date_default_timezone_set('America/Los_Angeles');
      $date_today = new DateTimeImmutable();
      $date_published = get_post_datetime( $id, 'date', 'local' );
      $date_difference = $date_published->diff($date_today);
      $date_difference_days = $date_difference->format('%a');
      $post_is_new = $date_difference_days <= 30 ? true : false;

      // ---------------------------------------- Template
      $html .= "<article class='{$block_classes}' id='{$block_id}'>";

        if ( !empty($featured_image) ) {
          $html .= "<div class='{$block_name}__featured-image image'>";
            $html .= $permalink ? "<a class='{$block_name}__featured-image-link link' href='{$permalink}' target='_self' title='{$title}'>" : "";
              $html .= $this->render_lazyload_image([ 'image' => $featured_image ]);
              if ( $post_is_new ) {
                $html .= "<div class='{$block_name}__badge'>";
                  $html .= $this->render_svg([ 'type' => 'longevity.badge.new' ]);
                $html .= "</div>";
              }
            $html .= $permalink ? "</a>" : "";
          $html .= "</div>";
        }

        $html .= "<div class='{$block_name}__content'>";
          if ( !empty($categories) && 'featured' == $style ) {
            $html .= "<div class='{$block_name}__categories body-copy--md'>";
              $html .= $this->render_categories([ "categories" => $categories ]);
            $html .= "</div>";
          }
          $html .= $title ? "<strong class='{$block_name}__title heading--primary'>{$title}</strong>" : "";
          $html .= $excerpt ? "<div class='{$block_name}__excerpt body-copy--primary body-copy--{$body_copy_size}'>" . trim_string( $excerpt, 125 ) . "</div>" : "";
          if ( $permalink ) {
            $link_classes = "{$block_name}__button button--tertiary button--{$text_colour}";
            $html .= "<div class='{$block_name}__cta'>";
              $html .= $this->render_nu_link([ "block_name" => "button", "classes" => $link_classes, "title" => $cta_title, "url" => $permalink ]);
            $html .= "</div>";
          }
        $html .= "</div>";

      $html .= "</article>";

    }

    return $html;

  }

  // ---------------------------------------- Card Masonry Post
  public function render_card_work( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'card-masonry',
        'classes' => '',
        'count' => 1,
        'html' => '',
        'id' => false,
      ],
      $params
    ));

    if ( $id ) {

      // ---------------------------------------- Post Data
      $categories_child = $this->render_categories_by_id([ 'children' => true, 'id' => $id ]) ?? '';
      $categories_parent = $this->render_categories_by_id([ 'children' => false, 'id' => $id ]) ?? '';
      $classes = $classes ? $classes . ' ' . $block_name : $block_name;
      $images[] = $this->get_featured_image_by_post_id( $id ) ?? [];
      $images[] = get_field( 'featured_image_alt', $id ) ?? [];
      shuffle( $images );
      $logo = get_field( 'logo', $id ) ?? [];
      $permalink = get_permalink( $id ) ?? '';
      $title = get_the_title( $id ) ?? '';

      // ---------------------------------------- Template
  		$html .= '<div
  		  class="' . $classes . '"
        data-post-count="' . $count . '"
        data-post-title="' . $title . '"
        data-post-id="' . $id . '"
      >';

  		  $html .= '<a class="' . $block_name . '__link link" href="' . $permalink . '" title="' . $title . '" target="_self">';

			    $html .= '<div class="' . $block_name . '__image">';
			      $html .= $this->render_lazyload_image([ 'alt_text' => $title, 'image' => $images[0] ]);
  				$html .= '</div>';

  				$html .= '<div class="' . $block_name . '__content body-copy--primary">';
            $html .= $categories_parent ? '<span class="' . $block_name . '__cats-parent">' . $categories_parent . '</span>' : '';
            if ( $logo ) {
              $html .= '<div class="' . $block_name . '__logo">' . $this->render_lazyload_image([ 'alt_text' => $title, 'classes' => 'has-logo', 'image' => $logo ]) . '</div>';
            } else {
              $html .= '<strong class="' . $block_name . '__title masonry-grid__card-title heading--primary">' . $title . '</strong>';
            }
            $html .= $categories_child ? '<span class="' . $block_name . '__cats-child">' . $categories_child . '</span>' : '';
					$html .= '</div>';

        $html .= '</a>';
      $html .= '</div>';

    }

    return $html;

  }

  // ---------------------------------------- Render Categories
  public function render_categories( $params = [] ) {

    extract(array_merge(
      [
        'children' => false,
        'html' => '',
        'categories' => [],
      ],
      $params
    ));

    $cats_count = 0;

    foreach ( $categories as $cat ) {
      $use_cat = $children && $cat->category_parent > 0 || !$children && $cat->category_parent === 0 ? true : false;
      if ( $use_cat ) {
        if ( 0 === $cats_count ) {
		      $html .= $cat->name;
	      } else {
		      $html .= ', ' . $cat->name;
	      }
        $cats_count++;
      }
    }

    return $html;

  }

  // ---------------------------------------- Render Child Categories by ID
  public function render_categories_by_id( $params = [] ) {

    extract(array_merge(
      [
        'children' => false,
        'html' => '',
        'id' => false,
      ],
      $params
    ));

    $cats_count = 0;
    $cats = get_the_category( $id ) ?? [];

    foreach ( $cats as $cat ) {
      $use_cat = $children && $cat->category_parent > 0 || !$children && $cat->category_parent === 0 ? true : false;
      if ( $use_cat ) {
        if ( 0 === $cats_count ) {
		      $html .= $cat->name;
	      } else {
		      $html .= ', ' . $cat->name;
	      }
        $cats_count++;
      }
    }

    return $html;

  }

  // ---------------------------------------- Contact Us
  public function render_colour_from_theme( $theme = '' ) {

    switch ( $theme ) {
      case 'arlo-green': {
        $colour = 'black';
        break;
      }
      case 'black': {
        $colour = 'white';
        break;
      }
      case 'blue': {
        $colour = 'white';
        break;
      }
      case 'beige': {
        $colour = 'black';
        break;
      }
      case 'green': {
        $colour = 'white';
        break;
      }
      case 'grey': {
        $colour = 'black';
        break;
      }
      case 'red': {
        $colour = 'black';
        break;
      }
      case 'white': {
        $colour = 'black';
        break;
      }
      case 'yellow': {
        $colour = 'black';
        break;
      }
      default: {
        $colour = 'black';
      }
    }

    return $colour;

  }

  // ---------------------------------------- Contact Us
  public function render_contact_us( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'contact-us',
        'contact_email' => '',
        'contact_heading' => '',
        'contact_message' => '',
        'html' => '',
        'services_heading' => '',
        'services_message' => '',
      ],
      $params
    ));

    if ( $contact_message || $services_message ) {

      $block_id = $this->get_unique_id("{$block_name}--");

      $html .= '<div class="' . $block_name . '" id="' . $block_id . '">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          $html .= '<div class="' . $block_name . '__main">';
            $html .= '<div class="' . $block_name . '__layout">';

              $html .= '<div class="' . $block_name . '__contact">';

                if ( $contact_heading ) {
                  $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 1, 'transition' => 'fade-up' ]);
                  $html .= '<h2 class="' . $block_name . '__heading heading--2" ' . $aos . '>' . $contact_heading . '</h2>';
                }

                if ( $contact_message ) {
                  $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 2, 'transition' => 'fade-up' ]);
                  $html .= '<div class="' . $block_name . '__message body-copy--primary body-copy--md" ' . $aos . '>' . $contact_message . '</div>';
                }

                if ( $contact_email ) {
                  $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 3, 'transition' => 'fade-up' ]);
                  $html .= '<div class="cta" data-justify="left" ' . $aos . '>';
                    $html .= $this->render_link([
                      'classes' => $block_name . '__link link',
                      'link' => 'mailto:' . $contact_email,
                      'style' => 'outlined',
                      'target' => '_blank',
                      'title' => 'Say Hi',
                    ]);
                  $html .= '</div>';
                }

              $html .= '</div>';

              $html .= '<div class="' . $block_name . '__services">';

                if ( $services_heading ) {
                  $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 2, 'transition' => 'fade-up' ]);
                  $html .= '<h2 class="' . $block_name . '__heading heading--2" ' . $aos . '>' . $services_heading . '</h2>';
                }

                if ( $services_message ) {
                  $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 3, 'transition' => 'fade-up' ]);
                  $html .= '<div class="' . $block_name . '__message body-copy--primary body-copy--md" ' . $aos . '>' . $services_message . '</div>';
                }

                $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 4, 'transition' => 'fade-down' ]);
                $html .= '<div class="' . $block_name . '__services-icon" ' . $aos . '>';
                  $html .= '<img src="' . $this->get_theme_directory('assets') . '/img/VPA--about-us--services.svg" width="209" height="123" alt="Very Polite Services Icon" />';
                $html .= '</div>';

              $html .= '</div>';

            $html .= '</div>';
          $html .= '</div>';
        $html .= $this->render_bs_container( 'close' );
      $html .= '</div>';

    }

    return $html;

  }

  // ---------------------------------------- Days Left
  public function render_days_left( $params = [] ) {

    $html = '';
    $block_name = 'days-left';
    $block_id = $this->get_unique_id("{$block_name}--");

    $circles = 9;
    $this_year = (int) date( 'Y' );

    $html .= '<div class="' . $block_name . '" id="' . $block_id . '">';

      $html .= '<div class="' . $block_name . '__background">';
        for ( $i = 1; $i <= $circles; $i++ ) {
          $html .= '<div class="' . $block_name . '__circle ' . $block_name . '__circle--' . ( $i == $circles ? 'center' : 'ring' ) . '" data-count="' . $i . '"></div>';
        }
      $html .= '</div>';

      $html .= '<div class="' . $block_name . '__main">';

        $html .= '<div class="' . $block_name . '__calculator">';
          $html .= '<h2 class="' . $block_name . '__heading">Enter Birthday</h2>';
          $html .= '<div class="' . $block_name . '__fields">';
            $html .= '<input class="' . $block_name . '__input-day" type="number" data-date="day" min="1" max="31"  placeholder="DD">';
            $html .= '<span class="days-left__divider">/</span>';
            $html .= '<input class="' . $block_name . '__input-month" type="number" data-date="month" min="1" max="12"  placeholder="MM">';
            $html .= '<span class="days-left__divider">/</span>';
            $html .= '<input class="' . $block_name . '__input-year" type="number" data-date="year" min="' . ( $this_year - 150 ) . '" max="' . ( $this_year + 150 ) . '" placeholder="YYYY">';
          $html .= '</div>';
          $html .= '<button class="' . $block_name . '__button-submit days-left__button days-left__button--submit button" type="button">Submit</button>';
        $html .= '</div>';

        $html .= '<div class="' . $block_name . '__result">';
          $html .= '<h2 class="' . $block_name . '__result-heading ' . $block_name . '__heading days-left__heading--result"></h2>';
          $html .= '<button class="' . $block_name . '__button-reset days-left__button days-left__button--reset button" type="button">Reset</button>';
        $html .= '</div>';

      $html .= '</div>';

    $html .= '</div>';

    return $html;

  }

  // ---------------------------------------- Embedded Video
  public function render_embedded_video( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'aspect_ratio' => '16/9',
        'block_name' => 'embedded-content',
        'caption' => '',
        'html' => '',
        'id' => '',
        'inset' => '',
        'margin_bottom' => '',
        'placeholder' => '',
        'type' => 'vimeo',
        'vimeo_id' => '',
      ],
      $params
    ));

    if ( $id ) {

      $block_id = $this->get_unique_id("{$block_name}--");

      $html .= '
        <style>
          #' . $block_id . ' .' . $block_name . '__iframe {
            aspect-ratio: ' . $aspect_ratio . ';
          }
        </style>
      ';

      $html .= '<div class="' . $block_name . '" id="' . $block_id . '">';
        $html .= ( $inset ) ? $this->render_bs_container( 'open', 'col-12' ) : '';
          $html .= '<div class="' . $block_name . '__main">';

            $html .= '<div class="' . $block_name . '__iframe iframe">';
              $html .= $this->render_lazyload_iframe([ 'id' => $id, 'type' => $type ]);
            $html .= '</div>';

            $html .= $caption ? '<div class="' . $block_name . '__caption caption rte body-copy--primary body-copy--md"><p>' . $caption . '</p></div>' : '';

          $html .= '</div>';
        $html .= ( $inset ) ? $this->render_bs_container( 'close' ) : '';
      $html .= '</div>';

    }

    return $html;

  }

  // ---------------------------------------- Fifty Fifty
  public function render_fifty_fifty( $params  = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'block_name' => 'fifty-fifty',
        'caption' => '',
        'cta' => [],
        'full_bleed' => '',
        'html' => '',
        'icon' => [],
        'image' => [],
        'justification' => '',
        'message' => '',
        'vertical_alignment' => '',
      ],
      $params
    ));

    if ( !empty($image) || $caption || $message ) {

      $block_id = $this->get_unique_id("{$block_name}--");
      $grid_stlye = $image && $message ? 'grid grid--2' : 'grid';

      $html .= '<div
        class="' . $block_name . '"
        id="' . $block_id . '"
        data-full-bleed="' . ( $full_bleed ? 'true' : 'false' ) . '"
        data-justification="' . $justification . '"
        data-vertical-alignment="' . $vertical_alignment . '"
      >';
        $html .= ( $full_bleed ) ? '' : $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          $html .= '<div class="' . $block_name . '__main">';
            $html .= '<div class="' . $block_name . '__layout ' . $grid_stlye . '">';

              $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 1, 'transition' => 'fade-left' ]);
              $html .= '<div class="' . $block_name . '__media image" ' . $aos . '>';
                $html .= !empty($image) ? $this->render_lazyload_image([ 'image' => $image ]) : '';
                $html .= $caption ? '<div class="' . $block_name . '__caption caption--primary caption--xs"><p>' . $caption . '</p></div>' : '';
              $html .= '</div>';

              $aos = $this->render_aos_attrs([ 'anchor' => $block_id, 'delay' => 250 * 1, 'transition' => 'fade-down' ]);
              $html .= '<div class="' . $block_name . '__content content" ' . $aos . '>';
                $html .= !empty($icon) ? '<div class="' . $block_name . '__icon">' . $this->render_lazyload_image([ 'image' => $icon ]) . '</div>' : '';
                $html .= $message ? '<div class="' . $block_name . '__message body-copy--primary body-copy--md">' . $message . '</div>' : '';
                if ( !empty($cta) ) {
                  $cta['classes'] = $block_name . '__cta';
                  $cta['link_classes'] = $block_name . '__link';
                  $html .= $this->render_cta($cta);
                }
              $html .= '</div>';

            $html .= '</div>';
          $html .= '</div>';
        $html .= ( $full_bleed ) ? '' : $this->render_bs_container( 'close' );
      $html .= '</div>';

    }

    return $html;

  }

  // ---------------------------------------- Get Address
  public function get_address( $type = 'human' ) {

  	global $post;

    // look here

    $address = $address_2 = $city = $country = $name = $postal = $region = $result = '';
  	$address_obj = get_field( 'company_info', 'options' ) ? get_field( 'company_info', 'options' ) : [];
    $google_maps_directions_query = 'https://www.google.com/maps/search/?api=1&query=';

    extract( $address_obj );

    if ( $address_obj ) {
      switch ( $type ) {
        case 'human':

          $result = $address ? $address : '';
          $result .= $city ? '<br>' . $city : '';
          $result .= $region ? ' ' . $region : '';
          $result .= $postal ? ' ' . $postal : '';
          $result = $address_2 ? $address_2 . '–' . $result : $result;

          break;
        case 'directions':

          $result = $address ? $address : '';
          $result .= $city ? ' ' . $city : '';
          $result .= $region ? ' ' . $region : '';
          $result .= $postal ? ' ' . $postal : '';
          $result .= $name ? ', ' . $name : '';
          $result = $address_2 ? $address_2 . '–' . $result : $result;
          $result = $google_maps_directions_query . urlencode(trim($result));

          break;
      }
    }

  	return $result;

	}

  // ---------------------------------------- Get Server Url
  public function get_server_url() {

    $url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'https://' : 'http://';
    $url .= $_SERVER['HTTP_HOST'] ?? 'host-not-set';
    $url .= $_SERVER['REQUEST_URI'] ?? 'request-uri-not-set';

    return $url;

  }

  // ---------------------------------------- Heading
  public function render_heading( $params = [] ) {

    $html = '';
    $block_name = 'heading';
    $columns = 'col-12 col-md-10 offset-md-1';
    $heading = false;

    extract( $params );

    if ( $heading ) {
      $html .= '<div class="' . $block_name . '">';
        $html .= $this->render_bs_container( 'open', $columns );
          $html .= '<div class="' . $block_name . '__main">';
            $html .= '<h2 class="' . $block_name . '__heading">' . $heading . '</h2>';
          $html .= '</div>';
        $html .= $this->render_bs_container( 'open', $columns );
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Lottie
  public function render_lottie( $params = [] ) {

    // ---------------------------------------- Deconstruct
    extract(array_merge(
      [
        'background' => '',
        'classes' => '',
        'html' => '',
        'id' => '',
        'source' => '',
      ],
      $params
    ));

    // ---------------------------------------- Data
    $id = $id ?: 'lottie--' . $this->get_unique_id();

    // ---------------------------------------- Template
    if ( $source ) {
      $html .=  '<lottie-player
        ' . ( $background ? 'background="' . $background . '"' : '' ) . '
        id="' . $id . '"
        src="' . $source . '"
        loop autoplay></lottie-player>';
    }

    return $html;

  }

  // ---------------------------------------- Marquee
  public function render_marquee_items( $params = [] ) {

    extract(array_merge(
      [
        'items' => [],
        'hidden' => false,
        'html' => '',
      ],
      $params
    ));

    if ( !empty($items) ) {
      foreach ( $items as $i => $item ) {
        $item_text = $item["text"] ?? '';
        $item_type = $item["type"] ?? 'text';
        $html .= "<span class='marquee__item {$item_type}'" . ( $hidden ? " aria-hidden='true'" : "" ) . ">{$item_text}</span>";
      }
    }

    return $html;

  }

  // ---------------------------------------- Masonry
  public function render_masonry( $ids = [] ) {

  	$html = '';
  	$this_theme_directory = 'theme_directory';
  	$this_lazyload  = 'lazyload';

  	if ( $ids ) {

    	$html = '<div class="masonry-grid" id="masonry-grid">';
    	  $html .= '<div class="masonry-grid__container" id="masonry-grid__container" data-masonry-layout-complete="false">';

          $html .= '<div class="masonry-grid__sizer"></div>';

  	      foreach ( $ids as $index => $id ) {
            $html .= $this->render_card_masonry([
              'id' => $id,
              'index' => $index,
            ]);
  	      }

        $html .= '</div>';
      $html .= '</div>';

  	}

    echo $html;

  }

  // ---------------------------------------- Media Item
  public function render_media( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'media-item',
        'html' => '',
        'image' => [],
        'lottie' => '',
        'svg' => '',
        'type' => '',
        'video' => [],
      ],
      $params
    ));

    switch( $type ) {
      case 'image': {
        $html .= !empty( $image ) ? $this->render_lazyload_image([ 'image' => $image ]) : '';
        break;
      }
      case 'svg': {
        $html .= $svg ?: '';
        break;
      }
      case 'lottie': {
        $html .= $lottie ? $this->render_lottie([ 'source' => $lottie ]) : '';
        break;
      }
      case 'video': {
        $html .= !empty( $video ) ? $this->render_lazyload_video([ 'video' => $video ]) : '';
        break;
      }
    }

    return $html;

  }

  // ---------------------------------------- Media Row
  public function render_media_row( $params = [] ) {

    $html = '';
    $block_name = 'media';
    $inset = false;
    $media_row = [];

    extract( $params );

    if ( $media_row ) {

      $html .= '<div class="' . $block_name . '">';
        $html .= ( $inset ) ? $this->render_bs_container( 'open', 'col-12' ) : '';
          $html .= '<div class="' . $block_name . '__main">';

            foreach ( $media_row as $index => $row ) {

              $aos_id = $this->get_unique_id("media-row-aos--");
              $caption = $row['caption'] ?? '';
              $gutter_horizontal = $row['gutters']['horizontal'] ?? false;
              $gutter_vertical = $row['gutters']['vertical'] ?? false;
              $media = $row['media'] ?: [];
              $media_count = count( $media );
              $media_carousel = $media_count > 2 ? true : false;
              $media_carousel_gap = $gutter_vertical ? 'true' : 'false';
              $media_carousel_id = $this->get_unique_id("media-row-glide--");

              if ( !empty( $media ) ) {

                $html .= '<div class="' . $block_name . '__row" data-media-row-index="' . $index . '" id="' . $aos_id . '">';

                  if ( $media_carousel ) {
                    $html .= '<div class="' . $block_name . '__carousel glide js--glide d-md-none" id="' . $media_carousel_id . '" data-glide-gap="0" data-glide-style="work" data-glide-mobile-only="true">';
                      $html .= '<div class="glide__track" data-glide-el="track">';
                        $html .= '<ul class="glide__slides">';
                          foreach ( $media as $index => $item ) {
                            $html .= '<li class="glide__slide">';
                              $html .= '<div class="' . $block_name . '__item ' . ( $item['type'] ?? '' ) . '">';
                                $html .= $this->render_media( $item );
                              $html .= '</div>';
                            $html .= '</li>';
                          }
                        $html .= '</ul>';
                      $html .= '</div>';
                    $html .= '</div>';
                  }

                  $html .= '<div class="' . $block_name . '__grid grid grid--' . $media_count . ' ' . ( $media_carousel ? 'd-none d-md-grid' : '' ) . '">';
                    foreach ( $media as $index => $item ) {
                      $aos = $this->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => 250 * ( $index + 1 ), 'transition' => 'fade-up' ]);
                      $html .= '<div class="' . $block_name . '__item grid__item ' . ( $item['type'] ?? '' ) . '" ' . $aos . '>';
                        $html .= $this->render_media( $item );
                      $html .= '</div>';
                    }
                  $html .= '</div>';

                $html .= '</div>';

                $html .= $caption ? '<div class="' . $block_name . '__caption caption--primary rte body-copy--primary body-copy--xs"><p>' . $caption . '</p></div>' : '';

              }

            }

          $html .= '</div>';
        $html .= ( $inset ) ? $this->render_bs_container( 'closed' ) : '';
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Navigation
  public function render_navigation( $links = [], $current_id = false, $class_modifier = '' ) {

    $html = '';
    $block_name = 'navigation';
    $block_classes = ( $class_modifier ) ? $block_name . ' ' . $block_name . '--' . $class_modifier : $block_name;

    if ( $links ) {
      $html .= '<nav class="' . $block_classes . '">';
        foreach( $links as $i => $item ) {

          // set defaults
          $type = $title = $link = $link_anchor = $link_category = $link_external = $link_page = false;
          $is_internal = true;
          $is_active = false;

          // extract link data
          extract( $item );

          switch ( $type ) {
            case 'anchor':
              $link = ( $link_anchor ) ? '#' . $link_anchor : false;
              break;
            case 'external':
              $link = ( $link_external ) ? $link_external : false;
              $is_internal = false;
              break;
            case 'category':
              break;
            case 'page-post':
              break;
          }

          $html .= $this->render_navigation_item( $title, $link, $is_active, $is_internal );

        }
      $html .= '</nav>';
    }

    return $html;

  }

  // --------------------------- Navigation for WP
  public function render_navigation_wp( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'navigation';
    $default_args = [ 'current_id' => false, 'menu_title' => '' ];
    $html = '';

    extract( array_merge( $default_args, $params ) );

    if ( wp_get_nav_menu_items( $menu_title ) ) {

      $menu_items = wp_get_nav_menu_items( $menu_title ) ? wp_get_nav_menu_items( $menu_title ) : [];

      foreach ( $menu_items as $item ) {

        $id = $item->object_id;
        $link = $item->url;
        $title = $item->title;
        $object_type = $item->object;
        $link_type = $item->type;
        $active = ( $id == $current_id ) ? true : false;
        $new_tab = ( 'custom' == $link_type ) ? true : false;
        $classes = $block_name . '__link';

        $link_args = [
          'active' => $active,
          'classes' => $classes,
          'new_tab' => $new_tab,
          'title' => $title,
          'url' => $link
        ];

        $html .= '<div class="' . $block_name . '__item">';
          $html .= ( $link && $title ) ? $this->render_link( $link_args ) : '';
        $html .= '</div>';

      }

    }

    return $html;

  }

  // --------------------------- Navigation Item
  public function render_navigation_item( $title = '', $url = '', $active = false, $internal = true ) {

    $html = '';
    $block_name = 'navigation';
    $target = '_self';
    $rel = false;

    if ( !$internal ) {
      $target = '_blank';
      $rel = 'noopener';
    }

    if ( $title && $url ) {
      $html .= '<div class="' . $block_name . '__item' . ( $active ? ' active' : '' ) . '">';
        $html .= '<a
          class="' . $block_name . '__link' . ( $active ? ' active' : '' ) . '"
          href="' . $url . '"
          target="' . $target . '"
          ' . ( $rel ? 'rel="' . $rel . '"' : '' ) . '
        >' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Post Categories
  public function render_post_categories( $params = [] ) {

    $block_name = 'categories';
    $defaults = [ 'show_cat_icon' => false, 'post_id' => false, 'limit' => 1 ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $categories = get_the_category( $post_id ) ? get_the_category( $post_id ) : [];

      foreach( $categories as $i => $cat ) {



        if ( $i < $limit ) {

          // ---------------------------------------- WP Data
          $cat_name = $cat->name;
          $cat_id = $cat->term_id;
          $cat_slug = $cat->slug;
          $cat_url = get_category_link( $cat_id );
          $cat_term = get_term( $cat_id ) ? get_term( $cat_id ) : false;

          // ---------------------------------------- ACF Data
          $cat_icon = get_field( 'icon_svg', $cat_term );

          if ( $i > 0 ) {
            $html .= '<div class="' . $block_name . '__item delimiter">|</div>';
          }

          $html .= '<div class="' . $block_name . '__item">';
            $html .= ( $show_cat_icon && $cat_icon ) ? '<div class="' . $block_name . '__icon">' . $cat_icon . '</div>' : '';
            $html .= '<div class="' . $block_name . '__name">' . $cat_name . '</div>';
          $html .= '</div>';

        }
      }

    }

    return $html;

  }

  // --------------------------- Post Meta
  public function render_post_meta( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'show_cat_icon' => false, 'cat_limit' => 1, 'date_format' => 'm.d.y', 'meta_types' => [], 'post_id' => false ];
    $html = $meta_html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id && $meta_types ) {

      if ( 'post' !== get_post_type( $post_id ) ) {
        $block_name = get_post_type( $post_id );
      }

      foreach ( $meta_types as $i => $meta ) {

        switch( $meta ) {
          case 'author':

            $author_id = get_post_field( 'post_author', $post_id );
            $author = get_the_author_meta( 'display_name', $author_id );
            $meta_html = ( $author ) ? '<div class="' . $block_name . '__author author">' . $author . '</div>' : '';
            break;

          case 'categories':

            $categories = $this->render_post_categories([ 'show_cat_icon' => $show_cat_icon, 'post_id' => $post_id, 'limit' => $cat_limit ]);
            $meta_html = ( $categories ) ? '<div class="' . $block_name . '__categories categories" data-show-cat-icont="' . $show_cat_icon . '">' . $categories . '</div>' : '';
            break;

          case 'date':

            $date = get_the_date( $date_format, $post_id );
            $meta_html = ( $date ) ? '<time class="' . $block_name . '__date date" datetime="' . $date . '">' . $date . '</time>' : '';
            break;

          case 'issue':

            $issue = get_field( 'issue', $post_id );
            $meta_html = ( $issue ) ? '<div class="' . $block_name . '__issue issue">Issue ' . ( $issue > 10 ? $issue : '0' . $issue ) . '</div>' : '';
            break;

        }

        if ( $meta_html ) {
          $html .= ( $i > 0 ) ? '<div class="' . $block_name . '__delimiter delimiter">|</div>' . $meta_html : $meta_html;
        }

      }
    }

    return $html;

  }

  // --------------------------- Post Preview
  public function render_post_preview( $post_id = false, $params = [] ) {

    $html = '';
    $block_name = 'article';

    if ( $post_id ) {

      // default $params
      $appearance = '';
      $date_format = 'F j, Y';

      if ( $params ) {
        extract( $params );
      }

      // get data
      $date = ( get_the_date( $date_format, $post_id ) ) ? get_the_date( $date_format, $post_id ) : false;
      $excerpt = ( get_the_excerpt( $post_id ) ) ? get_the_excerpt( $post_id ) : false;
      $featured_image = ( $this->get_featured_image_by_post_id( $post_id ) ) ? $this->get_featured_image_by_post_id( $post_id ) : false;
      $permalink = ( get_permalink( $post_id ) ) ? get_permalink( $post_id ) : false;
      $title = ( get_the_title( $post_id ) ) ? get_the_title( $$post_idid ) : false;

      // build template
      $html .= '<article class="' . $block_name . ( $appearance ? ' ' . $block_name . '--' . $appearance : '' ) . '" data-post-id="' . $post_id . '">';
        $html .= '<a href="' . $permalink . '" target="_self">';

          $html .= '<div class="' . $block_name . '__featured-image">';
            $html .= $featured_image ? $this->render_lazyload_image([ 'image' => $featured_image ]) : '';
          $html .= '</div>';

          $html .= '<div class="' . $block_name . '__content">';
            $html .= $date ? '<div class="' . $block_name . '__date">' . $date . '</div>' : '';
            $html .= $title ? '<h2 class="' . $block_name . '__title">' . $title . '</h2>' : '';
          $html .= '</div>';

        $html .= '</a>';
      $html .= '</article>';

    }

    return $html;

  }

  public function render_post_tags_by_id( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'block_name' => 'tags',
        'classes' => '',
        'html' => '',
        'id' => '',
      ],
      $params
    ));

    $tags = get_the_tags( $id ) ?: [];
    $classes = $classes ? $classes . ' ' . $block_name : $block_name;

    if ( !empty($tags) ) {
      $html .= '<div class="' . $classes . '">';
        foreach( $tags as $index => $item ) {
          if ( 0 == $index ) {
            $html .= $item->name;
          } else {
            $html .= ', ' . $item->name;
          }
        }
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Pre-connect Scripts
  public function render_preconnect_external_scripts( $scripts = [] ) {
    $html = '';
    $relationship = [ 'preconnect', 'dns-prefetch' ];
    if ( !empty( $scripts ) && !empty( $relationship ) ) {
      foreach ( $relationship as $rel ) {
        foreach ( $scripts as $script ) {
          $html .= "<link rel='{$rel}' href='{$script}'>";
        }
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_preload_fonts( $fonts = [] ) {
    $html = '';
    if ( $fonts ) {
      foreach ( $fonts as $font ) {
        $font_src = $this->get_theme_directory('assets') . "/fonts/{$font}.woff2";
        $html .= "<link rel='preload' href='{$font_src}' as='font' type='font/woff2' crossorigin>";
      }
    }
    return $html;
  }

  // --------------------------- Preload Fonts
  public function render_seo( $enable = true ) {
    $html = '<meta name="robots" content="noindex, nofollow">';
    if ( $enable && !is_attachment( $this->get_theme_info('post_ID') ) ) {
			if ( defined( 'WPSEO_VERSION' ) ) {
				$html = '<!-- Yoast Plugin IS ACTIVE! -->';
			} else {
				$html = '<!-- Yoast Plugin IS NOT ACTIVE! -->';
				$html .= '<meta name="description" content="' . get_bloginfo( 'description' ) . '">';
			}
    }
    return $html;
  }

  // ---------------------------------------- Services
  public function render_services( $params = [] ) {

    $html = '';
    $block_name = 'services';
    $heading = $list = false;

    extract( $params );

    if ( $heading || $list ) {

      $html .= '<div class="' . $block_name . '">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          $html .= '<div class="' . $block_name . '__main">';

            $html .= ( $heading ) ? '<h2 class="' . $block_name . '__heading">' . $heading . '</h2>' : '';

             if ( $list ) {

              $last_item_index = count( $list ) + 1;
              $count = 1;

              foreach ( $list as $index => $value ) {

                $enable = $heading = $image = $message = false;
                $remainder = $count % 2;

                extract( $value );

                if ( $count === 1 ) {
                  $html .= '<div class="' . $block_name . '__group">';
                }

                if ( $enable && ( $heading || $image || $message ) ) {
                  $html .= '<div class="' . $block_name . '__item" data-count="' . $count . '">';
                    $html .= ( $image ) ? '<div class="' . $block_name . '__image">' . $this->render_lazyload_image([ 'image' => $image ]) . '</div>' : '';
                    $html .= ( $heading ) ? '<h3 class="' . $block_name . '__heading subheading">' . $heading . '</h3>' : '';
                    $html .= ( $message ) ? '<div class="' . $block_name . '__message body-copy--primary body-copy--md">' . $message . '</div>' : '';
                  $html .= '</div>';
                  $count++;
                }

                if ( ! $remainder ) {
                  $html .= '</div>'; // .group
                  $html .= '<div class="' . $block_name . '__group">';
                }

              } // foreach

                $html .= '<div class="' . $block_name . '__item end ' . ( $count % 2 ? 'odd' : 'even' ) . '" data-count="' . $count . '">';
                   $html .= '<img src="' . $this->get_theme_directory('assets') . '/img/VPA--about-us--services-animated-logo.svg" />';
                $html .= '</div>';

              $html .= '</div>'; // .group

            } // endif

          $html .= '</div>';
        $html .= $this->render_bs_container( 'close' );
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Subnavigation Item
  public function render_subnavigation_item ( $params = [], $current_id = 0 ) {

    // default data
    $html = '';
    $block_name = 'subnavigation';
    $link = $link_attachment = $link_category = $link_external = $link_id = $link_page = $link_type = $title = false;
    $target = '_self';

    extract( $params );

    switch( $link_type ) {
      case 'attachment':
        $link = ( isset($link_attachment['url']) ) ? $link_attachment['url'] : false;
        $target = '_blank';
        break;
      case 'category':
        $link_id = $link_category;
        $link = get_category_link( $link_id );
        break;
      case 'external':
        $link = $link_external;
        $target = '_blank';
        break;
      case 'page':
        $link_id = $link_page;
        $link = get_permalink( $link_id );
        break;
    }

    $is_active = ( $current_id === $link_id ) ? true : false;

    if ( $title && $link ) {
      $html .= '<div class="' . $block_name . '__item" data-is-active="' . ( $is_active ? 'true' : 'false' ) . '">';
        $html .= '<a class="' . $block_name . '__link' . ( $is_active ? ' active' : '' ) . '" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // ---------------------------------------- Render Tags by ID
  public function render_tags_by_id( $params = [] ) {

    extract(array_merge(
      [
        'html' => '',
        'id' => false,
      ],
      $params
    ));

    $tags_count = 0;
    $tags = get_the_tags( $id ) ?: [];

    foreach ( $tags as $index => $tag ) {
      if ( 0 === $index ) {
        $html .= $tag->name;
      } else {
        $html .= ', ' . $tag->name;
      }
    }

    return $html;

  }

  // --------------------------- SVG Icon
  public function render_svg( $params = [] ) {

    // ---------------------------------------- Defaults
    extract(array_merge(
      [
        'html' => '',
        'type' => 'burger'
      ],
      $params
    ));

    switch ( $type ) {

      case 'icon.close': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 25.4 25.4" style="enable-background:new 0 0 25.4 25.4;" xml:space="preserve">
            <polygon points="25.4,2.8 22.5,0 12.7,9.9 2.8,0 0,2.8 9.9,12.7 0,22.5 2.8,25.4 12.7,15.5 22.5,25.4 25.4,22.5 15.5,12.7 "/>
          </svg>';
        break;
      }

      case 'icon.email': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <path  d="M36,8.1C36,8,36,8,36,7.9c-0.1-2.5-2.2-4.6-4.8-4.6H4.8c-2.6,0-4.7,2-4.8,4.6C0,7.9,0,8,0,8.1c0,0,0,0,0,0v19.8
	        c0,2.7,2.2,4.8,4.8,4.8h26.4c2.7,0,4.8-2.2,4.8-4.8L36,8.1C36,8.1,36,8.1,36,8.1z M4.8,6.3h26.4c0.7,0,1.4,0.4,1.6,1.1L18,17.8
	        L3.2,7.4C3.5,6.8,4.1,6.3,4.8,6.3z M33,27.9c0,1-0.8,1.8-1.8,1.8H4.8c-1,0-1.8-0.8-1.8-1.8V11l14.1,9.9c0.3,0.2,0.6,0.3,0.9,0.3
	        s0.6-0.1,0.9-0.3L33,11V27.9z"/>
        </svg>';
        break;
      }

      case 'icon.instagram': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <g>
	          <path d="M35.9,10.6c0-1.5-0.3-3-0.8-4.4C34.6,5,33.9,3.9,33,3c-0.9-0.9-2-1.6-3.2-2.1c-1.4-0.5-2.9-0.8-4.4-0.8
		          C23.5,0,22.9,0,18,0c-4.9,0-5.5,0-7.4,0.1c-1.5,0-3,0.3-4.4,0.8C5,1.4,3.9,2.1,3,3C2.1,3.9,1.4,5,0.9,6.2c-0.5,1.4-0.8,2.9-0.8,4.4
		          C0,12.5,0,13.1,0,18c0,4.9,0,5.5,0.1,7.4c0,1.5,0.3,3,0.8,4.4C1.4,31,2.1,32.1,3,33c0.9,0.9,2,1.6,3.2,2.1c1.4,0.5,2.9,0.8,4.4,0.8
		          C12.5,36,13.1,36,18,36c4.9,0,5.5,0,7.4-0.1c1.5,0,3-0.3,4.4-0.8c1.2-0.5,2.3-1.2,3.2-2.1c0.9-0.9,1.6-2,2.1-3.2
		          c0.5-1.4,0.8-2.9,0.8-4.4C36,23.5,36,22.9,36,18C36,13.1,36,12.5,35.9,10.6z M32.7,25.3c0,1.1-0.2,2.3-0.6,3.3
		          c-0.3,0.8-0.8,1.5-1.3,2.1c-0.6,0.6-1.3,1-2.1,1.3c-1.1,0.4-2.2,0.6-3.3,0.6c-1.9,0.1-2.5,0.1-7.3,0.1c-4.8,0-5.4,0-7.3-0.1
		          c-1.1,0-2.3-0.2-3.3-0.6c-0.8-0.3-1.5-0.8-2.1-1.3c-0.6-0.6-1.1-1.3-1.3-2.1c-0.4-1.1-0.6-2.2-0.6-3.3c-0.1-1.9-0.1-2.5-0.1-7.3
		          c0-4.8,0-5.4,0.1-7.3c0-1.1,0.2-2.3,0.6-3.3c0.3-0.8,0.8-1.5,1.3-2.1C5.9,4.7,6.6,4.3,7.4,4c1.1-0.4,2.2-0.6,3.3-0.6
		          c1.9-0.1,2.5-0.1,7.3-0.1l0,0c4.8,0,5.4,0,7.3,0.1c1.1,0,2.3,0.2,3.3,0.6c0.8,0.3,1.5,0.8,2.1,1.3c0.6,0.6,1.1,1.3,1.3,2.1
		          c0.4,1.1,0.6,2.2,0.6,3.3c0.1,1.9,0.1,2.5,0.1,7.3C32.8,22.8,32.7,23.4,32.7,25.3z"/>
	          <path d="M24.5,11.5c-0.9-0.9-1.9-1.5-3-2C20.4,9,19.2,8.8,18,8.8c-1.8,0-3.6,0.5-5.1,1.6c-1.5,1-2.7,2.5-3.4,4.1
		          c-0.7,1.7-0.9,3.5-0.5,5.3c0.4,1.8,1.2,3.4,2.5,4.7c1.3,1.3,2.9,2.2,4.7,2.5c1.8,0.4,3.7,0.2,5.3-0.5c1.7-0.7,3.1-1.9,4.1-3.4
		          c1-1.5,1.6-3.3,1.6-5.1c0-1.2-0.2-2.4-0.7-3.5C26.1,13.3,25.4,12.3,24.5,11.5z M22.2,22.2C21.1,23.4,19.6,24,18,24
		          c-1.2,0-2.3-0.4-3.3-1s-1.8-1.6-2.2-2.7c-0.5-1.1-0.6-2.3-0.3-3.5c0.2-1.2,0.8-2.2,1.6-3.1c0.8-0.8,1.9-1.4,3.1-1.6
		          s2.4-0.1,3.5,0.3c1.1,0.5,2,1.2,2.7,2.2s1,2.1,1,3.3C24,19.6,23.4,21.1,22.2,22.2z"/>
	          <path d="M27.6,6.2c-1.2,0-2.2,1-2.2,2.2c0,1.2,1,2.2,2.2,2.2c1.2,0,2.2-1,2.2-2.2C29.8,7.2,28.8,6.2,27.6,6.2z"/>
          </g>
        </svg>';
        break;
      }

      case 'icon.spotify': {
        $html = '<svg x="0px" y="0px" viewBox="0 0 36 36" style="enable-background:new 0 0 36 36;" xml:space="preserve">
          <path d="M18,0C8.1,0,0,8.1,0,18s8.1,18,18,18s18-8.1,18-18S27.9,0,18,0z M26.3,26.1c-0.5,0.4-0.9,0.7-1.6,0.4
	        c-4.3-2.7-9.4-3.1-15.8-1.8c-0.7,0.2-1.1-0.2-1.3-0.9c-0.2-0.7,0.2-1.1,0.9-1.4c6.8-1.6,12.8-0.9,17.5,2
	        C26.5,24.8,26.5,25.4,26.3,26.1z M28.4,21.1c-0.5,0.7-1.4,0.9-1.8,0.5c-4.9-2.9-12.1-3.8-18-2c-0.7,0-1.4-0.2-1.6-1.1
	        c-0.2-0.7,0.2-1.4,0.9-1.6c6.5-2,14.6-0.9,20.2,2.5C28.6,19.6,28.8,20.5,28.4,21.1z M28.6,16c-5.9-3.4-15.3-3.8-20.9-2
	        c-0.9,0.2-1.8-0.2-2-1.1c-0.2-0.9,0.2-1.8,1.1-2c6.3-1.8,16.9-1.6,23.6,2.5c0.9,0.5,1.1,1.6,0.7,2.2C30.6,16.2,29.5,16.4,28.6,16z"
	        />
        </svg>';
        break;
      }

      case 'longevity.badge.new': {
        $html = '<svg viewBox="0 0 225 134" fill="none">
          <g clip-path="url(#clip0_905_722)">
            <path d="M202.87 55.47L175.57 68.64L187.16 92.75L152.83 88.1L144.11 115.81L116.21 98.71L93.32 122.09L78.11 97.34L45.94 110.22L51.31 84.02L21.62 77.9L48.93 64.73L37.33 40.62L71.67 45.27L80.39 17.56L108.28 34.66L131.17 11.28L146.38 36.03L178.55 23.15L173.18 49.35L202.87 55.47Z" fill="white"/>
            <path d="M224.49 52.79L184.88 71.89L199.08 101.4L157.72 95.8L148.08 126.43L117.3 107.56L92.05 133.36L75.24 106L36.49 121.51L43.07 89.43L0 80.57L39.61 61.47L25.41 31.96L66.77 37.56L76.41 6.93L107.18 25.8L132.44 0L149.25 27.36L188 11.85L181.42 43.93L224.49 52.8V52.79ZM175.56 68.64L202.87 55.47L173.18 49.35L178.55 23.14L146.38 36.02L131.17 11.27L108.28 34.65L80.38 17.55L71.66 45.26L37.32 40.61L48.92 64.72L21.61 77.89L51.3 84.01L45.93 110.22L78.1 97.34L93.31 122.09L116.2 98.71L144.1 115.81L152.82 88.1L187.16 92.75L175.56 68.64Z" fill="#FF0000"/>
            <path d="M68.49 55.7L77.7 53.92L87.49 70.94L87.58 70.92L87.62 52L96.47 50.29V83.27L86.95 85.11L77.45 67.89L77.36 67.91V86.96L68.5 88.67V55.7H68.49Z" fill="#231F20"/>
            <path d="M107.03 71.61C107.49 73.74 108.7 75.72 111.72 75.13C113.1 74.86 114.29 74.12 114.63 72.77L122.63 71.23C122.35 73.82 121.19 75.94 119.46 77.51C117.54 79.26 115.38 80.28 112.8 80.78C106.13 82.07 100.36 79.4 99.04 72.59C97.56 64.9 101.75 57.95 109.36 56.49C112.16 55.95 114.27 56.14 116.12 56.85C120.27 58.49 122.4 62.65 122.68 68.59L107.03 71.61V71.61ZM106.75 67.05L114.44 65.57C114.1 63.33 112.81 61.64 110.4 62.11C108.71 62.44 107 63.87 106.74 67.06L106.75 67.05Z" fill="#231F20"/>
            <path d="M121.22 54.98L129.62 53.36L131.7 61.49C132.14 63.3 132.81 66.3 132.81 66.3L132.9 66.28C132.9 66.28 133.4 62.91 133.84 60.89L135.8 52.16L142.78 50.81L144.69 58.79C145.16 60.77 145.85 63.87 145.85 63.87L145.94 63.85C145.94 63.85 146.54 60.51 146.99 58.53L149.05 49.6L157.54 47.96L150.75 72.79L142.83 74.32L140.33 64.93C139.84 63.09 139.29 60.52 139.29 60.52L139.2 60.54C139.2 60.54 138.83 63.38 138.41 65.3L136.16 75.61L128.29 77.13L121.2 54.97L121.22 54.98Z" fill="#231F20"/>
            </g>
            <defs>
            <clipPath id="clip0_905_722">
            <rect width="224.49" height="133.37" fill="white"/>
            </clipPath>
            </defs>
          </svg>';
        break;
      }

      case 'longevity.barcode': {
        $html = '<svg viewBox="0 0 240.8 64.66">
          <rect class="uuid-099a0c91-8228-4ba9-9b72-96a012ab11dc" x="0" y="0" width="240.8" height="64.66" fill="#fff"/>
            <g><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="110.8" y="5.88" width="8.26" height="37.75" fill="#111"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M11.68,5.88V58.78c-1.91,0-3.71,0-5.51,0,0-45.89,0-7.01,0-52.9h5.51Z"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="55.74" y="5.88" width="5.51" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="86.02" y="5.88" width="5.51" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="97.04" y="5.88" width="5.51" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="165.04" y="5.88" width="5.51" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="173.9" y="5.88" width="5.51" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="127.32" y="5.88" width="5.51" height="37.75"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M17.19,5.88c0,45.78,0,6.79,0,52.73h-2.75c0-45.82,0-6.89,0-52.73h2.75Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M30.96,5.88c0,45.78,0,6.79,0,52.73-.91,0-1.7,0-2.75,0,0-45.82,0-6.89,0-52.73h2.75Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M21.71,5.88c0,45.78,0,6.79,0,52.73-.91,0-1.7,0-2.75,0,0-45.82,0-6.89,0-52.73h2.75Z"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="140.88" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="134.29" y="5.88" width="1.96" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="81.74" y="5.88" width="1.96" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="78.39" y="5.88" width="1.96" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="46.25" y="5.88" width="1.96" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="147.52" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="152.58" y="5.88" width="2.75" height="37.75"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M39.22,5.88c0,45.78,0,6.79,0,52.73-.91,0-1.7,0-2.75,0,0-45.82,0-6.89,0-52.73h2.75Z"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="41.97" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="183.73" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="191.99" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="197.5" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="66.75" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="72.26" y="5.88" width="2.75" height="37.75"/><rect class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" x="121.82" y="5.88" width="2.75" height="37.75"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M229.12,5.88c0,45.78,0,6.79,0,52.73-2.73,0-5.36,0-8.26,0,0-45.84,0-6.9,0-52.73,2.75,0,5.51,0,8.26,0Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M212.6,5.88c0,45.78,0,6.79,0,52.73-1.82,0-3.52,0-5.51,0,0-45.83,0-6.9,0-52.73,1.84,0,3.67,0,5.51,0Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M234.62,5.88c0,45.78,0,6.79,0,52.73-.91,0-1.7,0-2.75,0,0-45.82,0-6.89,0-52.73h2.75Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M218.78,5.88c0,45.78,0,6.79,0,52.73-.63,0-1.18,0-1.91,0,0-45.82,0-6.89,0-52.73,.64,0,1.28,0,1.91,0Z"/></g><g><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M53.77,56.48h-4.37s-.8,1.98-.8,1.98h-2.34s3.95-9.22,3.95-9.22h2.77s3.95,9.22,3.95,9.22h-2.34s-.8-1.98-.8-1.98Zm-.71-1.79l-1.44-3.54h-.08s-1.44,3.54-1.44,3.54h2.95Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M58.57,49.24h2.24s0,7.3,0,7.3h5.2s0,1.92,0,1.92h-7.44s0-9.22,0-9.22Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M68.44,49.24l1.99,6.82h.08s2.04-6.82,2.04-6.82h2.62s2.01,6.82,2.01,6.82h.08s2-6.82,2-6.82h2.48s-3.15,9.22-3.15,9.22h-2.71s-1.99-6.85-1.99-6.85h-.08s-2,6.85-2,6.85h-2.71s-3.13-9.22-3.13-9.22h2.48Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M88.94,56.48h-4.37s-.8,1.98-.8,1.98h-2.34s3.95-9.22,3.95-9.22h2.77s3.95,9.22,3.95,9.22h-2.34s-.8-1.98-.8-1.98Zm-.71-1.79l-1.44-3.54h-.08s-1.44,3.54-1.44,3.54h2.95Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M94.9,55.16l-3.9-5.93h2.63s2.34,3.87,2.34,3.87h.08s2.35-3.87,2.35-3.87h2.63s-3.9,5.93-3.9,5.93v3.29s-2.24,0-2.24,0v-3.29Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M101.62,56.48l1.57-1.42c.79,.93,2.02,1.62,3.57,1.62,1.25,0,1.84-.42,1.84-.94,0-.47-.4-.7-1.33-.86l-2.07-.34c-2.09-.34-3.11-1.11-3.11-2.66,0-1.78,1.66-2.81,4.03-2.81,1.82,0,3.53,.58,4.6,1.73l-1.55,1.39c-.82-.82-1.79-1.22-3.14-1.22-1.19,0-1.79,.34-1.79,.82,0,.46,.33,.63,1.29,.79l1.95,.32c2.38,.38,3.39,1.21,3.39,2.78,0,1.82-1.74,2.92-4.2,2.92-2.27,0-3.99-.79-5.05-2.12Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M125.27,55.8c0,1.75-1.33,2.66-3.6,2.66h-5.1s0-9.22,0-9.22h4.94c2.2,0,3.27,.8,3.27,2.2,0,1.09-.62,1.69-1.58,1.98v.05c1.39,.24,2.07,1.12,2.07,2.33Zm-6.45-4.77v1.67h2.58c.79,0,1.16-.28,1.16-.84,0-.57-.37-.83-1.16-.83h-2.58Zm4.19,4.52c0-.72-.5-1.08-1.5-1.08h-2.69s0,2.2,0,2.2h2.69c1,0,1.5-.38,1.5-1.12Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M127.48,49.24h7.81s0,1.9,0,1.9h-5.57s0,1.56,0,1.56h4.46s0,1.9,0,1.9h-4.46s0,1.98,0,1.98h5.57s0,1.9,0,1.9h-7.81s0-9.22,0-9.22Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M149.38,52.13c0,1.82-1.28,2.9-3.48,2.9h-2.63s0,3.42,0,3.42h-2.24s0-9.22,0-9.22h4.87c2.2,0,3.48,1.08,3.48,2.9Zm-2.24,0c0-.63-.4-1.03-1.34-1.03h-2.53v2.06h2.53c.95,0,1.34-.4,1.34-1.03Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M150.93,53.85c0-2.87,2.13-4.77,5.32-4.77,3.19,0,5.32,1.9,5.32,4.77,0,2.87-2.13,4.77-5.32,4.77-3.19,0-5.32-1.9-5.32-4.77Zm8.35,0c0-1.8-1.16-2.82-3.03-2.82-1.87,0-3.03,1.01-3.03,2.82,0,1.8,1.16,2.82,3.03,2.82,1.87,0,3.03-1.01,3.03-2.82Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M163.75,49.24h2.24s0,7.3,0,7.3h5.2s0,1.92,0,1.92h-7.44s0-9.22,0-9.22Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M175.43,49.24v9.22s-2.24,0-2.24,0v-9.22s2.24,0,2.24,0Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M180.75,51.16h-3.37s0-1.92,0-1.92h8.98s0,1.92,0,1.92h-3.37s0,7.3,0,7.3h-2.24s0-7.3,0-7.3Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M188.31,49.24h7.81s0,1.9,0,1.9h-5.57s0,1.56,0,1.56h4.46s0,1.9,0,1.9h-4.46s0,1.98,0,1.98h5.57s0,1.9,0,1.9h-7.81s0-9.22,0-9.22Z"/><path class="uuid-0d2bc192-f34f-4ced-b751-e5cf75a4677f" d="M202.35,56.36c0,1.24-1.01,2.25-2.25,2.25-1.24,0-2.25-1.01-2.25-2.25,0-1.24,1.02-2.25,2.25-2.25s2.25,1.01,2.25,2.25Zm-.42,0c0-1-.83-1.83-1.83-1.83-1,0-1.83,.83-1.83,1.83,0,1,.83,1.83,1.83,1.83,1,0,1.83-.82,1.83-1.83Zm-.9,.49l.16,.59h-.68s-.12-.53-.12-.53c-.04-.16-.11-.22-.3-.22h-.33v.75s-.62,0-.62,0v-2.17s1.16,0,1.16,0c.62,0,.84,.24,.84,.65,0,.33-.19,.5-.46,.57v.04c.21,.01,.3,.12,.36,.33Zm-.53-.87c0-.16-.08-.22-.26-.22h-.49v.45h.49c.18,0,.26-.05,.26-.22Z"/></g>
          </svg>';
        break;
      }

      case 'longevity.monogram': {
        $html = '<svg viewBox="0 0 200 140">
          <g clip-path="url(#clip0_916_721)">
          <path d="M199.664 36.1736L175.355 10.5812C175.259 10.4849 175.127 10.4247 174.995 10.4247H141.037C140.881 10.4247 140.749 10.5571 140.749 10.7136V16.6483C140.749 16.9492 140.402 17.1177 140.162 16.9251C134.672 12.3748 125.682 8.72738 113.072 8.72738C112.065 8.72738 111.058 8.75145 110.075 8.7996C109.739 8.81164 109.428 8.607 109.308 8.29401C107.102 2.80476 101.66 0.0119858 94.9116 0.0119858C83.9197 0.0119858 71.1417 10.509 63.5781 18.8512C62.9068 19.5855 61.7201 19.4531 61.2406 18.5864C56.2301 9.57002 43.8478 5.98275 34.3063 5.98275C17.9922 5.97071 0 16.0223 0 34.2717C0 53.8813 21.0488 64.619 38.3698 64.619C43.7878 64.619 48.9182 63.9088 54.1444 62.8856C54.9356 62.7291 55.6787 63.331 55.6787 64.1496V82.1822C55.6787 97.0128 46.5088 104.175 32.2445 104.175H23.1945C22.5951 104.175 22.0437 104.488 21.7441 105.006L13.1855 119.704C12.5262 120.824 13.3413 122.244 14.6359 122.244H81.1507C84.2074 122.244 90.9919 122.966 89.3018 127.023C89.1819 127.312 88.4986 128.383 87.7794 129.503C87.0602 130.622 87.4558 131.959 88.6785 132.392C89.4576 132.669 90.2847 132.416 90.7881 131.814L95.1393 126.53C95.8466 125.663 97.1052 125.482 98.0282 126.12C109.763 134.246 123.836 140.012 139.73 140.012C175.895 140.012 197.063 117.08 198.61 94.4488C198.633 94.0395 198.49 93.6422 198.202 93.3533L188.792 83.6027C188.481 83.2777 188.708 82.736 189.164 82.736H198.586C199.377 82.736 200.012 82.098 200.012 81.3035V36.9922C200.012 36.6913 199.892 36.3903 199.688 36.1736H199.664ZM84.2074 9.37742C86.4369 9.37742 88.2589 10.1599 89.7093 11.4239C90.3206 11.9535 90.1768 12.9286 89.4456 13.2416C84.4111 15.3361 79.8442 18.1048 75.8526 21.4995C75.3491 21.9208 74.5939 21.5116 74.6659 20.8615C75.3012 15.4686 78.142 9.38946 84.2074 9.38946V9.37742ZM74.534 26.6999C74.534 26.0017 74.8097 25.3275 75.3132 24.846C79.6524 20.693 84.7707 17.2983 90.4645 14.8306C91.3515 14.4453 92.3824 14.8546 92.7659 15.7454C93.7489 18.0206 94.2283 20.5726 94.2283 22.8357C94.2283 33.8503 85.2023 44.239 75.9964 49.6319C75.3491 50.0171 74.534 49.5356 74.534 48.7772V26.6999ZM54.4201 58.1306C49.7453 59.2742 45.0824 59.8521 40.4075 59.8521C25.2922 59.8521 11.3755 50.8116 11.3755 34.2717C11.3755 20.9699 20.7132 10.7377 34.1265 10.7377C44.3152 10.7377 53.3054 17.7317 53.3054 28.4694C53.3054 36.4866 47.8753 43.8177 39.2089 43.8177C34.4501 43.8177 27.8334 41.0851 27.8334 35.4634C27.8334 34.7772 28.169 33.9346 28.6844 33.5855C29.8711 34.7772 31.4055 35.8005 33.2634 35.8005C36.4879 35.8005 39.3767 32.7308 39.3767 29.6612C39.3767 26.0859 36.4879 23.3533 33.0956 23.3533C26.9823 23.3533 22.4034 28.8065 22.4034 34.2596C22.4034 43.4686 31.4055 48.5846 39.7243 48.5846C44.8906 48.5846 49.9131 46.4058 54.2044 43.4926C54.8397 43.0713 55.6787 43.5047 55.6787 44.2751V56.4815C55.6787 57.2519 55.1633 57.938 54.4081 58.1186L54.4201 58.1306ZM73.1315 104.175H55.2232C54.3962 104.175 54.0485 103.116 54.7078 102.622L65.0165 94.9664C65.3161 94.7497 65.6038 94.533 65.8795 94.3164C66.6107 93.7506 67.6656 93.9312 68.169 94.7136C69.8232 97.3138 71.7051 100.022 73.8148 102.767C74.2583 103.345 73.8508 104.175 73.1196 104.175H73.1315ZM122.062 114.949C117.627 114.949 113.371 114.66 109.308 114.119C108.529 114.01 108.205 113.059 108.756 112.506L118.046 103.116C118.909 102.249 119.053 100.829 118.262 99.8778C117.423 98.8667 115.984 98.7583 114.99 99.5528C110.734 102.96 104.573 104.187 97.6086 104.187H82.7809C81.9538 104.187 81.1268 103.947 80.4435 103.465C76.404 100.624 72.9038 97.2777 69.979 93.4737C69.2838 92.5709 69.3317 91.2949 70.0749 90.4281C73.5271 86.3834 74.51 82.0137 74.51 75.2123V57.1074C74.51 56.3972 74.9176 55.7592 75.5529 55.4582C90.7042 48.2115 110.327 32.1771 110.327 14.1805C110.327 13.1453 110.243 12.1582 110.087 11.2433C110.003 10.7738 110.363 10.3525 110.83 10.3284C111.561 10.3043 112.304 10.2803 113.048 10.2803C124.963 10.2803 136.098 14.8907 141.768 21.4634C141.996 21.7282 142.415 21.5597 142.415 21.2106V12.4471C142.415 12.2906 142.535 12.1582 142.703 12.1582H173.329C173.605 12.1582 173.821 12.3869 173.821 12.6517V57.938C173.821 58.251 173.569 58.5038 173.257 58.5038H138.064C137.776 58.5038 137.525 58.2992 137.453 58.0103C134.588 45.7317 131.16 39.9174 121.774 39.9174C113.455 39.9174 107.51 46.0567 107.51 61.5735C107.51 78.4505 112.089 85.2759 122.277 85.2759C131.148 85.2759 135.403 80.3043 137.633 66.2562C137.681 65.9312 137.98 65.6904 138.316 65.7265L175.151 68.6878C175.451 68.7119 175.679 68.9767 175.667 69.2777C174.564 92.9441 156.08 114.937 122.038 114.937L122.062 114.949Z" fill="#111111"/>
          </g>
          <defs>
          <clipPath id="clip0_916_721">
          <rect width="200" height="140" fill="white"/>
          </clipPath>
          </defs>
          </svg>';
        break;
      }

    }

    return $html;

  }

  // ---------------------------------------- Testimonials
  public function render_testimonials( $params = [] ) {

    $html = '';
    $block_name = 'testimonials';
    $heading = $list = false;

    extract( $params );

    if ( $heading || $list ) {

      $html .= '<div class="' . $block_name . '">';
        $html .= $this->render_bs_container( 'open', 'col-12 col-lg-10 offset-lg-1' );
          $html .= '<div class="' . $block_name . '__main">';

            $html .= ( $heading ) ? '<h2 class="' . $block_name . '__heading">' . $heading . '</h2>' : '';

            if ( $list ) {
              $html .= '<div class="' . $block_name . '__list">';
                foreach ( $list as $index => $value ) {

                  $client = $message = false;

                  extract( $value );

                  if ( $client || $message ) {
                    $html .= '<div class="' . $block_name . '__item">';
                      $html .= ( $message ) ? '<div class="' . $block_name . '__message rte">' . $message . '</div>' : '';
                      $html .= ( $client ) ? '<div class="' . $block_name . '__client">– ' . $client . '</div>' : '';
                    $html .= '</div>';
                  }

                }
              $html .= '</div>';
            }

          $html .= '</div>';
        $html .= $this->render_bs_container( 'close' );
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Theme Vitals
  public function render_theme_vitals() {

    $html = '';
    $html .= "<!-- PHP Version: " . $this->get_theme_info('php_version') . " -->";
  	$html .= "<!-- WP Version: " . $this->get_theme_info('wp_version') . " -->";
  	$html .= "<!-- Current Template: " . $this->get_theme_info('template') . " -->";
  	$html .= "<!-- Post ID: " . $this->get_theme_info('post_ID') . " -->";
    $html .= "<!-- Object ID: " . $this->get_theme_info('object_ID') . " -->";

    return $html;

  }

  // --------------------------- Placeholder Content
  public function render_placeholder_content( $type = 'grid', $container = 'container' ) {

    $html = '<div class="placeholder">';
      $html .= $this->render_bs_container( 'open', 'col-12', $container );

        switch ( $type ) {
          case 'content':

            $html .= '<div class="placeholder__content rte">';

              $html .= '<h1>H1 - Placeholder Content</h1>';
              $html .= '<h2>H2 - Mauris turpis enim venenatis quis mi egestas mattis purus</h2>';
              $html .= '<p>Pellentesque at interdum enim. Suspendisse vulputate convallis mi quis auctor. Cras at urna mi. Quisque pretium tempus lacus in viverra. Sed auctor erat enim, sed accumsan orci tristique sit amet. Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</p>';
              $html .= '<p>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum. Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt a. In eget purus massa. Nulla facilisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris laoreet sapien vel odio accumsan, sed posuere libero vestibulum. Praesent est felis, tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida ac pharetra ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>';
              $html .= '<p>Vestibulum id nunc tempor, faucibus leo eu, tincidunt dui. In cursus, metus vel commodo tincidunt, odio felis facilisis arcu, in egestas erat enim id quam. In laoreet metus id luctus pellentesque. Nullam ac nunc non arcu porta maximus ac non odio. Suspendisse luctus mauris sit amet dignissim lacinia. Duis volutpat facilisis nisl quis vulputate. Sed risus purus, mollis in pulvinar in, rhoncus tristique nisi. Nunc sollicitudin sapien nibh, laoreet congue velit porttitor at. Vestibulum elementum maximus condimentum. Nam aliquam, velit ut consectetur scelerisque, sapien magna bibendum sem, ut vulputate libero massa ut justo.</p>';

              $html .= '<h3>H3 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h3>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';

              $html .= '<ol>';
                $html .= '<li>Pellentesque fringilla massa non metus cursus, vitae pretium dolor feugiat.</li>';
                $html .= '<li>Nunc bibendum sapien ac cursus sollicitudin.</li>';
                $html .= '<li>Pellentesque ut elit ac arcu luctus tincidunt.</li>';
                $html .= '<li>Morbi a arcu a lacus iaculis efficitur.</li>';
                $html .= '<li>Suspendisse efficitur nibh in lectus porttitor, vel faucibus lacus sagittis.</li>';
              $html .= '</ol>';

              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Quisque sodales tristique tincidunt. Phasellus suscipit velit vel massa feugiat placerat. Cras quis dolor iaculis nunc commodo rhoncus ac eu ex. Morbi pharetra egestas nunc, at fermentum lorem condimentum quis. Praesent sed erat id diam sollicitudin tincidunt eget vitae odio. Praesent quam odio, ultricies consectetur est eu, varius rhoncus felis. Nam vitae lectus cursus, porttitor orci in, fringilla ipsum. Vestibulum facilisis lacus turpis.</p>';

              $html .= '<h4>H4 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h4>';

              $html .= '<p>Ted aliquam augue a auctor vulputate. Nunc ut congue tellus, non mattis purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi quam ex, euismod at elit nec, placerat aliquam mi. Mauris nec blandit lorem, lacinia tristique enim. Donec suscipit lobortis arcu, non hendrerit urna rutrum in. Sed vitae metus id ex accumsan tempor. Phasellus sed commodo ex. Maecenas quis dui tortor. Vivamus quis commodo orci. Vivamus dignissim, diam malesuada imperdiet vehicula, ligula ligula ultricies libero, ut tempor turpis lacus non ipsum. Quisque non magna quis mauris ullamcorper vestibulum. Aliquam ipsum urna, faucibus congue erat vitae, maximus tincidunt nunc. Maecenas iaculis dui at velit egestas, vel commodo magna scelerisque. Nulla quis risus eu velit ornare aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
              $html .= '<p>Nam ornare laoreet vulputate. Aliquam cursus risus libero, ac tincidunt ante interdum ac. Proin scelerisque dolor non justo vestibulum, nec blandit ante fermentum. Aliquam at odio lobortis, vestibulum felis vitae, vehicula nulla. Sed dapibus sit amet nisl quis tempus. Nulla rutrum non velit id fermentum. Praesent orci nulla, finibus ac sapien nec, tristique imperdiet enim. Nulla elementum lacus vel iaculis condimentum. Vivamus eu semper ligula. Quisque ut ultricies nibh. Maecenas risus justo, gravida eu urna vestibulum, sollicitudin dictum sapien.</p>';

              $html .= '<ul>';
                $html .= '<li>Pellentesque in ligula quis nisl egestas pharetra.</li>';
                $html .= '<li>Cras elementum arcu vel leo tempus convallis.</li>';
                $html .= '<li>Pellentesque non orci id ipsum condimentum laoreet.</li>';
                $html .= '<li>Nulla maximus enim nec neque volutpat, ultrices viverra tellus facilisis.</li>';
                $html .= '<li>Morbi pharetra nunc sed tristique posuere.</li>';
                $html .= '<li>Quisque efficitur odio vitae ipsum fringilla, id aliquam ipsum pretium.</li>';
              $html .= '</ul>';

              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';
              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Mauris fermentum dui sed leo commodo efficitur. Sed quis fringilla mi. Etiam lectus odio, ultricies vestibulum leo sodales, placerat auctor arcu. Donec lorem orci, scelerisque at ante sed, vehicula condimentum purus. In ultrices facilisis nibh, sed iaculis justo vestibulum vestibulum. Duis ut felis id nunc gravida consectetur sit amet ut metus. Integer at neque imperdiet, maximus arcu nec, tincidunt turpis. Vestibulum sit amet felis quis nibh aliquam vehicula eget efficitur eros. Pellentesque semper vulputate nisl, iaculis rutrum magna molestie vitae. Nunc ac dolor ut leo suscipit gravida id non dui. Fusce vestibulum tellus elit, euismod semper libero rutrum eu. Sed tempus, lorem et dapibus interdum, arcu urna rutrum odio, sed dictum dui leo nec nunc. Vestibulum congue tortor tellus. Vivamus euismod risus a tellus laoreet, eu consequat enim ullamcorper. Etiam a iaculis odio, in faucibus purus. Pellentesque venenatis, mauris eu iaculis tempus, dui est vulputate felis, nec interdum magna velit at enim.</p>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
            $html .= '</div>';

            break;
          case 'grid':

            $html .= '<div class="placeholder__grid rte">';

              $html .= '<h1>H1 - Placeholder Grid</h1>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 12; $i++ ) {
                  $html .= '<div class="col-1">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 6; $i++ ) {
                  $html .= '<div class="col-2">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 4; $i++ ) {
                  $html .= '<div class="col-3">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 3; $i++ ) {
                  $html .= '<div class="col-4">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 2; $i++ ) {
                  $html .= '<div class="col-6">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

            $html .= '</div>';

            break;

        } // switch $type

      $html .= $this->render_bs_container( 'close' );
    $html .= '</div>';

    return $html;

  }

  // ---------------------------------------- WYSIWYG
  public function render_wysiwyg( $params = [] ) {

    $html = '';
    $block_name = 'wysiwyg';
    $columns = 'col-12';
    $inset = false;
    $cta = $wysiwyg = false;

    extract( $params );

    $columns .= ( $inset ) ? ' col-lg-8 offset-lg-2' : ' col-lg-10 offset-lg-1';

    if ( $wysiwyg ) {

      $html .= '<div class="' . $block_name . '" data-aos="fade-up" data-aos-duration="650" data-aos-delay="150" data-aos-once="true">';
        $html .= $this->render_bs_container( 'open', $columns );
          $html .= '<div class="' . $block_name . '__main">';

            $html .= ( $wysiwyg ) ? '<div class="' . $block_name . '__message body-copy--primary body-copy--md">' . $wysiwyg . '</div>' : '';
            $html .= ( $cta ) ? $this->render_cta( $cta ) : '';

          $html .= '</div>';
        $html .= $this->render_bs_container( 'close' );
      $html .= '</div>';

    }

    return $html;

  }

}
