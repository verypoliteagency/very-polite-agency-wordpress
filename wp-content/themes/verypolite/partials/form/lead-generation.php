<?php

  /**
  *
  *   Lead Generation Form
  *
  */

  $args = wp_parse_args($args, [
    "action_url" => "",
    "redirect_url" => "",
    "submit_button_title" => "Submit",
    "opt_in_message" => "I consent to receiving marketing material from Very Polite Agency Inc.",
  ]);

  $THEME = $THEME ?? new CustomTheme();

?>

<?php if ( !empty($args["action_url"]) ) : ?>

  <form
    action="<?php echo esc_url($args["action_url"]); ?>"
    class="form js--validate-me"
    data-redirect-url="<?php echo esc_url($args["redirect_url"]); ?>"
    data-form-type="lead-generation"
    enctype="multipart/form-data"
    method="POST"
  >

    <div class="form__main body-copy--primary body-copy--md">

      <div class="form__row row">
        <div class="form__field field col-12 col-lg-6">
          <?= $THEME->render_form_input([
            'classes' => 'input--underlined',
            'name' => 'firstName',
            'placeholder' => 'First Name',
            'required' => true,
            'type' => 'text'
          ]); ?>
        </div>
        <div class="form__field field col-12 col-lg-6">
          <?= $THEME->render_form_input([
            'classes' => 'input--underlined',
            'name' => 'lastName',
            'placeholder' => 'Last Name',
            'required' => true,
            'type' => 'text'
          ]); ?>
        </div>
      </div>

      <div class="form__row row">
        <div class="form__field field col-12 col-lg-12">
          <?= $THEME->render_form_input([
            'classes' => 'input--underlined',
            'name' => 'email',
            'placeholder' => 'Email',
            'required' => true,
            'type' => 'email'
          ]); ?>
        </div>
      </div>

      <div class="form__row row">
        <div style="position: absolute; left: 99223300px;">
          <?=
            $THEME->render_form_input([
              'name' => 'rude',
              'type' => 'text',
            ]);
          ?>
          <input type="checkbox" name="_optin" value="yes" checked readonly tabindex="-1" />
          <input type="text" name="tags" value="very-polite-agency, newsletter" readonly tabindex="-1" />
          <input type="text" name="source" value="<?= $_SERVER['SERVER_NAME'] ?? 'source-not-available'; ?>" readonly tabindex="-1" />
        </div>
      </div>

      <?php if ( !empty($args["opt_in_message"]) ) : ?>
        <div class="form__row row">
          <div class="form__message body-copy--primary body-copy--sm col-12">
            <?php echo wp_kses_post($args["opt_in_message"]); ?>
          </div>
        </div>
      <?php endif; ?>

      <div class="form__row row submit">
        <div class="form__field field col-12">
          <button class="form__button button button--primary" type="submit">
            <?php echo esc_html($args["submit_button_title"]); ?>
          </button>
        </div>
      </div>

    </div>

    <div class="form__loading">
      <div class="form__spinner"></div>
    </div>

  </form>

<?php endif; ?>
