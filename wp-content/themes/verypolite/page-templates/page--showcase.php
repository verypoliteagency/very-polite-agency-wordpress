<?php
	
/*
*	
*   Template Name: Page [ Showcase ]
*   Filename: page--showcase.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();

?>

<div id="showcase" class="page page--showcase" role="main">
  
  <?php 
    
    $showcase_post_ids = array();
    
    if ( have_rows( 'posts' ) ) {
      while ( have_rows( 'posts' ) ) {
        
        // init data
        the_row();
        
        // default data
        $id = false;
        
        // get data
        if ( get_sub_field( 'post' ) ) {
          $id = get_sub_field( 'post' );
          array_push( $showcase_post_ids, $id );
        }
        
      } // endwhile
      
      
      if ( !empty( $showcase_post_ids ) ) {
        
        shuffle( $showcase_post_ids );
        
        $THEME->render_masonry( $showcase_post_ids ); 
        
      }
      
    } // endif

  ?>        
  
</div>
<!-- /#showcase -->	
	
<?php get_footer(); ?>
