<?php

/*
*
*   Template Name: Page [ Lead Generator ]
*   Filename: page--lead-generator.php.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory("home");
$assets_dir = $THEME->get_theme_directory("assets");
$theme_dir = $THEME->get_theme_directory();

// ---------------------------------------- Comment
$template = "lead-generator";
$template_id = $THEME->get_unique_id("{$template}--");

$colour_theme = get_field("colour_theme") ?: "white";
$form = get_field("form") ?: [];
$hero = get_field("hero") ?: [];
$hero_alignment = $hero["alignment"] ?? "center";
$hero_heading = $hero["heading"] ?? "";
$hero_logo = $hero["logo"] ?? "";
$hero_subheading = $hero["subheading"] ?? "";

?>

<div class="<?php echo esc_attr($template); ?> page page--<?php echo esc_attr($template); ?>" id="<?php echo esc_attr($template_id); ?>">

  <?php if ( !empty($hero_heading) || !empty($hero_logo) || !empty($hero_subheading) ) : ?>
    <div class="<?php echo esc_attr($template); ?>__hero">
      <?= $THEME->render_bs_container( "open", "col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2" ); ?>

        <?php if ( !empty($hero_logo) ) : ?>
          <div class="<?php echo esc_attr($template); ?>__logo">
            <?= $THEME->render_lazyload_image([ "image" => $hero_logo ]); ?>
          </div>
        <?php endif; ?>

        <?php if ( !empty($hero_heading) ) : ?>
          <h1 class="<?php echo esc_attr($template); ?>__heading heading--primary heading--md">
            <?php echo esc_html($hero_heading); ?>
          </h1>
        <?php endif; ?>

        <?php if ( !empty($hero_subheading) ) : ?>
          <div class="<?php echo esc_attr($template); ?>__subheading body-copy--primary body-copy--md">
            <?php echo wp_kses_post($hero_subheading); ?>
          </div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( "closed" ); ?>
    </div>
  <?php endif; ?>

  <div class="<?php echo esc_attr($template); ?>__main">
    <?= $THEME->render_bs_container( "open", "col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2" ); ?>
      <?php get_template_part( "./partials/form/lead-generation", null, $form ); ?>
    <?= $THEME->render_bs_container( "closed" ); ?>
  </div>

</div>

<?php get_footer(); ?>
