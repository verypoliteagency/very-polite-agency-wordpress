<?php

/*
*
*   Template Name: Page [ Promo ]
*   Filename: page--promo.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();

?>

<div id="promo" class="page page--promo" role="main">

  <?php

    // default data
    $promo_key = false;
    $tax_query = array();
    $promo_taxonomies = array();
    $query_string = $_SERVER['QUERY_STRING'];
    $query_params = array();
    parse_str( $query_string, $query_params );

    $args = array(
      'post_status'			 => array( 'publish' ),
      'orderby'          => 'rand',
      'posts_per_page'   => '48',
    );

    // default query
    array_push( $tax_query, array(
  		'taxonomy'         => 'category',
  		'terms'            => 'Hidden',
  		'field'            => 'name',  // or slug or id
  		'operator'         => 'NOT IN',
  		'include_children' => true,
  	));

    if ( isset( $query_params['show'] ) && !empty( $query_params['show'] ) ) {

      $promo_key = $query_params['show'];

      if ( have_rows( 'promo' ) ) {
        while ( have_rows( 'promo' ) ) {

          // init data
          the_row();

          // default data
          $show = $cats = $tags = false;

          // get data
          if ( get_sub_field( 'show' ) ) {

            $show = get_sub_field( 'show' );
            $promo_taxonomies[$show] = array();

            if ( get_sub_field( 'categories' ) ) {
              $cats = get_sub_field( 'categories' );
              $promo_taxonomies[$show]['cats'] = $cats;
            }

            if ( get_sub_field( 'tags' ) ) {
              $tags = get_sub_field( 'tags' );
              $promo_taxonomies[$show]['tags'] = $tags;
            }

          }

        }
      }

      if ( isset( $promo_taxonomies[$promo_key] ) && !empty( $promo_taxonomies[$promo_key] ) ) {

        $these_taxonomies = $promo_taxonomies[$promo_key];

        if ( isset( $these_taxonomies['cats'] ) && !empty( $these_taxonomies['cats'] ) ) {

          array_push( $tax_query, array(
        		'taxonomy'         => 'category',
        		'terms'            => $these_taxonomies['cats'],
        		'field'            => 'id',  // or slug
        		'operator'         => 'IN',
        		'include_children' => true,
        	));

        }

        if ( isset( $these_taxonomies['tags'] ) && !empty( $these_taxonomies['tags'] ) ) {

          array_push( $tax_query, array(
        		'taxonomy'         => 'post_tag',
        		'terms'            => $these_taxonomies['tags'],
        		'field'            => 'id',  // or slug
        		'operator'         => 'IN',
        		'include_children' => true,
        	));

        }

        if ( count( $tax_query ) > 1 ) {
          $tax_query['relation'] = 'AND';
        }

        if ( !empty( $tax_query ) ) {
          $args['tax_query'] = $tax_query;
        }

      }

      $THEME->render_masonry( $args );

    } // endif

  ?>

</div>
<!-- /#promo -->

<?php get_footer(); ?>
