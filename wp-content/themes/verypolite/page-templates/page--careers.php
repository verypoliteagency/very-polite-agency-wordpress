<?php

  /**
  *
  *   Template Name: Page [ Careers ]
  *   Filename: page--careers.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- ACF Data
  $content = get_field( 'content_careers' ) ?: [];
  $postings = get_field( 'postings' ) ?: [];
  $show_postings = get_field( 'show_postings' ) ?: false;
  $hr = get_field( 'hr' ) ?: [];

?>

<div id="post" class="post post--careers post--careers" role="main">

  <?php include( locate_template( "./snippets/legacy/post-hero.php" ) ); ?>

  <div class="post__body">

    <?php

      //////////////////////////////////////////////////////////
  		////  Content
  		//////////////////////////////////////////////////////////

      if ( $content ) {
        echo '<section class="flexible-content">';
          echo $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1' );
            foreach( $content as $i => $block ) {

              $layout = isset($block['acf_fc_layout']) ? $block['acf_fc_layout'] : 'not-set';
              $heading = isset($block['heading']) ? $block['heading'] : false;
              $wysiwyg = isset($block['wysiwyg']) ? $block['wysiwyg'] : false;

              echo '<div class="flexible-content__block">';
                switch ( $layout ) {
                  case 'heading':
                    echo $heading ? '<h2 class="flexible-content__heading heading--primary heading--sm">' . $heading . '</h2>' : '';
                    break;
                  case 'wysiwyg':
                    echo $wysiwyg ? '<div class="flexible-content__wysiwyg wysiwyg body-copy--primary body-copy--xs">' . $wysiwyg . '</div>' : '';
                    break;
                }
              echo '</div>';

            }
          echo $THEME->render_bs_container( 'closed' );
        echo '</section>';
      }

      //////////////////////////////////////////////////////////
  		////  Job Postings
  		//////////////////////////////////////////////////////////

      echo '<section class="job-postings">';
        echo $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1' );
          echo '<div class="job-postings__main">';

            if ( $show_postings ) {

              foreach ( $postings as $i => $posting ) {

                $job_id = $posting['job'] ?? 0;
                $job_anchor_id = get_field( 'anchor_id', $job_id ) ?: '';
                $job_collapse_id = $THEME->get_unique_id("job-posting--");
                $job_collapsed = $i === 0 ? false : true;
                $job_title = get_the_title( $job_id ) ?: '';
                $job_location = get_field( 'location', $job_id ) ?: '';
                $job_description = get_field( 'description', $job_id ) ?: '';
                $job_skills = get_field( 'skills', $job_id ) ?: '';
                $job_responsibilities = get_field( 'responsibilities', $job_id ) ?: '';
                $job_other_competencies = get_field( 'other_competencies', $job_id ) ?: '';
                $email_subject = "Hello! I'm interested in the {$job_title} position";
                $email_subject .= $job_location ? ", in {$job_location}" : ".";
                $email_subject_encoded = rawurlencode($email_subject);

                $job_id = $job_anchor_id ?: $job_id;

                if ( $job_title && $job_description ) {

                  echo "<div class='job-postings__item" . ( $job_collapsed ? "" : " show" ) . "' id='{$job_id}' data-collapse-id='{$job_collapse_id}' data-index='{$i}'>";

                    echo "<button
                      class='job-postings__trigger-button button button--collapse-trigger heading--primary heading--sm" . ( $job_collapsed ? " collapsed" : "" ) . "'
                      type='button'
                      data-bs-toggle='collapse'
                      data-bs-target='#{$job_collapse_id}'
                      aria-expanded='true'
                      aria-controls='{$job_collapse_id}'
                      >";
                      echo "<div class='button__title'>";
                        echo "<span class='job-postings__job-title'>{$job_title}</span>";
                        echo $job_location ? "<span class='job-postings__job-location body-copy--primary body-copy--md'>{$job_location}</span>" : "";
                      echo "</div>";
                      echo "<span class='button__icon'>{$THEME->render_svg([ 'type' => 'icon.close' ])}</span>";
                    echo "</button>";

                    echo "<div class='job-postings__body collapse" . ( $job_collapsed ? "" : " show" ) . "' id='{$job_collapse_id}'>";
                      echo "<div class='job-postings__body-padding'>";

                        echo $job_description ? "<div class='job-postings__content body-copy--primary body-copy--xs'>{$job_description}</div>" : "";

                        if ( $job_skills ) {
                          echo "
                            <div class='job-postings__content body-copy--primary body-copy--xs'>
                              <h4>Skills + Qualifications:</h4>
                              {$job_skills}
                            </div>
                          ";
                        }

                        if ( $job_responsibilities ) {
                          echo "
                            <div class='job-postings__content body-copy--primary body-copy--xs'>
                              <h4>Responsibilities:</h4>
                              {$job_responsibilities}
                            </div>
                          ";
                        }

                        if ( $job_other_competencies ) {
                          echo "
                            <div class='job-postings__content body-copy--primary body-copy--xs'>
                              <h4>Other Competencies:</h4>
                              {$job_other_competencies}
                            </div>
                          ";
                        }

                        echo "
                          <div class='job-postings__content body-copy--primary body-copy--xs'>
                            <h4>Benefits:</h4>
                            <ul>
                              <li>Extended health benefits</li>
                              <li>Wellness program</li>
                              <li>Polite co-workers</li>
                              <li>Sake Fizz fridge</li>
                            </ul>
                          </div>
                        ";

                        echo "
                          <div class='job-postings__content body-copy--primary body-copy--xs'>
                            <h4>Application:</h4>
                            <p>We’re excited to meet you! Send us an email introducing yourself, including your CV and any relevant work samples, to <a href='mailto:excuseme@weareverypolite.com?subject={$email_subject_encoded}' target='_blank'>excuseme@weareverypolite.com</a>, please and thank you.</p>
                          </div>
                        ";

                      echo '</div>';
                    echo '</div>';
                  echo '</div>';

                }

              }

            } else {

              //////////////////////////////////////////////////////////
              ////  No Job Postings
              //////////////////////////////////////////////////////////

              echo '
                <div class="job-postings__message message body-copy--primary body-copy--xs">
                  <h2>Nothing Available?</h2>
                  <p>We’re very sorry to say we don’t have any positions at this particular moment. But do check-in again soon! Please and thank you.</p>
                </div>
              ';

            }

          echo '</div>';
        echo $THEME->render_bs_container( 'closed' );
      echo '</section>';

      //////////////////////////////////////////////////////////
  		////  Outro
  		//////////////////////////////////////////////////////////

      echo '<section class="outro">';
        echo $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1' );
          echo '<div class="section__message message body-copy--primary body-copy--xs">';
            echo '<p>Say hello anytime at <a href="mailto:excuseme@weareverypolite.com">excuseme@weareverypolite.com</a> and we’ll keep in touch.</p>';
            echo '<p>Please and thank you!</p>';
            echo '<p><span class="sans-serif">:)</span></p>';
          echo '</div>';
        echo $THEME->render_bs_container( 'closed' );
      echo '</section>';

    ?>

  </div>

</div>

<?php

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
