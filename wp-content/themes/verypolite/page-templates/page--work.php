<?php

  /*
  *
  *   Template Name: Page [ Work ]
  *   Filename: page--work.php
  *
  */

  get_header();

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template
  $template_name = "work";
  $template_classes = $template_name;
  $template_id = $THEME->get_unique_id("{$template_name}--");

  // ---------------------------------------- Content (ACF)
  $post_limit = get_field("post_limit") ?: 1;
  $manual_posts = get_field("contents") ?: [];

?>

<div id="work" class="page page--work">

  <?php

    /*
    //////////////////////////////////////////////////////////
    ////  Specific Post Content
    //////////////////////////////////////////////////////////
    */

    $post_limit = 1;
    $post_count = 0;
    $pre_selected_posts = [];
    $sliced_posts = [];
    $queried_post_ids = [];
    $posts_to_show = 0;


    // get specific post ids
    if ( have_rows( 'contents' ) ) {
      while ( have_rows( 'contents' ) ) {

        // init data
        the_row();

        // default data
        $id = get_sub_field( 'post' );

        if ( $id ) {
          $post_count++;
          array_push( $pre_selected_posts, $id );
        }

      }
    }

    // calculate remaining posts to show based on limit and number of specific posts added
    $posts_to_show = $post_limit - $post_count;

    /*
    //////////////////////////////////////////////////////////
    ////  Build WP Args for Query
    //////////////////////////////////////////////////////////
    */

    // default args
    $tax_query = [];
    $args = [
      'post_status'			  => [ 'publish' ],
      'order'             => 'ASC',
      'posts_per_page'    => $posts_to_show,
    ];

    // do not query posts with 'Hidden' category
    array_push( $tax_query, [
      'taxonomy'         => 'category',
      'terms'            => 'Hidden',
      'field'            => 'name',  // or slug or id
      'operator'         => 'NOT IN',
      'include_children' => true,
    ]);

    // exclude pre-selected posts from WP Query
    if ( !empty( $pre_selected_posts ) ) {
      $args['post__not_in'] = $pre_selected_posts;
    }

    // include posts with specific taxonomies
    if ( have_rows( 'main_menu', 'options' ) ) {
      while ( have_rows( 'main_menu', 'options' ) ) {

        // init data
        the_row();

        // get data
        $post_cats = get_sub_field( 'post_categories' );

        if ( $post_cats ) {
          array_push( $tax_query, [
            'taxonomy'         => 'category',
            'terms'            => $post_cats,
            'field'            => 'id',  // or slug
            'operator'         => 'IN',
            'include_children' => true,
          ]);
        }

        if ( count( $tax_query ) > 1 ) {
          $tax_query['relation'] = 'AND';
        }

      }
    }

    // set tax query params
    if ( !empty( $tax_query ) ) {
      $args['tax_query'] = $tax_query;
    }

    // get new Query
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {

        // init data
        $query->the_post();

        // default post data
        $id = get_the_ID();

        // add post id to array of post ids
        array_push( $queried_post_ids, $id );

      }
    }

    // Restore original Post Data
    wp_reset_postdata();

    // get number of posts to 'slice' out of post_ids array
    $slice_size = 6;

    // if the number of posts returned is less than the slice value, reset slice value to half the total results
    if ( count( $queried_post_ids ) < $slice_size ) {
      $slice_size = ( count( $queried_post_ids ) / 2 );
    }

    // get array of sliced posts
    $sliced_posts = array_slice( $queried_post_ids, 0, $slice_size );

    // merge sliced posts with pre-selected posts
    $result = array_merge( $sliced_posts, $pre_selected_posts );

    // shuffle/randomize results
    shuffle( $result );

    // shuffle/randomize queried
    shuffle( $queried_post_ids );

    // loop through queried and add ids to results that aren't already in
    foreach( $queried_post_ids as $id ) {
      if ( ! in_array( $id, $result, true ) ) {
        array_push( $result , $id );
      }
    }

    // ship it!
    $THEME->render_masonry( $result );

  ?>

</div>

<?php get_footer(); ?>
