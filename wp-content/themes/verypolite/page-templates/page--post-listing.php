<?php

  /**
  *
  *   Template Name: Page [ Post Listing ]
  *   Filename: page--post-listing.php
  *
  */

  get_header();

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template
  $template = 'post-listing';
  $template_id = $THEME->get_unique_id("{$template}--");

  // ---------------------------------------- AOS
  $aos_id = $template_id . '--aos';
  $aos_delay = 250;
  $aos_delay_increment = 250;

  // ---------------------------------------- ACF
  $acf_settings = get_field( 'settings' ) ?: [];
  $acf_post_type = $acf_settings['post_type'] ?: [ 'post' ];
  $acf_post_limit = $acf_settings['limit'] ?: 1;
  $acf_layout = $acf_settings['layout'] ?: 'masonry';
  $acf_order = $acf_settings['order'] ?: 'DESC';

  // ---------------------------------------- WP Query
  $post_count = 0;
  $curated_posts = [];
  $sliced_posts = [];
  $queried_posts = [];
  $posts_to_show = 0;

  foreach ( $acf_curated_posts as $item ) {
    if ( isset($item['post']) && !empty($item['post']) ) {
      $post_count++;
      $curated_posts[] = $item['post'];
    }
  }

  // calculate remaining posts to show based on limit and number of specific posts added
  $posts_to_show = $acf_post_limit - $post_count;

  // Build WP Query
  $args = [
    'post_status'			  => [ 'publish' ],
    'order'             => 'ASC',
    'posts_per_page'    => $posts_to_show,
    'post_type'         => $acf_post_type,
  ];

  // exclude curated posts from WP Query
  if ( !empty( $curated_posts ) ) {
    $args['post__not_in'] = $curated_posts;
  }

  // get new Query
  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {

      // init data
      $query->the_post();

      // default post data
      $id = get_the_ID();

      // add post id to array of post ids
      $queried_posts[] = $id;

    }
  }

  // Restore original Post Data
  wp_reset_postdata();

  // get number of posts to 'slice' out of post_ids array
  $slice_size = 6;

  // if the number of posts returned is less than the slice value, reset slice value to half the total results
  if ( count( $queried_posts ) < $slice_size ) {
    $slice_size = ( count( $queried_posts ) / 2 );
  }

  // get array of sliced posts
  $sliced_posts = array_slice( $queried_posts, 0, $slice_size );

  // merge sliced posts with pre-selected posts
  $result = array_merge( $sliced_posts, $curated_posts );

  // shuffle/randomize results
  shuffle( $result );

  // shuffle/randomize queried
  shuffle( $queried_posts );

  // loop through queried and add ids to results that aren't already in
  foreach( $queried_posts as $id ) {
    if ( ! in_array( $id, $result, true ) ) {
      array_push( $result , $id );
    }
  }

?>

<div class="<?= $template; ?>" id="<?= $template; ?>">
  <?php
    switch ( $acf_layout ) {
      case 'masonry': {
        echo $THEME->render_masonry( $result );
        break;
      }
    }
  ?>
</div>

<?php get_footer(); ?>
