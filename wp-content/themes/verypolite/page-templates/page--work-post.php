<?php

  /**
  *
  *   Template Name: Page [ Work Post ]
  *   Filename: page--work-post.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Legacy Layout
  include( locate_template( "./snippets/legacy/post-hero.php" ) );
  include( locate_template( "./snippets/legacy/post-body.php" ) );
  include( locate_template( "./snippets/legacy/post-footer.php" ) );

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
