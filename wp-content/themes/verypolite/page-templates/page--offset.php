<?php

/*
*
*   Template Name: Page [ Offset ]
*   Filename: page--offset.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();


echo '<section class="section section--offset offset">';
  echo '<div class="offset__main">';
    echo $THEME->render_bs_container( 'open' );

      $args = array(
        'post_type'              => array( 'post' ),
      	'post_status'            => array( 'publish' ),
      	'posts_per_page'         => 1,
        'paged'                  => false,
        'category__in'           => [ 17 ],
      );

      $query = new WP_query ( $args );

      if ( $query->have_posts() ) {
        $count = 1;
        while ( $query->have_posts() ) {
          $query->the_post();
          echo $THEME->render_article_preview( get_the_ID(), [ 'count' => $count ] );
          $count++;
        }
      }

    echo $THEME->render_bs_container( 'close' );
  echo '</div>';
echo '</section>';

get_footer();

?>
