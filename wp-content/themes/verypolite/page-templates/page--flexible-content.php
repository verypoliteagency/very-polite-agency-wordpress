<?php

/*
*
*   Template Name: Page [ Flexbile Content ]
*	  Filename: page--flexible-content.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();

echo '<section class="section section--hero hero">';
  if ( get_the_title() ) {
    echo $THEME->render_bs_container( 'open', 'col-12' );
      echo '<h1 class="hero__title heading--primary heading--title">' . get_the_title() . '</h1>';
    echo $THEME->render_bs_container( 'close' );
  }
echo '</section>';

include( locate_template( "./snippets/layout--flexible-page.php" ) );

get_footer();

?>
