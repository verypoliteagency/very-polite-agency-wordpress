<?php

/*
*
* Template Name: Careers
*	Filename: single--careers.php
* Template Post Type: post
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
$theme_classes = $THEME->get_theme_classes();
$post_type = $THEME->get_theme_info('post_type');
$post_ID = $THEME->get_theme_info('post_ID');
$post_template = 'careers';
$content = get_field( 'content_careers' ) ? get_field( 'content_careers' ) : [];
$postings = get_field( 'postings' ) ? get_field( 'postings' ) : [];
$show_postings = get_field( 'show_postings' ) ? true : false;
$hr = get_field( 'hr' ) ? get_field( 'hr' ) : [];
$hr_html = isset( $hr['url'] ) ? '<div class="job-postings__hr"><img src="' . $hr['url'] . '" alt="Horizontal Rule" width="300" height="4" loading="lazy" /></div>' : '';

?>

<div id="post" class="post post--<?php echo $post_type; ?> post--careers" role="main">

  <?php include( locate_template( "./snippets/post--hero.php" ) ); ?>

  <div class="post__body">

    <?php

      //////////////////////////////////////////////////////////
  		////  Content
  		//////////////////////////////////////////////////////////

      if ( $content ) {
        echo '<section class="flexible-content">';
          echo $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1' );
            foreach( $content as $i => $block ) {

              $layout = isset($block['acf_fc_layout']) ? $block['acf_fc_layout'] : 'not-set';
              $heading = isset($block['heading']) ? $block['heading'] : false;
              $wysiwyg = isset($block['wysiwyg']) ? $block['wysiwyg'] : false;

              echo '<div class="flexible-content__block">';
                switch ( $layout ) {
                  case 'heading':
                    echo $heading ? '<h2 class="flexible-content__heading heading heading--2">' . $heading . '</h2>' : '';
                    break;
                  case 'wysiwyg':
                    echo $wysiwyg ? '<div class="flexible-content__wysiwyg wysiwyg rte">' . $wysiwyg . '</div>' : '';
                    break;
                }
              echo '</div>';

            }
          echo $THEME->render_bs_container( 'closed' );
        echo '</section>';
      }

      //////////////////////////////////////////////////////////
  		////  Job Postings
  		//////////////////////////////////////////////////////////

      echo '<section class="job-postings">';
        echo $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1' );
          echo $hr_html ? $hr_html : '';

            if ( $show_postings ) {

              //////////////////////////////////////////////////////////
              ////  Active Job Postings
              //////////////////////////////////////////////////////////

              foreach( $postings as $i => $posting ) {

                $job_id = isset($posting['job']) ? $posting['job'] : false;
                $job_title = get_the_title( $job_id ) ? get_the_title( $job_id ) : false;
                $job_description = get_field( 'description', $job_id ) ? get_field( 'description', $job_id ) : false;
                $job_skills = get_field( 'skills', $job_id ) ? get_field( 'skills', $job_id ) : false;;
                $job_responsibilities = get_field( 'responsibilities', $job_id ) ? get_field( 'responsibilities', $job_id ) : false;
                $email_subject = urlencode( "Hello! I'm interested in the " . $job_title . " position." );

                if ( $job_title && $job_description ) {

                  echo '<div class="job-postings__item" data-index="' . ( $i + 1 ) . '">';

                    echo $hr_html ? $hr_html : '';

                    echo '
                      <h2 class="job-postings__heading heading">' . $job_title . '</h2>
                      <div class="job-postings__message message rte">' . $job_description . '</div>
                    ';

                    if ( $job_skills ) {
                      echo '
                        <h4 class="job-postings__heading heading heading--tertiary">Skills + Qualifications:</h4>
                        <div class="job-postings__message message rte">' . $job_skills . '</div>
                      ';
                    }

                    if ( $job_responsibilities ) {
                      echo '
                        <h4 class="job-postings__heading heading heading--tertiary">Responsibilities:</h4>
                        <div class="job-postings__message message rte">' . $job_responsibilities . '</div>
                      ';
                    }

                    echo '
                      <h4 class="job-postings__heading heading heading--tertiary">Benefits:</h4>
                      <div class="job-postings__message message rte">
                        <ul>
                          <li>Extended health benefits</li>
                          <li>Wellness program</li>
                          <li>Polite co-workers</li>
                        </ul>
                      </div>
                      <h4 class="job-postings__heading heading heading--tertiary">Application:</h4>
                      <div class="job-postings__message message rte"><p>We’re excited to meet you! Send us an email introducing yourself, including your CV and any relevant work samples, to <a href="mailto:excuseme@weareverypolite.com?subject=' . $email_subject . '" target="_blank">excuseme@weareverypolite.com</a>, please and thank you.</p></div>
                    ';

                  echo '</div>';

                }

              }

            } else {

              //////////////////////////////////////////////////////////
              ////  No Job Postings
              //////////////////////////////////////////////////////////

              echo '
                <h2 class="job-postings__heading heading">Nothing Available?</h2>
                <div class="job-postings__message message rte"><p>We’re very sorry to say we don’t have any positions at this particular moment. But do check-in again soon! Please and thank you.</p></div>
              ';

            }

          echo $hr_html ? $hr_html : '';
        echo $THEME->render_bs_container( 'closed' );
      echo '</section>';

      //////////////////////////////////////////////////////////
  		////  Outro
  		//////////////////////////////////////////////////////////

      echo '<section class="outro">';
        echo '<div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1">';
          echo '<div class="section__message message rte">';
            echo '<p>Say hello anytime at <a href="mailto:excuseme@weareverypolite.com">excuseme@weareverypolite.com</a> and we’ll keep in touch.</p>';
            echo '<p>Please and thank you!</p>';
            echo '<p><span class="sans-serif">:)</span></p>';
          echo '</div>';
        echo '</div></div></div>';
      echo '</section>';

    ?>

  </div>
  <!-- /.post__body -->

  <div class="post__footer"></div>
  <!-- /.post__footer -->

</div>
<!-- /#post.<?php echo $post_type; ?> -->

<?php get_footer(); ?>
