<?php

/*
*
* Template Name: Agency Content
*	Filename: single--agency-content.php
* Template Post Type: post
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
$theme_classes = $THEME->get_theme_classes();
$post_type = $THEME->get_theme_info('post_type');
$post_ID = $THEME->get_theme_info('post_ID');

?>

<div id="post" class="post post--<?php echo $post_type; ?> post--agency-content" role="main">

  <?php include( locate_template( "./snippets/post--hero.php" ) ); ?>

  <div class="post__body">

    <?php

      //////////////////////////////////////////////////////////
  		////  Agency Content
  		//////////////////////////////////////////////////////////

  		$post_template = 'agency';
  		$section_count = 0;

      if ( have_rows( 'content_agency' ) ) {

        $section_count = count( get_field( 'content_agency' ) );

        while( have_rows( 'content_agency' ) ) {

          // init data
          the_row();

          // default data
          $inset = $margin_bottom = $fade_in = false;
          $section_classes = "section";

          // row data
          $section_row_index = get_row_index();
          $row_layout = get_row_layout();
          $inset = get_sub_field( 'inset' );
          $margin_bottom = get_sub_field( 'margin_bottom' );

          // build section classes
          $section_classes .= " section--" . $row_layout . " " . $row_layout;
          if ( $inset ) {
            $section_classes .= " section--inset";
          }
          if ( $margin_bottom ) {
            $section_classes .= " section--margin-bottom";
          }

          if ( "wysiwyg" == $row_layout && ( $section_count != $section_row_index ) ) {
            $fade_in = true;
          }

          echo '<section class="' . $section_classes . '" data-section-index="' . $section_row_index . '">';

            switch ( $row_layout ) {

              case 'heading':

                include( locate_template( './snippets/layout--heading.php' ) );
            		break;

              case 'horizontal_rule':

                include( locate_template( './snippets/layout--horizontal-rule.php' ) );
            		break;

              case 'media':

                include( locate_template( './snippets/layout--media.php' ) );
            		break;

            	case 'wysiwyg':

            	  include( locate_template( './snippets/layout--wysiwyg.php' ) );
            		break;

            }

          echo '</section>';

        }
      } else {
        echo '<!-- No Flexible Content -->';
      }

    ?>

  </div>
  <!-- /.post__body -->

  <div class="post__footer"></div>
  <!-- /.post__footer -->

</div>
<!-- /#post.<?php echo $post_type; ?> -->

<?php get_footer(); ?>
