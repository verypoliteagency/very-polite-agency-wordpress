<?php
	
/*
*		
* Template Name: Brand Me
*	Filename: single--brand-me.php
* Template Post Type: post
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
$theme_classes = $THEME->get_theme_classes();
$post_type = $THEME->get_theme_info('post_type');
 
?>

<div id="post" class="post post--<?php echo $post_type; ?> post--brand-me" role="main">
  
  <div class="post__hero">
    <div class="post__image post__image--hero with-container">
      <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me.svg" alt="Brand Me Contest Intro Graphic" />
    </div>
  </div>
 
  <div class="post__body">
     
    <section class="section section--wysiwyg wysiwyg section--margin-bottom">
      <div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1">
        <div class="section__message rte">
          <p>Hello, Brand Me submissions are now closed.</p>
          <p>It wasn’t easy, but we have selected our finalists and will be announcing the winner at the end of month! Thank you to each and every one of you that entered your Brand Me submission.</p>
          <p>It was our pleasure to learn about you.</p>
          <p>Very Polite</p>
        <div>
      </div></div></div>
    </section>
  
    <section class="section section--image section--margin-bottom">
      <div class="section__image with-container" data-node="1">  
        <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--section-01.svg" alt="Decorative" />
      </div>
    </section> 
  
  	<section class="section section--wysiwyg wysiwyg section--margin-bottom">
    	
      
    	
    	<div class="container"><div class="row">
      	
      	<div class="col-12 col-md-10 offset-md-1">
        	<div class="section__message rte">
            <h2>What is it?</h2>
            <p>Brand Me is a $150,000 emergency global branding fund created by Very Polite intended to help revitalize a selected brand that might be negatively impacted.</p>
            <p>In other words, a rare opportunity for us to rebrand, refresh, rework, develop, launch or relaunch your brand. You get the picture.</p>
          </div>
      	</div>
      	
    	</div></div>
    	
  	</section>
  	
  	<section class="section section--image section--margin-bottom"> 
      <div class="section__image with-container" data-node="2A">
        <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--section-02A.svg" alt="Brand Who? Brand You (Me)!" />
      </div>
      <div class="section__image" data-node="2B">
        <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--section-02B.svg" alt="Decorative" />
      </div>
    </section> 

    <section class="section section--wysiwyg wysiwyg section--margin-bottom">   
      <div class="container"><div class="row"> 
                 
        <div class="col-12 col-md-10 offset-md-1">        
          <div class="section__message rte">
            <h2>What do you win?</h2>
            <p>Aside from the euphoric feeling of being the envy of their friends, family and colleagues, the winner will receive the following services worth a gazillion dollars (well, 150k):</p>
          </div>
        </div>
        
        <div class="col-12 col-md-10 offset-md-1">
          
          <div class="section__image" data-node="2C">
            <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--section-02C.svg" alt="VPA--web-assets--brand-me--section-02C" />
          </div>
          
        </div>
        
      </div></div>
    </section>
         
    <section class="section section--image section--margin-bottom">      
      <div class="section__image with-container" data-node="2D">
        <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--section-02D.svg" alt="VPA--web-assets--brand-me--section-02C" />
      </div>  
    </section>     
         
    <section class="section section--wysiwyg wysiwyg section--margin-bottom">   
      <div class="container"><div class="row">     
                 
        <div class="col-12 col-md-10 offset-md-1">   
          <div class="section__message rte">
            <p>Businesses of all backgrounds from all over the world are eligible to enter Brand Me for a chance to receive this fully immersive Very Polite Branding package.</p>
      
            <p>We are looking for a brand that has something to say (you just might be struggling with the right way to express it). We are looking for innovators and future game-changers. Are you excited about what you are building? Why? Is your business going to disrupt or redefine an industry? Or have you been around for a while and need to pivot in order to lead once again? We want to know. Inspire us.</p>
          </div>
        </div>
      
      </div></div>
    </section>	

    <section class="section section--form form section--margin-bottom">
      <?php include( locate_template( "./snippets/form--brand-me.php" ) ); ?>
    </section>
    
    <section class="section section--image section--margin-bottom">
      <div class="section__image with-container" data-node="outro">
        <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--outro.svg" alt="VPA--web-assets--brand-me--form" />
      </div>
    </section>
    
     <section class="section section--wysiwyg wysiwyg section--margin-bottom">   
      <div class="container"><div class="row">     
                 
        <div class="col-12 col-md-10 offset-md-1">   
          <div class="section__message rte">
            <p style="text-align: center;">*<a href="/terms-conditions">Terms and Conditions</a> Apply*</p>
          </div>
        </div>
      
      </div></div>
    </section>	
    
  </div>
  <!-- /.post__body -->
  
  <div class="post__footer">
    <div class="post__masthead">
      <div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
        <div class="contributors">
          <h3 class="contributors__heading masthead__heading">Thanks/Credits:</h3>
          <div class="contributors__main message rte">
            <p>Illustrations: Spencer Pidgeon</p>
          </div>
        </div>
      </div></div></div>
    </div>
  </div>
  			
</div>
<!-- /#post.<?php echo $post_type; ?> -->

<?php get_footer(); ?>
