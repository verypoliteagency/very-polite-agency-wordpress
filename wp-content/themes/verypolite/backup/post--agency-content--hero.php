 <div class="post__hero">
    <div class="container">
      <div class="row">
        <div class="col-12">
          
          <?php
          
            //////////////////////////////////////////////////////////
        		////  Post Title
        		//////////////////////////////////////////////////////////
    
            if ( get_the_title() ) {
              echo '<h1 class="post__title">' . get_the_title() . '</h1>';
            }
            
            //////////////////////////////////////////////////////////
        		////  Post Categories
        		//////////////////////////////////////////////////////////
        		
        		// defaults
  					$post_categories = wp_get_post_categories( $post_ID );
  					$post_cats_parents = array();
  					$post_cats_parents_str = '';
  					$post_cats = array();
  					$post_cats_str = '';
  					
  					// loop through post categories
  					foreach ( $post_categories as $cat ) {
    					
  				    $category = get_category( $cat );
  				    
  				    $name = $category->name;
  				    $slug = $category->slug;
              $parent_id = $category->parent;
              
              if ( $name ) {
                array_push( $post_cats, $name );
              }
              
              if ( $parent_id ) {
                $parent_cat = get_the_category_by_ID( $parent_id );
                array_push( $post_cats_parents, $parent_cat );
              }
  					    
  					}
  					
  					// remove duplicates
  					$post_cats_parents = array_unique( $post_cats_parents );
  					$post_cats = array_unique( $post_cats );
  					
            // build parent cat string
            foreach ( $post_cats_parents as $index=>$parent ) {
              if(strlen(trim($post_cats_parents_str)) > 0){
                // not empty string
                $post_cats_parents_str .= ', ' . $parent;
              } else {
                // empty string
                $post_cats_parents_str = $parent;
              }
            }
            
            // build cat string
            foreach ( $post_cats as $index=>$cat ) {
              if(strlen(trim($post_cats_str)) > 0){
                // not empty string
                $post_cats_str .= ', ' . $cat;
              } else {
                // empty string
                $post_cats_str = $cat;
              }
            }
            
            if ( false ) { 
              if ( $post_cats_parents_str || $post_cats_str ) {
                echo '<ul class="taxonomy-list taxonomy-list--categories">';
                  if ( $post_cats_parents_str ) {
                    echo '<li class="taxonomy-list__item taxonomy-list__item--parent">' . $post_cats_parents_str . '</li>';
                  }
                  if ( $post_cats_str ) {
                    echo '<li class="taxonomy-list__item taxonomy-list__item--child">' . $post_cats_str . '</li>';
                  }              
                echo '</ul>';
              }
            }
                              
        		//////////////////////////////////////////////////////////
        		////  Post Tags
        		//////////////////////////////////////////////////////////
        		
        		$tags = $note = false;
        		$post_tags_str = '';
        		
        		if ( get_field( 'note' ) ) {
          		echo '<div class="post__note">';
          		  echo '<p>' . get_field( 'note' ) . '</p>';
          		echo '</div>';
          	} else {	
          		
          		if ( get_field( 'tags' )  ) {
            		
            		$tags = get_field( 'tags' );
            		
            		foreach ( $tags as $index=>$tag ) {          		
              	  if ( strlen( trim( $post_tags_str ) ) > 0 ) {
                    $post_tags_str .= ', ' . $tag->name;
                  } else {
                    $post_tags_str = $tag->name;
                  }	
            		}
            		
            		echo '<ul class="taxonomy-list taxonomy-list--tags">';
                  echo '<li class="taxonomy-list__item">' . $post_tags_str . '</li>';
                echo '</ul>';
              
          		}
          		
            }
        		
        		//////////////////////////////////////////////////////////
        		////  Post Graphic
        		//////////////////////////////////////////////////////////
        		
        		if ( have_rows( 'intro' ) ){
          		while( have_rows( 'intro') ) {
            		
            		// init data
            		the_row();
            		
            		// default data
            		$type = false;
            		$image = false;
            		
            		// get data
            		if ( get_sub_field ('type' ) ) {
              		
              		$type = get_sub_field ('type' );
              		
              		switch ( $type ) {
                    case 'static':
                      if ( get_sub_field( 'image_static' ) ) {
                        $image = get_sub_field( 'image_static' );
                      }
                      break;
                    case 'animated':
                      if ( get_sub_field( 'image_animated' ) ) {
                        $image = get_sub_field( 'image_animated' );
                      }
                      break;
                  }
                  
                  if ( $image ) {
                    echo '<div class="post__intro-graphic post__intro-graphic--' . $type . '">';
                      if ( "animated" == $type ) {
                        echo $image;
                      } else {
                        echo $THEME->lazyload ( 'inline', 'image', $image, false, false );
                      } 
                    echo '</div>';
                  }    
                    		
                }
                
          		} // endwhile
        		} // endif
        		
          ?>
    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.wrapper -->
    
  </div>
  <!-- /.post__hero -->
