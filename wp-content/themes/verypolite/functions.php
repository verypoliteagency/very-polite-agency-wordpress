<?php

/*
*
*	Filename: functions.php
*
*/

$includes = [
  "tools",
  "advanced-custom-fields",
  "image-sizes",
  "menus",
  "optimization",
  "custom-post-types",
  "custom-wysiwyg-formats",
  "polite-department-base",
  "polite-department-templates",
  "polite-department",
  "security",
  "utilities",
  "enqueue-scripts",
  "enqueue-styles",
];

foreach( $includes as $include ) {
  include_once( "functions/" . $include . ".php" );
}

function custom_default_blocks() {
  $post_type_object = get_post_type_object( "post" );
  $post_type_object->template = [
    [ "acf/hero" ],
    [ "acf/text" ],
    [ "acf/related-posts" ],
  ];
}

add_action( "init", "custom_default_blocks" );
