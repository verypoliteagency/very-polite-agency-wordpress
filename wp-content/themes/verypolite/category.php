<?php

/*
*
*	Filename: category.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////





//    $featured_post_id = 2202;
//     $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
//
//     $args = [
//       'post_type'             => [ 'post' ],
//       'post_status'           => [ 'publish' ],
//       'order'                 => 'DESC',
//       'orderby'               => 'date',
//       'nopaging'              => false,
//       'paged'                 => $paged,
//       'posts_per_page'        => -1,
//     ];
//
//     if ( !empty( $featured_post_id ) ) {
//       $args['post__not_in'] = [ $featured_post_id ];
//     }
//
//     if ( get_queried_object() ) {
//       $queried_object = get_queried_object();
//       $this_cat_id = $queried_object->cat_ID;
//       if ( $this_cat_id ) {
//         $args['category__in'] = [ $this_cat_id ];
//       }
//     }
//
//     // The Query
//     $query = new WP_Query( $args );
//
//     // The Loop
//     if ( $query->have_posts() ) {
//
//       $count = 1;
//
//     	while ( $query->have_posts() ) {
//
//     		$query->the_post();
//
//         $id = get_the_ID();
//         $title = get_the_title();
//
//         debug_this( [ $id, $title ] );
//
//         // echo $THEME->render_article_preview( $id, [ 'count' => $count ] );
//
//         $count++;
//
//     	}
//
//       if ( $query->max_num_pages > 1 )  {
//
//         $pagination_links = [];
//         $max_pages = $query->max_num_pages;
//         $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
//         $pagination_args = [
//           'base' => get_pagenum_link(1) . '%_%',
//           'format' => $pagination_format,
//           'total' => $max_pages,
//           'current' => $paged,
//           'aria_current' => false,
//           'show_all' => true,
//           'end_size' => 1,
//           'mid_size' =>2,
//           'prev_next' => false,
//           'prev_text' => '',
//           'next_text' => '',
//           'type' => 'array',
//           'add_args' => false,
//           'add_fragment' => '',
//           'before_page_number' => '',
//           'after_page_number' => ''
//         ];
//
//         $pagination_links = paginate_links( $pagination_args );
//
//         echo '<div class="blog__pagination pagination">';
//
//           echo '<div class="pagination__button pagination__button--prev">';
//             $button_label = '<span class="pagination__arrow prev">←</span>Prev';
//             if ( get_previous_posts_link( $button_label ) ) {
//               echo get_previous_posts_link( $button_label );
//             } else {
//               echo '<span class="pagination__no-link">' . $button_label . '</span>';
//             }
//           echo '</div>';
//
//           if ( $pagination_links && !empty( $pagination_links ) ) {
//             echo '<ul class="pagination__links">';
//             foreach( $pagination_links as $i => $link ) {
//               echo '<li class="pagination__links-item">' . $link . '</li>';
//             }
//             echo '</ul>';
//           }
//
//           echo '<div class="pagination__button pagination__button--next">';
//             $button_label = 'Next<span class="pagination__arrow next">→</span>';
//             if ( get_next_posts_link( $button_label, $max_pages ) ) {
//               echo get_next_posts_link( $button_label, $max_pages );
//             } else {
//               echo '<span class="pagination__no-link">' . $button_label . '</span>';
//             }
//           echo '</div>';
//
//         echo '</div>';
//
//       }
//
//     }
//
//     wp_reset_postdata();



//
//   $number_of_posts_per_page = 9;
//   $initial_offset = 1;
//   $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
//   // Use paged if this is not on the front page
//
//   $number_of_posts_past = $number_of_posts_per_page * ($paged - 1);
//   $offset = $initial_offset + (($paged > 1) ? $number_of_posts_past : 0);
//
// // WP_Query arguments
//   $args = array(
//     'post_type' => 'post',
//     'post_status' => 'publish',
//     'posts_per_page' => $number_of_posts_per_page,
//     'paged' => $paged,
//     'offset' => $offset
//   );
//
//   // the query
//   $query = new WP_Query( $args );
//
//     // start the loop
//     if ( $query->have_posts() ) {
//       the_title(sprintf('<h3><a href="%s" >', get_permalink()),'</a></h3>');
//     }
//
//   } // end of the loop
//
//   //display pagination
//   the_posts_navigation();
//
//   }
//   else {
//   // no posts found
//   }
//
// // restore the post data
// wp_reset_postdata();




$THEME = $THEME ?? new CustomTheme();


echo '<div class="category">';
  echo '<div class="category__main">';

    echo $THEME->render_bs_container( 'open' );

    //$featured_post_id = 2202;
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $query_args = [
      'post_type'             => [ 'post' ],
      'post_status'           => [ 'publish' ],
      'posts_per_page'        => 1,
      'paged'                 => $paged,
    ];

    if ( get_queried_object() ) {
      $queried_object = get_queried_object();
      $cat_id = $queried_object->cat_ID;
      if ( $cat_id ) {
        $query_args['category__in'] = [ $cat_id ];
      }
    }

     // The Query
    $the_query = new WP_Query( $query_args );

    // The Loop
    if ( $the_query->have_posts() ) {

      $count = 1;

    	while ( $the_query->have_posts() ) {

    		$the_query->the_post();

        $id = get_the_ID();
        $title = get_the_title();

        debug_this( [ 'count' => $count, 'id' => $id, 'title' => $title ] );

        // echo $THEME->render_article_preview( $id, [ 'count' => $count ] );

        $count++;

    	} // end while

      $fount_posts = $the_query->found_posts;
      $max_pages = $the_query->max_num_pages;

      next_posts_link( 'Older Entries', $max_pages );
      previous_posts_link( 'Newer Entries' );

    } // end if

    wp_reset_postdata();

    echo $THEME->render_bs_container( 'close' );

  echo '</div>';
echo '</div>';

get_footer();

?>
