<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

<head>

  <?php
    get_template_part( "snippets/google-tag-manager" );
    get_template_part( "snippets/google-tag" );
  ?>

	<title><?php wp_title(''); ?></title>

  <?php

    //////////////////////////////////////////////////////////
    ////  #TODOs
    //////////////////////////////////////////////////////////

    /*
      • Consolidate functions into PoliteDepartment Class
      • Consolidate lazyload parameters into single object
    */

    //////////////////////////////////////////////////////////
    ////  Theme Vars
    //////////////////////////////////////////////////////////

    $THEME = $THEME ?? new CustomTheme();
    $assets_dir = $THEME->get_theme_directory('assets');
    $theme_dir = $THEME->get_theme_directory();
    $theme_classes = $THEME->get_theme_classes();
    $is_longevity_club = is_singular([ 'post' ]) || is_page_template( 'page-templates/page--longevity-club.php' ) ? true : false;

    echo $THEME->render_preconnect_external_scripts([ 'https://cdn.jsdelivr.net', 'https://www.google-analytics.com' ]);

  ?>

  <link rel="apple-touch-icon" href="<?php echo $theme_dir; ?>/apple-touch-icon.png?v=<?php echo filemtime( get_template_directory() . '/apple-touch-icon.png' ); ?>">
  <link rel="shortcut icon" href="<?php echo $theme_dir; ?>/favicon.ico?v=<?php echo filemtime( get_template_directory() . '/favicon.ico' ); ?>">

  <meta charset="<?php echo get_bloginfo('charset'); ?>">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Very Polite">
  <meta http-equiv="Expires" content="7" />

  <?php

    // ---------------------------------------- Preload Fonts
    echo $THEME->render_preload_fonts([
      'AkkuratMonoLLWeb-Regular',
      'AkkuratMonoLLTT-Bold',
      'Bookerly-BoldItalic',
      'NotSorrySerif-Bold',
      'SorrySans-BoldExtended'
    ]);

    // ---------------------------------------- Search Engine Optimization (SEO)
    echo $THEME->render_seo();

		//////////////////////////////////////////////////////////
		////  Colour Theme
		//////////////////////////////////////////////////////////

		$colour_theme = 'white';
		if ( get_field( 'colour_theme' ) ) {
  		$colour_theme = get_field( 'colour_theme' );
		}
		if ( is_404() ) {
  		$colour_theme = 'black';
		}
		if ( have_rows( 'custom_classes' ) ) {
  		while ( have_rows( 'custom_classes' ) ) {

    		// init data
    		the_row();

    		// default data
    		$class = false;

    		if ( get_sub_field( 'class' ) ) {
      		$class = 'custom-class--' . clean_string( get_sub_field( 'class' ) );
      		$theme_classes .= ' ' . $class;
    		}

  		}
		}

    // ---------------------------------------- Supporting JavaScript
    get_template_part( "snippets/supporting-javascript" );

    // ---------------------------------------- WP Head Hook
    wp_head();

  ?>

</head>

<body class="<?php echo $theme_classes . ' colour-theme--' . $colour_theme; ?> updated" data-debugging="false" data-colour-theme='<?php echo $colour_theme; ?>'>

	<?php

    // ---------------------------------------- Site Vitals
		echo ( is_user_logged_in() || is_admin() ) ? $THEME->get_theme_info( 'vitals' ) : '';

    // ---------------------------------------- Longevity Club Spine
    if ( $is_longevity_club ) {
      get_template_part( "snippets/longevity-club/spine" );
		}

    // ---------------------------------------- Burger
    get_template_part( "snippets/burger" );

    // ---------------------------------------- Push Menu
    get_template_part( "snippets/push-menu" );

    // ---------------------------------------- Header
    get_template_part( "snippets/header" );

	?>

	<main class="<?php echo $theme_classes . ' colour-theme--' . $colour_theme; ?>" role="main">

    <?php
      // ---------------------------------------- Longevity Club Spine
      if ( $is_longevity_club ) {
        get_template_part( "snippets/longevity-club/monogram" );
		  }
    ?>
