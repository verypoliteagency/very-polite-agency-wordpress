<?php
	
/*
*	
*	Filename: search.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
 
?>

<div id="search" class="search" role="main">
			
	<?php if ( have_posts() ) : ?>
				
		<h2>Search Results For: <?php echo get_search_query();?></h2>
			
		<?php while ( have_posts() ) : the_post(); ?>
			
			<article>
				
				<div class="article__header">	
					<?php if ( get_the_title() ) : ?>				
						<h2 class="headline headline--beta"><?php the_title(); ?></h2>
					<?php endif; ?>
				</div>
				<!-- /.article__header -->
					
				<div class="article__main">
					<?php if ( get_the_excerpt() ) : ?>
						<div class="exceprt rte">
							<?php the_excerpt(); ?>	
						</div>
					<?php endif; ?>
				</div>
				<!-- /.article__main -->
					
				<div class="article__footer"></div>
				<!-- /.article__footer -->
					
			</article>
			
		<?php endwhile; ?>	
				
		<div class="pagination">
			<div class="prev"><?php echo get_previous_posts_link( 'Newer Entries' ); ?></div>
			<div class="next"><?php echo get_next_posts_link( 'Older Entries' ); ?></div>
		</div>
		<!-- /.pagination -->
				
	<?php else : ?>
							
	<?php endif; wp_reset_postdata(); ?>
			
</div>
<!-- /#search -->

<?php get_footer(); ?>
