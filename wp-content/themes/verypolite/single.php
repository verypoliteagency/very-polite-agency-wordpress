<?php

  /**
  *
  *	Filename: single.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'post';
  $template_id = $THEME->get_unique_id("{$template}--");

  // ---------------------------------------- Post Data
  $post_type = get_post_type($id);

  // ---------------------------------------- Template
  echo '<article class="' . $template . '" id="' . $template_id . '">';

    if ( have_posts() ) {
	    while ( have_posts() ) {

		    // init post content
		    the_post();

        // post content
        the_content();

	    }
    }

    switch ( $post_type ) {
      case 'work': {
        include( locate_template( "./snippets/post-templates/work.php" ) );
        break;
      }
    }

  echo '</article>';

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
