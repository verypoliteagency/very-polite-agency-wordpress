<?php

  /*
  *
  *	Filename: layout--button.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'button';
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- Content (ACF)
  $data = $args ?? [];

  $container = $data["container"] ?? "container";
  $image_type =  $data["image"]["type"] ?: "";
  $image_animated = $data["image"]["animated"] ?: "";
  $image_static = $data["image"]["static"] ?: [];
  $image_static_lazy = $THEME->render_lazyload_image([ "image" => $image_static ]);
  $image_static_rollover =  $data["image"]["static_rollover"] ?: [];
  $image_static_rollover_lazy = $THEME->render_lazyload_image([ "image" => $image_static_rollover ]);
  $inset = $data["inset"] ?: false;
  $link_external = $data["link"]["external"] ?? "";
  $link_internal = $data["link"]["internal"] ?? "";
  $link_target = "_self";
  $link_type = $data["link"]["type"] ?? "";

  // ---------------------------------------- Conditionals
  $cols = $inset ? "col-12 col-lg-8 offset-lg-2" : "col-12 col-lg-10 offset-lg-1";

  switch( $link_type ) {
    case 'external': {
      $link = $link_external;
      $link_target = "_blank";
      break;
    }
    case 'internal': {
      $link = $link_internal;
      break;
    }
    default: {
      $link = "";
      break;
    }
  }

?>

<div class="<?= $block_name; ?>">
  <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
    <div class="<?= $block_name; ?>__main">
      <a class="<?= $block_name; ?>__link link" href="<?= $link; ?>" target="<?= $link_target; ?>" title="Check this out!">

        <?php if ( "animated" === $image_type ) : ?>
          <?php if ( $image_animated ) : ?>
            <div class="<?= $block_name; ?>__image-animated"><?= $image_animated; ?></div>
          <?php endif; ?>
        <?php else : ?>
          <?php if ( $image_static_lazy ) : ?>
            <div class="<?= $block_name; ?>__image-static">
              <?= $image_static_lazy; ?>
              <?php if ( $image_static_rollover_lazy ) : ?>
                <div class="<?= $block_name; ?>__image-static-rollover">
                  <?= $image_static_rollover_lazy; ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
        <?php endif; ?>

      </a>
    </div>
  <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
</div>
