<?php

  /**
  *
  *	Filename: longevity-club-listing.php
  *
  */

  // ---------------------------------------- Theme Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'longevity-club';
  $template_id = $THEME->get_unique_id("{$template}--");

  // ---------------------------------------- ACF Data
  $acf_grid_gutter = get_field('grid_gutter') ?: 20;
  $acf_grid = get_field('grid') ?: 3;
  $acf_post_featured_id = get_field('post_featured_id') ?: false;
  $acf_post_type = get_field('post_type') ?: 'post';
  $acf_post_limit = get_field('post_limit') ?: 9;
  $acf_post_order = get_field('post_order') ?: 'DESC';

?>

<div class="<?= $template; ?>__listing" id="<?php echo esc_attr($template_id); ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>

    <?php if ( $acf_post_featured_id ) : ?>
      <div class="<?= $template; ?>__featured" data-grid-gutter="<?= $acf_grid_gutter; ?>">
        <?= $THEME->render_card_post([ 'id' => $acf_post_featured_id, 'style' => 'featured' ]); ?>
      </div>
    <?php endif; ?>

    <?php

      // Build WP Query
      $args = [
        'post_status'			  => [ 'publish' ],
        'order'             => $acf_post_order,
        'posts_per_page'    => $acf_post_limit,
        'post_type'         => $acf_post_type,
      ];

      // Exclude featured post
      if ( $acf_post_featured_id ) {
        $args['post__not_in'] = [ $acf_post_featured_id ];
      }

      // Get new Query
      $query = new WP_Query( $args );

    ?>

    <?php if ( $query->have_posts() ) : ?>
      <div class="<?= $template; ?>__grid grid grid--<?= $acf_grid; ?>" data-grid-gutter="<?= $acf_grid_gutter; ?>" role="list">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
          <?= $THEME->render_card_post([ 'id' => get_the_ID() ]); ?>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>

    <?php wp_reset_postdata(); ?>

  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>
