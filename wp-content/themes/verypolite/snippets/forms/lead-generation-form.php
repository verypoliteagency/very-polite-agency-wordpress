<?php

  /**
  *
  *   Lead Generation Form
  *
  */

  $THEME = $THEME ?? new CustomTheme();

  // ---------------------------------------- ACF Data
  $forms = get_field( 'form' ) ?: [];
  $action_url = $forms['action_url'] ?? '';
  $redirect_url = $forms['redirect_url'] ?? '';
  $opt_in_message = $forms['opt_in_message'] ?? 'Yes, please send me Very Polite info via email.';
  $submit_button_title = $forms['submit_button_title'] ?? 'Submit';

?>

<form
  action="<?= $action_url; ?>"
  class="form js--validate-me"
  data-redirect-url="<?= $redirect_url; ?>"
  data-form-type="lead-generation"
  enctype="multipart/form-data"
  method="POST"
>

  <div class="form__main body-copy--primary body-copy--md">

    <div class="form__row row">
      <div class="form__field field col-12 col-lg-6">
        <?= $THEME->render_form_input([
          'classes' => 'input--underlined',
          'name' => 'firstName',
          'placeholder' => 'First Name',
          'required' => true,
          'type' => 'text'
        ]); ?>
      </div>
      <div class="form__field field col-12 col-lg-6">
        <?= $THEME->render_form_input([
          'classes' => 'input--underlined',
          'name' => 'lastName',
          'placeholder' => 'Last Name',
          'required' => true,
          'type' => 'text'
        ]); ?>
      </div>
    </div>

    <div class="form__row row">
      <div class="form__field field col-12 col-lg-12">
        <?= $THEME->render_form_input([
          'classes' => 'input--underlined',
          'name' => 'email',
          'placeholder' => 'Email',
          'required' => true,
          'type' => 'email'
        ]); ?>
      </div>
    </div>

    <div class="form__row row">
      <div style="position: absolute; left: 99223300px;">
        <?=
          $THEME->render_form_input([
            'name' => 'rude',
            'type' => 'text',
          ]);
        ?>
        <input type="checkbox" name="_optin" value="yes" checked readonly tabindex="-1" />
        <input type="text" name="tags" value="very-polite-agency, newsletter" readonly tabindex="-1" />
        <input type="text" name="source" value="<?= $_SERVER['SERVER_NAME'] ?? 'source-not-available'; ?>" readonly tabindex="-1" />
      </div>
    </div>

    <?php if ( $opt_in_message ) : ?>
      <div class="form__row row">
        <div class="form__message body-copy--primary body-copy--sm col-12">
          <?= $opt_in_message; ?>
        </div>
      </div>
    <?php endif; ?>

    <div class="form__row row submit">
      <div class="form__field field col-12">
        <button class="form__button button--primary button--black" type="submit"><?= $submit_button_title; ?></button>
      </div>
    </div>

  </div>

  <div class="form__loading">
    <div class="form__spinner"></div>
  </div>

</form>
