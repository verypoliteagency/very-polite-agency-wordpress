<form
  action="https://formspree.io/f/xknagqaw"
  class="form form--newsletter-signup js--validate-me"
  data-redirect-url="<?= $redirect_url; ?>"
  data-form-type="lead-generation"
  enctype="multipart/form-data"
  method="POST"
>

  <div class="form__main">

    <div class="form__hero">
      <h2 class="form__heading">{{ heading }}</h2>
    </div>

    <div class="form__fields">
      <div class="form__row form__row--email">
        <div class="form__field">
          <input class="required" type="email" name="contact[email]" placeholder="Email Address...">
          <div class="form__error">Please enter a valid email address.</div>
        </div>
      </div>
      <div class="form__row form__row--rude">
        <div class="form__field">
          <label class="form__label">Rude</label>
          <input class="rude" type="text" name="contact[rude]" tabindex="-1" value="">
        </div>
      </div>
      <div class="form__action">
        <button class="form__button button" type="submit">Sign Up</button>
      </div>
    </div>

  </div>

</form>
