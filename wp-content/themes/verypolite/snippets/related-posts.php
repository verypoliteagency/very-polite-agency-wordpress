<?php

  /**
  *
  *   Related Articles
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'related-posts';
  $snippet_classes = $snippet_name;
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Layout
  $cols = 'col-12';
  $container = 'container';

  // ---------------------------------------- Content
  $acf_content = get_field('longevity_club', 'options') ?: [];
  $related_posts_container = $acf_content["related_posts_container"] ?? 'container';
  $related_posts_enable = $acf_content["related_posts_enable"] ?? false;
  $related_posts_gutter = $acf_content["related_posts_gutter"] ?? 0;
  $related_posts_heading = $acf_content["related_posts_heading"] ?? 'More stuff...';
  $related_posts_padding_bottom = $acf_content["related_posts_padding_bottom"] ?? 0;
  $related_posts_padding_top = $acf_content["related_posts_padding_top"] ?? 0;
  $related_posts = new WP_Query([
    'category__in'   => wp_get_post_categories( $id ),
    'posts_per_page' => 2,
    'post__not_in'   => [ $id ],
  ]);
  $related_posts_count = 1;

?>

<?php if ( $related_posts_enable && $related_posts->have_posts() ) : ?>

  <style data-block-id="<?= $snippet_name; ?>">

    <?=
      $THEME->render_element_styles([
        'background' => get_field('background'),
        'id' => $snippet_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top'),
        'text_colour' => get_field('text_colour'),
      ]);
    ?>

    <?php if ( $related_posts_gutter ) : ?>
      #<?= $snippet_id; ?> .<?= $snippet_name; ?>__grid {
        gap: <?= $related_posts_gutter; ?>px;
      }
    <?php endif; ?>

  </style>

  <section class="<?= esc_attr( $snippet_classes ); ?>" id="<?= esc_attr( $snippet_id ); ?>">
    <div class="<?= $snippet_name; ?>__main">
      <?= $THEME->render_bs_container( 'open', $cols, $related_posts_container ); ?>
        <div class="<?= $snippet_name; ?>__main-content">

          <?php if ( $related_posts_heading ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); ?>
            <strong class="<?= $snippet_name; ?>__heading heading--primary heading--lg" <?= $aos_attrs; ?>><?= $related_posts_heading; ?></strong>
          <?php endif; ?>

          <div class="<?= $snippet_name; ?>__grid grid grid--2" role="list">
            <?php while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
              <div class="<?= $snippet_name; ?>__grid-item grid__item">
                <?= $THEME->render_card_post([ 'count' => $related_posts_count, 'id' => get_the_ID() ]); ?>
              </div>
              <?php $related_posts_count++; ?>
            <?php endwhile; ?>
          </div>

        </div>
      <?= $THEME->render_bs_container( 'closed', $cols, $related_posts_container ); ?>
    </div>
  </section>

<?php endif; wp_reset_postdata(); ?>
