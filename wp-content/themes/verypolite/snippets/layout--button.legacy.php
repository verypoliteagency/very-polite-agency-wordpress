<?php

/*
*
*	Filename: layout--button.php
*
*/



$args = [
  'inset' => true,
  'link' => get_sub_field( 'link' ),
  'image' => get_sub_field( 'image' ),
];

echo $THEME->render_button( $args );





  // default data
//   $html = '';
//   $block_name = 'button';
//   $columns = 'col-12';
//   $image = $inset = $link = false;
//
//   extract( $params );
//
//   $columns .= ( $inset ) ? ' col-lg-8 offset-lg-2' : ' col-lg-10 offset-lg-1';
//
//   if ( $image && $link ) {
//     $html .= '<div class="' . $block_name . '">';
//       $html .= $this->render_bs_container( 'open', $columns );
//         $html .= '<div class="' . $block_name . '__main">';
//
//           // $link defaults
//           $url = false;
//           $external = $internal = $type = false;
//           $target = '_self';
//           $rel = '';
//
//           // extract $link
//           extract( $link );
//
//           switch( $type ) {
//             case 'external':
//               $url = $external;
//               $rel = 'noopener';
//               $target = '_blank';
//               break;
//             case 'internal':
//               $url = $internal;
//               break;
//           }
//
//           $html .= '<a
//             class="' . $block_name . '__link"
//             href="' . $url . '"
//             target="' . $target . '"
//             ' . ( $rel ? 'rel="' . $rel . '"' : '' ) . '
//           >';
//
//             // $image defaults
//             $type = false;
//             $animated = $static = $static_rollover = false;
//
//             // extract $link
//             extract( $image );
//
//             switch( $type ) {
//               case 'animated':
//                 $html .= ( $animated ) ? '<div class="' . $block_name . '__animated">' . $animated . '</div>' : '';
//                 break;
//               case 'static':
//                 $args = [ 'delay' => 0 ];
//                 if ( $static ) {
//                   $args['image'] = $static;
//                   $html .= '<div class="' . $block_name . '__image static">' . $this->render_lazyload_image( $args ) . '</div>';
//                 }
//                 if ( $static ) {
//                   $args['image'] = $static_rollover;
//                   $html .= '<div class="' . $block_name . '__image rollover">' . $this->render_lazyload_image( $args ) . '</div>';
//                 }
//                 break;
//             }
//
//           $html .= '</a>';
//
//
//         $html .= '</div>';
//       $html .= $this->render_bs_container( 'close' );
//     $html .= '</div>';
//   }
//
//   return $html;


?>
