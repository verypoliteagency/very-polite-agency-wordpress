<?php

  /**
  *
  *   Block Quote
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'wysiwyg';
  $snippet_classes = $snippet_name;
  $snippet_id = is_int(get_row_index()) && get_row_index() > -1 ?
    "{$snippet_name}--" . get_row_index() : $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Snippet Data
  $cols = 'col-12';
  $container = 'container';

  // ---------------------------------------- Content (ACF)
  $cta = get_sub_field("cta") ?: [];
  $inset = get_sub_field("inset") ?: "";
  $wysiwyg = get_sub_field("wysiwyg") ?: "";

  // ---------------------------------------- Conditionals
  $cols .= $inset ? " col-lg-8 offset-lg-2" : " col-lg-10 offset-lg-1";

?>

<?php if ( $wysiwyg ) : ?>
  <div class="<?= esc_attr( $snippet_classes ); ?>" id="<?= esc_attr( $snippet_id ); ?>">
    <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
      <div class="<?= $snippet_name; ?>__main">
        <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); ?>
        <div class="<?= $snippet_name; ?>__message body-copy--primary body-copy--xs" <?= $aos_attrs; ?>><?= $wysiwyg; ?></div>
        <?php if ( $wysiwyg ) : $THEME->render_cta( $cta ); endif; ?>
      </div>
    <?= $THEME->render_bs_container( 'close', $cols, $container ); ?>
  </div>
<?php endif; ?>
