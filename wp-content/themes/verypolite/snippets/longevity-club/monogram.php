<?php

  /**
  *
  *   Longevity Club Monogram
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'monogram';
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;

  // $THEME->render_svg([ 'type' => 'longevity.monogram' ]);

?>

<div class="<?= esc_attr( $block_classes ); ?>">
  <div class="<?= $block_name; ?>__icon">
    <img src="<?= $THEME->get_theme_directory('assets'); ?>/img/VPA--longevity-club-monogram.gif" alt="Longevity Club Logos" width="200" height="140" />
  </div>
</div>
