<?php

  /**
  *
  *   Longevity Club Spine
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'spine';
  $block_classes = $block_name;
  $block_id = $THEME->get_unique_id("{$block_name}--");

  // ---------------------------------------- AOS
  $aos_id = $block_id . '--aos';
  $aos_delay = 250;
  $aos_increment = 250;

?>

<div class="<?= esc_attr( $block_classes ); ?>">
  <div class="<?= $block_name; ?>__main" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__barcode">
      <?= $THEME->render_svg([ 'type' => 'longevity.barcode' ]); ?>
    </div>
    <h2 class="<?= $block_name; ?>__title heading--longevity-club">Longevity Club</h2>
  </div>
</div>

