<?php

  /**
  *
  *   Masthead
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "masthead";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id . "--aos";
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- ACF | Settings
  $colour_theme = $block_data["colour_theme"] ?? "";
  $container = $block_data["container"] ?? "";
  $padding_bottom = $block_data["padding_bottom"] ?? 0;
  $padding_top = $block_data["padding_top"] ?? 0;

  // ---------------------------------------- ACF | Content
  $cols = "col-12";
  $heading = get_field("heading") ?: "";
  $masthead = get_field("masthead") ?: "";
  $special_thanks = get_field("special_thanks") ?: "";
  $width = get_field("width") ?: "standard";

  switch ( $width ) {
    case "compact": {
      $cols .= " col-lg-8 offset-lg-2";
      break;
    }
    case "standard": {
      $cols .= " col-lg-10 offset-lg-1";
      break;
    }
  }

?>

<style>

  #<?= $block_id; ?> {
    padding-top: calc(<?= $padding_top; ?>px * 0.75);
    padding-bottom: calc(<?= $padding_bottom; ?>px  * 0.75);
  }

  @media screen and (min-width: 992px) {
    #<?= $block_id; ?> {
      padding-top: <?= $padding_top; ?>px;
      padding-bottom: <?= $padding_bottom; ?>px;
    }
  }

</style>

<?php if ( $masthead || $special_thanks ) : ?>
  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-colour-theme="<?= $colour_theme; ?>">
    <div class="<?= $block_name; ?>__main" id="<?= $aos_id; ?>">
      <div class="<?= $block_name; ?>__content">
        <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
          <?php if ( $heading ) : $aos = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); $aos_delay += $aos_increment; ?>
            <strong class="<?= $block_name; ?>__heading body-copy--primary body-copy--md" <?= $aos; ?>><?= $heading; ?></strong>
          <?php endif; ?>
          <?php if ( $masthead ) : $aos = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); $aos_delay += $aos_increment; ?>
            <div class="<?= $block_name; ?>__masthead body-copy--primary body-copy--md" <?= $aos; ?>><?= $masthead; ?></div>
          <?php endif; ?>
          <?php if ( $special_thanks ) : $aos = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>
            <div class="<?= $block_name; ?>__special-thanks body-copy--primary body-copy--md" <?= $aos; ?>>Thanks: <?= $special_thanks; ?></div>
          <?php endif; ?>
        <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
      </div>
    </div>
  </section>
<?php endif; ?>
