<?php

  /**
  *
  *   Featured Media
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "media";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- ACF | Settings
  $cols = "col-12";
  $container = $block_data["container"] ?? "full-width";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- ACF | Content
  $media = get_field("media") ?: [];
  $media_aspect_ratio = get_field("media_aspect_ratio") ?: "";
  $media_border_radius = get_field("media_border_radius");
  $media_carousel_id = "{$block_id}--carousel";
  $media_carousel_mobile = get_field("media_carousel_mobile") ?: false;
  $media_count = count($media);
  $media_gutter = get_field("media_gutter");

  $media_carousel_mobile_enabled = $media_carousel_mobile && $media_count > 1 ? true : false;

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top")
      ]);
    ?>
    <?php if ( $media_aspect_ratio ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__item {
        aspect-ratio: <?= $media_aspect_ratio; ?>;
      }
    <?php endif; ?>
    <?php if ( $media_gutter ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__grid {
        gap: <?= $media_gutter; ?>px;
      }
    <?php endif; ?>
    <?php if ( $media_border_radius ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__item img {
        border-radius: <?= $media_border_radius; ?>px;
      }
    <?php endif; ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ "block_name" => $block_name, "id" => get_field("anchor") ]); ?>
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>

        <?php if ( $media_carousel_mobile_enabled ) : ?>
          <div
            class="<?= $block_name; ?>__carousel glide js--glide d-md-none"
            id="<?= $media_carousel_id; ?>"
            data-glide-animation-duration="350"
            data-glide-autoplay="3200"
            data-glide-gap="<?= $media_gutter; ?>"
            data-glide-style="<?= $block_name; ?>"
          >
            <div class="glide__track" data-glide-el="track">
              <ul class="glide__slides">
                <?php foreach ( $media as $index => $item ) : ?>

                  <?php

                    $item_aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay * ( $index + 1 ), "offset" => $aos_offset, "transition" => "fade-up" ]);
                    $item_image = $item["image"] ?? [];
                    $item_lottie = $item["lottie"] ?? "";
                    $item_type = $item["type"] ?? "";

                    switch ( $item_type ) {
                      case "image": {
                        $item_html = $THEME->render_lazyload_image([ "image" => $item_image ]);
                        break;
                      }
                      case "lottie": {
                        $item_html = $THEME->render_lottie([ "source" => $item_lottie ]);
                        break;
                      }
                      default: {
                        $item_html ="";
                        break;
                      }
                    }

                  ?>

                  <?php if ( $item_html ) : ?>
                    <li class="glide__slide">
                      <div class="<?= $block_name; ?>__item <?= $item_type; ?>" <?= $item_aos_attrs; ?>><?= $item_html; ?></div>
                    </li>
                  <?php endif; ?>

                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        <?php endif; ?>

        <div class="<?= $block_name; ?>__grid grid grid--<?= $media_count; ?><?= $media_carousel_mobile_enabled ? " d-none d-md-grid" : "" ?>">
            <?php foreach ( $media as $index => $item ) : ?>

              <?php

                $item_aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay * ( $index + 1 ), "offset" => $aos_offset, "transition" => "fade-up" ]);
                $item_image = $item["image"] ?? [];
                $item_lottie = $item["lottie"] ?? "";
                $item_type = $item["type"] ?? "";

                switch ( $item_type ) {
                  case "image": {
                    $item_html = $THEME->render_lazyload_image([ "image" => $item_image ]);
                    break;
                  }
                  case "lottie": {
                    $item_html = $THEME->render_lottie([ "source" => $item_lottie ]);
                    break;
                  }
                  default: {
                    $item_html ="";
                    break;
                  }
                }

              ?>

              <?php if ( $item_html ) : ?>
                <div class="<?= $block_name; ?>__item <?= $item_type; ?>" <?= $item_aos_attrs; ?>><?= $item_html; ?></div>
              <?php endif; ?>

            <?php endforeach; ?>
          </div>

      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
