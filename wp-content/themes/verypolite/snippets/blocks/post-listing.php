<?php

  /**
  *
  *   Post Listing
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "post-listing";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = "col-12";
  $container = $block_data["container"] ?? "full-width";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- Content (ACF)
  $background = get_field("background") ?: "white";
  $border_radius = get_field("border_radius") ?: 0;
  $featured_post = get_field("featured_post") ?: false;
  $featured_post_id = $featured_post->ID ?? false;
  $grid_columns = get_field("grid_columns");
  $grid_gutter = get_field("grid_gutter") ?: 0;
  $layout = get_field("layout") ?: "masonry";
  $limit = get_field("limit") ?: 50;
  $order = get_field("order") ?: "DESC";
  $order_by = get_field("order_by") ?: "date";
  $padding_bottom = get_field("padding_bottom") ?: 0;
  $padding_top = get_field("padding_top") ?: 0;
  $post_count = 1;
  $post_type = get_field("post_type") ?: "post";
  $posts = [];
  $text_colour = get_field("text_colour") ?: "black";

  // ---------------------------------------- Content (WP Query)
  $query_args = [
    "post_status"			  => [ "publish" ],
    "order"             => $order,
    "orderby"           => $order_by,
    "posts_per_page"    => $limit,
    "post_type"         => $post_type,
  ];

  if ( $featured_post_id ) {
    $query_args["post__not_in"] = [ $featured_post_id ];
  }

  $query = new WP_Query( $query_args );
  $query_cats = [];

  while ( $query->have_posts() ) {

    $query->the_post();
    $post_id = get_the_ID();
    $post_cats = get_the_category($post_id) ?: [];

    foreach ( $post_cats as $cat ) {
      $query_cats[$cat->term_id] = [ "name" => $cat->name, "slug" => $cat->slug ];
    }

  }

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "background" => get_field("background"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>

    #<?= $block_id; ?> {
      --block--post-listing--border-radius: <?= $border_radius; ?>px;
      --block--post-listing--gap: <?= $grid_gutter; ?>px;
    }

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ "block_name" => $block_name, "id" => get_field("anchor") ]); ?>
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>

        <?= $THEME->render_card_post([ "classes" => "{$block_name}__featured-post", "id" => $featured_post_id, "style" => "featured", "text_colour" => get_field("text_colour") ]); ?>

        <?php if ( $featured_post_id ) : ?><span class="<?= $block_name; ?>__hr"></span><?php endif; ?>

        <?php if ( $query->have_posts() ) : ?>
          <?php switch( $layout ): case "grid": ?>

            <div class="<?= $block_name; ?>__grid grid grid--<?= $grid_columns; ?>" id="<?= $block_id; ?>--grid">
              <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <div class="<?= $block_name; ?>__grid-item grid__item">
                  <?php
                    switch ( $post_type ) {
                      case "post": {
                        echo $THEME->render_card_post([ "count" => $post_count, "id" => get_the_ID(), "text_colour" => get_field("text_colour") ]);
                        break;
                      }
                      case "work": {
                        echo $THEME->render_card_work([ "count" => $post_count, "id" => get_the_ID(),  ]);
                        break;
                      }
                    }
                  ?>
                </div>
                <?php $post_count++; ?>
              <?php endwhile; ?>
            </div>
            <?php break; ?>

            <?php case "masonry": ?>
              <div class="<?= $block_name; ?>__masonry-grid masonry-grid" id="<?= $block_id; ?>--masonry-grid">

                <?php if ( !empty($query_cats) && false ) : ?>
                  <div class="masonry-grid__filter">
                    <button class="masonry-grid__filter-button button button--primary js--masonry-filter-button is-selected" data-filter="all">All</button>
                    <?php foreach ( $query_cats as $id => $cat ) : ?>
                      <button class="masonry-grid__filter-button button button--primary js--masonry-filter-button" data-filter="<?= $cat["slug"]; ?>"><?= $cat["name"]; ?></button>
                    <?php endforeach; ?>
                  </div>
                <?php endif; ?>

                <div class="masonry-grid__container" id="<?= $block_id; ?>--masonry-grid-container">
                  <div class="masonry-grid__sizer"></div>
                  <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                    <?php
                      $post_id = get_the_ID();
                      $post_cats = get_the_category($post_id) ?: [];
                      $item_classes = "masonry-grid__item";
                      foreach ( $post_cats as $cat ) {
                        $item_classes .= " {$cat->slug}";
                      }
                    ?>

                    <div class="<?= $item_classes; ?>">
                      <?php
                        switch ( $post_type ) {
                          case "post": {
                            echo $THEME->render_card_post([ "count" => $post_count, "id" => get_the_ID() ]);
                            break;
                          }
                          case "work": {
                            echo $THEME->render_card_work([ "count" => $post_count, "id" => get_the_ID() ]);
                            break;
                          }
                        }
                      ?>
                    </div>
                    <?php $post_count++; ?>
                  <?php endwhile; ?>
                </div>
              </div>
            <?php break; ?>

          <?php endswitch; ?>
        <?php endif; wp_reset_postdata(); ?>

      <?= $THEME->render_bs_container( "closed", "col-12", $container ); ?>
    </div>
  </section>

<?php endif; ?>

