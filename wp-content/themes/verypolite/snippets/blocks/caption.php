<?php

  /**
  *
  *   Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "caption";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Block Data
  $cols = "col-12";
  $container = $block_data["container"] ?? "container";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- ACF | Content
  $cols = "col-12";
  $caption = get_field("caption_content") ?: "";
  $caption_size = get_field("caption_size");
  $caption_style = get_field("caption_style");

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top")
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ "block_name" => $block_name, "id" => get_field("anchor") ]); ?>
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>

        <?php if ( $caption ) : ?>
          <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-down" ]); ?>
          <div class="<?= $block_name; ?>__content caption--<?= $caption_style; ?> caption--<?= $caption_size; ?>" <?= $aos_attrs; ?>><?= $caption; ?></div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
