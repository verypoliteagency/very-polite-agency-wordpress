<?php

  /**
  *
  *   Showcase
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'showcase';
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id . "--aos";
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- ACF | Settings
  $colour_theme = $block_data["colour_theme"] ?? "";
  $container = $block_data["container"] ?? "";
  $padding_bottom = $block_data["padding_bottom"] ?? 0;
  $padding_top = $block_data["padding_top"] ?? 0;

  // ---------------------------------------- ACF | Content
  $carousel_id = $block_id . "--glide";
  $items = get_field("items") ?? [];
  $items_count = count($items) ?? 0;
  $options = get_field("options") ?? [];

?>

<style>

  #<?= $block_id; ?> {
    padding-top: calc(<?= $padding_top; ?>px * 0.75);
    padding-bottom: calc(<?= $padding_bottom; ?>px  * 0.75);
  }

  @media screen and (min-width: 992px) {
    #<?= $block_id; ?> {
      padding-top: <?= $padding_top; ?>px;
      padding-bottom: <?= $padding_bottom; ?>px;
    }
  }

</style>

<?php if ( !empty($items) ) : ?>
  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-colour-theme="<?= $colour_theme; ?>">
    <div class="<?= $block_name; ?>__main <?= "full-width" === $container ? $container : ""; ?>" id="<?= $aos_id; ?>">
      <?= $THEME->render_bs_container( "open", "col-12", $container ); ?>

        <div
          class="<?= $block_name; ?>__carousel glide js--glide"
          id="<?= $carousel_id; ?>"
          data-glide-animation-duration="<?= $options["duration"] ?? 500; ?>"
          data-glide-autoplay="<?= $options["delay"] ?? 2500; ?>"
          data-glide-gap="<?= $options["gap"] ?? 0; ?>"
          data-glide-style="<?= $block_name; ?>"
        >
          <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
              <?php foreach ( $items as $index => $item ) : ?>

                <?php
                  $image = $item["image"] ?? [];
                  $image_desktop = $image["desktop"] ?? [];
                  $image_mobile = $image["mobile"] ?? [];
                  $link = $item["link"] ?? [];
                  $link_type = $link["type"] ?? "not-set";
                  $link_url = $link[$link_type] ?? "";
                  $link_target = "inernal" === $link_type ? "_self" : "_blank";
                  $title = $item["title"] ?? "";
                  $block_name
                ?>

                <?php if ( $item["enable"] ?? false ) : ?>
                  <li class="glide__slide" data-title="<?= $title; ?>">
                    <div class="<?= $block_name; ?>__item js--auto-height">
                      <?= $link_url ? "<a class='{$block_name}__link link' href='{$link_url}' title='{$title}' target='{$link_target}'>" : ""; ?>
                        <?= $THEME->render_lazyload_image([ "classes" => "d-block d-lg-none", "image" => $image_mobile ]); ?>
                        <?= $THEME->render_lazyload_image([ "classes" => "d-none d-lg-block", "image" => $image_desktop ]); ?>
                      <?= $link_url ? "</a>" : ""; ?>
                    </div>
                  </li>
                <?php endif; ?>

              <?php endforeach; ?>
            </ul>
          </div>
        </div>

      <?= $THEME->render_bs_container( "closed", "col-12", $container ); ?>
    </div>
  </section>
<?php endif; ?>
