<?php

  /**
  *
  *   Featured Video
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "featured-video";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id . "--aos";
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- ACF | Settings
  $colour_theme = $block_data["colour_theme"] ?? "";
  $container = $block_data["container"] ?? "";
  $padding_bottom = $block_data["padding_bottom"] ?? 0;
  $padding_top = $block_data["padding_top"] ?? 0;

  // ---------------------------------------- ACF | Content
  $video_aspect_ratio = get_field("aspect_ratio") ?: "16/9";
  $video_id = get_field("id") ?: "";
  $video_type = get_field("type") ?: "vimeo";

?>

<style>

  #<?= $block_id; ?> {
    padding-top: calc(<?= $padding_top; ?>px * 0.75);
    padding-bottom: calc(<?= $padding_bottom; ?>px  * 0.75);
  }

  #<?= $block_id; ?> .<?= $block_name; ?>__iframe {
    aspect-ratio: <?= $video_aspect_ratio; ?>;
  }

  @media screen and (min-width: 992px) {
    #<?= $block_id; ?> {
      padding-top: <?= $padding_top; ?>px;
      padding-bottom: <?= $padding_bottom; ?>px;
    }
  }

</style>

<?php if ( $video_id && $video_type ) : ?>
  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-colour-theme="<?= $colour_theme; ?>">
    <div class="<?= $block_name; ?>__main <?= "full-width" === $container ? $container : ""; ?>" id="<?= $aos_id; ?>">

      <div class="<?= $block_name; ?>__media" <?= $THEME->render_aos_attrs([ "anchor" => $aos_id, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>>
        <?= $THEME->render_bs_container( "open", "col-12", $container ); ?>
          <div class="<?= $block_name; ?>__iframe iframe">
            <?= $THEME->render_lazyload_iframe([ "id" => $video_id, "type" => $video_type ]); ?>
          </div>
        <?= $THEME->render_bs_container( "closed", "col-12", $container ); ?>
      </div>

    </div>
  </section>
<?php endif; ?>
