<?php

  /**
  *
  *   Caption
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "call-to-action";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id . "--aos";
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");
  $aos = $THEME->render_aos_attrs([ "anchor" => $aos_id, "offset" => $aos_offset, "transition" => "fade-left" ]);

  // ---------------------------------------- ACF | Settings
  $colour_theme = $block_data["colour_theme"] ?? "";
  $container = $block_data["container"] ?? "";
  $padding_bottom = $block_data["padding_bottom"] ?? 0;
  $padding_top = $block_data["padding_top"] ?? 0;

  // ---------------------------------------- ACF | Content
  $cols = "col-12";
  $text = get_field("text") ?: "";
  $text_extra = get_field("text_extra") ?: [];
  $links = get_field("links") ?: [];
  $width = get_field("width") ?: "standard";

  switch ( $width ) {
    case "compact": {
      $cols .= " col-lg-8 offset-lg-2";
      break;
    }
    case "standard": {
      $cols .= " col-lg-10 offset-lg-1";
      break;
    }
  }

?>

<style>

  #<?= $block_id; ?> {
    padding-top: calc(<?= $padding_top; ?>px * 0.75);
    padding-bottom: calc(<?= $padding_bottom; ?>px  * 0.75);
  }

  @media screen and (min-width: 992px) {
    #<?= $block_id; ?> {
      padding-top: <?= $padding_top; ?>px;
      padding-bottom: <?= $padding_bottom; ?>px;
    }
  }

</style>

<?php if ( $text || !empty($links) ) : ?>
  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-colour-theme="<?= $colour_theme; ?>">
    <div class="<?= $block_name; ?>__main" id="<?= $aos_id; ?>">
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
        <div class="<?= $block_name; ?>__layout">
          <?php if ( $text || !empty($text_extra) ) : ?>
            <div class="<?= $block_name; ?>__content">
              <div class="<?= $block_name; ?>__text body-copy--primary body-copy--md"><?= $text; ?></div>
              <div class="<?= $block_name; ?>__text-extra"><?= $THEME->render_lazyload_image([ "image" => $text_extra ]); ?></div>
            </div>
          <?php endif; ?>
          <?php if ( !empty($links) ) : ?>
            <div class="<?= $block_name; ?>__links">
              <?php foreach ( $links as $index => $item ) : ?>
                <?php
                  $item["block_name"] = $block_name . "__link";
                  $item["link_block_name"] = "button";
                ?>
                <?= $THEME->render_cta( $item ); ?>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        </div>
      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>
<?php endif; ?>
