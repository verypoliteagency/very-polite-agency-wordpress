<?php

  /**
  *
  *   Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "text";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = "col-12";
  $container = $block_data["container"] ?? "container";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- Content (ACF)
  $body_copy = get_field("body_copy_content") ?: "";
  $body_copy_size = get_field("body_copy_size");
  $body_copy_style = get_field("body_copy_style");

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">
    <?=
      $THEME->render_element_styles([
        "background" => get_field("background"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>
  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ "block_name" => $block_name, "id" => get_field("anchor") ]); ?>
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content">
          <?php if ( $body_copy ) : ?>
            <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>
            <div class="<?= $block_name; ?>__message body-copy--<?= $body_copy_style; ?> body-copy--<?= $body_copy_size; ?>" <?= $aos_attrs; ?>><?= $body_copy; ?></div>
          <?php endif; ?>
        </div>
      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
