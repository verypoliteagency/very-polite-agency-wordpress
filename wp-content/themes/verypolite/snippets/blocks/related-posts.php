<?php

  /**
  *
  *   Related Posts
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "related-posts";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = "col-12";
  $container = $block_data["container"] ?? "full-width";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- Content (ACF)
  $aspect_ratio = get_field("aspect_ratio") ?: "";
  $border_radius = get_field("border_radius") ?: 0;
  $gutter = get_field("gutter") ?: 0;
  $heading = get_field("heading_content") ?: "You might also like...";
  $heading_size = get_field("heading_size") ?: "md";
  $heading_style = get_field("heading_style") ?: "primary";

  // ---------------------------------------- Content (WP Query)
  $related_posts = new WP_Query([
    "category__in"   => wp_get_post_categories( $id ),
    "posts_per_page" => 2,
    "post__not_in"   => [ $id ],
  ]);

  // ---------------------------------------- Conditionals
  $block_classes .= $aspect_ratio ? " with-aspect-ratio" : "";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "background" => get_field("background"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>

    <?php if ( $aspect_ratio ) : ?>
      #<?= $block_id; ?> .card-post__featured-image,
      #<?= $block_id; ?> .card-post__featured-image-link {
        aspect-ratio: <?= $aspect_ratio; ?>;
      }
    <?php endif; ?>

    #<?= $block_id; ?> {
      --block--related-posts--border-radius: <?= $border_radius; ?>px;
      --block--related-posts--gap: <?= $gutter; ?>px;
    }

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content">

          <?php if ( $related_posts->have_posts() ) : $related_posts_count = $related_posts->post_count; ?>

            <span class="<?= $block_name; ?>__hr"></span>

            <?php if ( $heading ) : ?>
              <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>
              <strong class="<?= $block_name; ?>__heading heading--<?= $heading_style; ?> heading--<?= $heading_size; ?>" <?= $aos_attrs; ?>><?= $heading; ?></strong>
            <?php endif; ?>

            <div class="<?= $block_name; ?>__grid grid grid--2" role="list">
              <?php while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
                <div class="<?= $block_name; ?>__grid-item grid__item">
                  <?= $THEME->render_card_post([ "count" => $related_posts_count, "id" => get_the_ID(), "text_colour" => get_field("text_colour") ]); ?>
                </div>
                <?php $related_posts_count++; ?>
              <?php endwhile; ?>
            </div>

          <?php endif; wp_reset_postdata(); ?>

        </div>
      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
