<?php

  /**
  *
  *   Featured Animation
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "featured-animation";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id . "--aos";
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- ACF | Settings
  $colour_theme = $block_data["colour_theme"] ?? "";
  $container = $block_data["container"] ?? "";
  $padding_bottom = $block_data["padding_bottom"] ?? 0;
  $padding_top = $block_data["padding_top"] ?? 0;

  // ---------------------------------------- ACF | Content
  $lottie = get_field("lottie") ?: [];

?>

<style>

  #<?= $block_id; ?> {
    padding-top: calc(<?= $padding_top; ?>px * 0.75);
    padding-bottom: calc(<?= $padding_bottom; ?>px  * 0.75);
  }

  @media screen and (min-width: 992px) {
    #<?= $block_id; ?> {
      padding-top: <?= $padding_top; ?>px;
      padding-bottom: <?= $padding_bottom; ?>px;
    }
  }

</style>

<?php if ( $lottie ) : ?>
  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>" data-colour-theme="<?= $colour_theme; ?>">
    <div class="<?= $block_name; ?>__main <?= "full-width" === $container ? $container : ""; ?>" id="<?= $aos_id; ?>">

      <div class="<?= $block_name; ?>__media" <?= $THEME->render_aos_attrs([ "anchor" => $aos_id, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>>
        <?= $THEME->render_bs_container( "open", "col-12", $container ); ?>
          <?= $THEME->render_lottie([ "source" => $lottie ]); ?>
        <?= $THEME->render_bs_container( "closed", "col-12", $container ); ?>
      </div>

    </div>
  </section>
<?php endif; ?>
