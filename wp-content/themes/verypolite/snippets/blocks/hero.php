<?php

  /**
  *
  *   Hero
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = 'hero';
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = 'col-12';
  $container = $block_data['container'] ?? 'container';
  $enable = $block_data['enable'] ?? false;

  // ---------------------------------------- Content (ACF)
  $excerpt_show = get_field('excerpt_show') ?: false;
  $excerpt_size = get_field('excerpt_size') ?: "md";
  $excerpt_style = get_field('excerpt_style') ?: "primary";
  $graphic = get_field('graphic') ?: [];
  $hero_border_radius = get_field('hero_border_radius');
  $style = get_field('style') ?: 'post';

  // ---------------------------------------- Content (Post)
  $categories = get_the_category( $id ) ?: [];
  $excerpt = get_the_excerpt($id) ?: '';
  $featured_image = $THEME->get_featured_image_by_post_id($id);
  $title = get_the_title() ?: 'Title Not Set';

  // ---------------------------------------- Content (Post Date)
  date_default_timezone_set('America/Los_Angeles');
  $date_today = new DateTimeImmutable();
  $date_published = get_post_datetime( $id, 'date', 'local' );
  $date_difference = $date_published->diff($date_today);
  $date_difference_days = $date_difference->format('%a');
  $post_is_new = $date_difference_days <= 30 ? true : false;

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        'background' => get_field('background'),
        'id' => $block_id,
        'padding_bottom' => get_field('padding_bottom'),
        'padding_top' => get_field('padding_top'),
        'text_colour' => get_field('text_colour'),
      ]);
    ?>

    <?php if ( $hero_border_radius ) : ?>
      #<?= $block_id; ?> .<?= $block_name; ?>__featured-image img {
        border-radius: <?= $hero_border_radius; ?>px;
      }
    <?php endif; ?>

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_anchor([ 'block_name' => $block_name, 'id' => get_field('anchor') ]); ?>
      <?= $THEME->render_bs_container( 'open', $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content <?= $style; ?>">

        <?php switch ( $style ) : case 'page' : ?>

          <!-- Hero Page -->
            <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]); ?>
            <h1 class="<?= $block_name; ?>__heading heading--primary heading--title" <?= $aos_attrs; ?>><?= $title; ?></h1>
            <?php break; ?>

          <!-- Hero Post -->
          <?php case 'post' : ?>
            <?php if ( !empty($categories) ) : ?>
              <?php
                $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]);
                $aos_delay += $aos_increment;
              ?>
              <div class="<?= $block_name; ?>__categories" <?= $aos_attrs; ?>><?= $THEME->render_categories([ 'categories' => $categories ]); ?></div>
            <?php endif; ?>
            <?php
              $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]);
              $aos_delay += $aos_increment;
            ?>
            <h1 class="<?= $block_name; ?>__heading heading--primary heading--post-title text--uppercase" <?= $aos_attrs; ?>><?= $title; ?></h1>
            <?php if ( $excerpt_show && $excerpt ) : ?>
              <?php
                $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]);
                $aos_delay += $aos_increment;
              ?>
              <div class="<?= $block_name; ?>__excerpt body-copy--<?= $excerpt_style; ?> body-copy--<?= $excerpt_size; ?>" <?= $aos_attrs; ?>><?= $excerpt; ?></div>
            <?php endif; ?>
            <?php if ( $featured_image ) : ?>
              <div class="<?= $block_name; ?>__feature">
                <?php
                  $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-up" ]);
                  $aos_delay += $aos_increment;
                ?>
                <div class="<?= $block_name; ?>__featured-image" <?= $aos_attrs; ?>>
                  <?= $THEME->render_lazyload_image([ 'image' => $featured_image ]); ?>
                </div>
                <?php if ( $post_is_new ) : ?>
                  <?php
                    $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => 'fade-right' ]);
                    $aos_delay += $aos_increment;
                  ?>
                  <div class="<?= $block_name; ?>__featured-image-badge" <?= $aos_attrs; ?>>
                    <?= $THEME->render_svg([ 'type' => 'longevity.badge.new' ]); ?>
                  </div>
                <?php endif; ?>
                <time class="<?= $block_name; ?>__date-published js--post-date-published body-copy--primary body-copy--sm" datetime="<?= $date_published->format('Y-m-d'); ?>" itemprop="datePublished"><?= $date_published->format('F d, Y'); ?></time>
              </div>
            <?php endif; ?>
            <?php break; ?>

          <!-- Hero Work -->
          <?php case 'work' : ?>

            <?php
              $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-left" ]);
              $aos_delay += $aos_increment;
            ?>
            <h1 class="<?= $block_name; ?>__heading heading--primary heading--lg text--uppercase" <?= $aos_attrs; ?>><?= $title; ?></h1>

            <?php
              $graphic_type = $graphic['type'] ?? 'image';
              switch ( $graphic_type ) {
                case 'image': {
                  $image = $graphic['image'] ?? [];
                  $graphic_html = $THEME->render_lazyload_image([ 'image' => $image ]);
                  break;
                }
                case 'lottie': {
                  $graphic_html = '';
                  break;
                }
                case 'svg': {
                  $graphic_html = '';
                  break;
                }
                case 'video': {
                  $graphic_html = '';
                  break;
                }
                default: {
                  $graphic_html = '';
                  break;
                }
              }
            ?>
            <?php if ( $graphic_html ) : ?>
              <div class="<?= $block_name; ?>__graphic"><?= $graphic_html; ?></div>
            <?php endif; ?>
            <?= $THEME->render_post_tags_by_id([ 'classes' => 'hero__tags body-copy--primary body-copy--md', 'id' => $id ]); ?>
            <?php break; ?>

        <?php endswitch; ?>

      <?= $THEME->render_bs_container( 'closed', $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
