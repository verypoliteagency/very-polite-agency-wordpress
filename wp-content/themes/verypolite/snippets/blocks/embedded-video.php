<?php

  /**
  *
  *   Text
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "embedded-video";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = "col-12";
  $container = $block_data["container"] ?? "full-width";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- Content (ACF)
  $embed_controls = get_field("embed_controls") ?: false;
  $embed_id = get_field("embed_id") ?: "";
  $embed_source = get_field("embed_source");
  $embed_aspect_height = get_field("embed_aspect_height") ?: "";
  $embed_aspect_width = get_field("embed_aspect_width") ?: "";

  // ---------------------------------------- Conditionals
  $block_classes .= " {$block_name}--{$embed_source}";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "background" => get_field("background"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>

    #<?= $block_id; ?> .<?= $block_name; ?>__main-content {
      aspect-ratio: <?= $embed_aspect_width; ?>/<?= $embed_aspect_height; ?>
    }

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <div class="<?= $block_name; ?>__main">
      <?= $THEME->render_bs_container( "open", $cols, $container ); ?>
        <div class="<?= $block_name; ?>__main-content">
          <?=
            $THEME->render_lazyload_embed([
              "controls" => $embed_controls,
              "duration" => 750,
              "id" => $embed_id,
              "preload" => true,
              "source" => $embed_source
            ]);
          ?>
        </div>
      <?= $THEME->render_bs_container( "closed", $cols, $container ); ?>
    </div>
  </section>

<?php endif; ?>
