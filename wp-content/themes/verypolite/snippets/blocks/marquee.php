<?php

  /**
  *
  *   Marquee
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $block_name = "marquee";
  $block_classes = "{$block_name} block block--{$block_name}";
  $block_data = $block["data"] ?? [];
  $block_id = isset($block["id"]) && !empty($block["id"]) ? "{$block_name}--{$block["id"]}" : $block_name;

  // ---------------------------------------- AOS
  $aos_id = $block_id;
  $aos_delay = 175;
  $aos_increment = 150;
  $aos_offset = get_field("aos_offset");

  // ---------------------------------------- Layout
  $cols = "col-12";
  $container = $block_data["container"] ?? "full-width";
  $enable = $block_data["enable"] ?? false;

  // ---------------------------------------- Content (ACF)
  $marquee_direction = get_field("marquee_direction");
  $marquee_duration = get_field("marquee_duration");
  $marquee_gap = get_field("marquee_gap");
  $marquee_items = get_field("marquee_items") ?: [];
  $marquee_size = get_field("marquee_size");
  $marquee_style = get_field("marquee_style");

  // ---------------------------------------- Conditionals
  $block_classes .= " {$block_name}--{$marquee_direction}";
  $block_classes .= " {$block_name}--{$marquee_style}";
  $block_classes .= " {$block_name}--{$marquee_size}";

?>

<?php if ( $enable ) : ?>

  <style data-block-id="<?= $block_name; ?>">

    <?=
      $THEME->render_element_styles([
        "background" => get_field("background"),
        "id" => $block_id,
        "padding_bottom" => get_field("padding_bottom"),
        "padding_top" => get_field("padding_top"),
        "text_colour" => get_field("text_colour"),
      ]);
    ?>

    #<?= $block_id; ?> {
      --marquee-duration: <?= $marquee_duration; ?>s;
      --marquee-gap: <?= $marquee_gap; ?>px;
    }

  </style>

  <section class="<?= esc_attr( $block_classes ); ?>" id="<?= esc_attr( $block_id ); ?>">
    <?php $aos_attrs = $THEME->render_aos_attrs([ "anchor" => $aos_id, "delay" => $aos_delay, "offset" => $aos_offset, "transition" => "fade-up" ]); ?>
    <div class="<?= $block_name; ?>__main" <?= $aos_attrs; ?>>

      <?php if ( !empty($marquee_items) ) : ?>
        <div class="<?= $block_name; ?>__group">
          <?php for ( $x = 0; $x <= 10; $x++ ) : ?>
            <?php if ( 0 === $x ) : ?>
              <?= $THEME->render_marquee_items([ "hidden" => false, "items" => $marquee_items ]); ?>
            <?php else : ?>
              <?= $THEME->render_marquee_items([ "hidden" => true, "items" => $marquee_items ]); ?>
            <?php endif; ?>
          <?php endfor; ?>
        </div>
        <div class="<?= $block_name; ?>__group" aria-hidden="true">
          <?php for ( $x = 0; $x <= 10; $x++ ) : ?>
            <?= $THEME->render_marquee_items([ "hidden" => false, "items" => $marquee_items ]); ?>
          <?php endfor; ?>
        </div>
      <?php endif; ?>

    </div>
  </section>

<?php endif; ?>
