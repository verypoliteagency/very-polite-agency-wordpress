<?php

  //////////////////////////////////////////////////////////
  ////  Theme Vars
  //////////////////////////////////////////////////////////

  $THEME = $THEME ?? new CustomTheme();
  $home = $THEME->get_theme_directory('home');
  $assets_dir = $THEME->get_theme_directory('assets');
  $theme_dir = $THEME->get_theme_directory();

  // ---------------------------------------- ACF Data
  $acf_social = get_field( 'social', 'options' ) ?: [];
  $acf_company = get_field( 'company', 'options' ) ?: [];
  $summary = $acf_company['summary'] ?? '';

  //////////////////////////////////////////////////////////
  ////  Snippet Vars
  //////////////////////////////////////////////////////////

  $block_name = 'push-menu';

  if ( have_rows( 'push_menu', 'options' ) ) {
    while ( have_rows( 'push_menu', 'options' ) ) {

      // default data
      the_row();

      echo '<div class="' . $block_name . '" data-colour-theme="black">';
        echo '<div class="' . $block_name . '__container">';

          //////////////////////////////////////////////////////////
          ////  Main
          //////////////////////////////////////////////////////////

          echo '<div class="' . $block_name . '__main">';

            if ( have_rows( 'main_links' ) ) {
              echo '<nav class="' . $block_name . '__main-navigation navigation navigation--main">';
                while ( have_rows( 'main_links' ) ) {

                    // init data
                    the_row();

                    // default data
                    $active = $link = $link_id = $title = $type = false;
                    $target = '_blank';
                    $current_id = get_the_ID();

                    // get data
                    if ( get_sub_field( 'title' ) ) {
                      $title = get_sub_field( 'title' );
                    }
                    if ( get_sub_field( 'type' ) ) {
                      $type = get_sub_field( 'type' );
                    }

                    switch ( $type ) {
                      case 'external':
                        if ( get_sub_field( 'link_external' ) ) {
                          $link = get_sub_field( 'link_external' );
                        }
                        break;
                      case 'file':
                        if ( get_sub_field( 'link_file' ) ) {
                          $link = get_sub_field( 'link_file' );
                          $link = $link['url'];
                        }
                        break;
                      case 'internal':
                        if ( get_sub_field( 'link_internal' ) ) {
                          $link_id = get_sub_field( 'link_internal' );
                          $current_id = get_the_ID();
                          $target = '_self';
                          if ( $current_id == $link_id ) {
                            $active = true;
                          }
                          $link = get_permalink( $link_id );
                        }
                        break;
                    }

                    if ( $link && $title ) {
                      echo '<div class="' . $block_name . '__main-navigation-item navigation__item ' . ( $active ? 'active' : '' ) . '" data-current-id="' . $current_id . '" data-link-id="' . $link_id . '">';
                        echo '<a class="' . $block_name . '__main-navigation-link navigation__link link ' . ( $active ? 'active' : '' ) . '" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
                      echo '</div>';
                    }

                  }
              echo '</nav>';
            }

            if ( $summary ) {
              echo '<div class="' . $block_name . '__main-message body-copy--primary body-copy--xs">';
                echo $summary;
                echo '<p class="d-lg-none">We help brands forge lasting relationships with consumers across a network of media-technology platforms using targeted content. We think differently, with a focus on emotional resonance, connection and responsiveness.</p>';
                echo '<p>Thank you very much.</p>';
              echo '</div>';
            }

          echo '</div>';

          //////////////////////////////////////////////////////////
          ////  Footer
          //////////////////////////////////////////////////////////

          if ( have_rows( 'footer_links' ) ) {
            echo '<div class="' . $block_name . '__footer">';

              if ( $acf_social ) {
                echo '<div class="' . $block_name . '__footer-social">';
                  foreach ( $acf_social as $i => $item ) {

                    $type = $item['type'] ?? 'not-set';
                    $link = $item['link'] ?? '';
                    $link_title = get_bloginfo('name') . ucfirst($type);

                    if ( 'email' === $type ) {
                      $link = 'mailto:' . $link;
                    }

                    if ( $type && $link ) {
                      echo '<div class="' . $block_name  . '__footer-social-item ' . $type . '">';
                        echo '<a class="' . $block_name  . '__footer-social-link link" href="' . $link . '" target="_blank" title="' . $link_title . '">';
                          echo $THEME->render_svg([ 'type' => 'icon.' . $type ]);
                        echo '</a>';
                      echo '</div>';
                    }

                  }
                echo '</div>';
              }

              echo '<nav class="' . $block_name . '__footer-navigation navigation navigation--footer">';
                while ( have_rows( 'footer_links' ) ) {

                  // init data
                  the_row();

                  // default data
                  $active = $link = $link_id = false;
                  $target = '_blank';
                  $current_id = get_the_ID();

                  // get data
                  $title = get_sub_field( 'title' ) ?: '';
                  $type = get_sub_field( 'type' ) ?: '';

                  switch ( $type ) {
                    case 'external':
                      $link = get_sub_field( 'link_external' ) ?: '';
                      break;
                    case 'file':
                      $link = get_sub_field( 'link_file' ) ?: [];
                      $link = $link['url'] ?? '';
                      break;
                    case 'internal':
                      if ( get_sub_field( 'link_internal' ) ) {
                        $link_id = get_sub_field( 'link_internal' );
                        $current_id = get_the_ID();
                        $target = '_self';
                        if ( $current_id == $link_id ) {
                          $active = true;
                        }
                        $link = get_permalink( $link_id );
                      }
                      break;
                  }

                  if ( $link && $title ) {
                    echo '<div class="' . $block_name . '__footer-navigation-item navigation__item ' . ( $active ? 'active' : '' ) . '" data-current-id="' . $current_id . '" data-link-id="' . $link_id . '">';
                      echo '<a class="' . $block_name . '__footer-navigation-link navigation__link link body-copy--xs ' . ( $active ? 'active' : '' ) . '" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
                    echo '</div>';
                  }

                }
              echo '</nav>';

            echo '</div>';
          }

        echo '</div>';
      echo '</div>';

    }
  }

?>
