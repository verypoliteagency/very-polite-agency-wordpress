<?php

  /*
  *
  *	Filename: layout--testimonials.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'testimonials';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Content (ACF)
  $acf_heading = get_sub_field( 'heading' ) ?: '';
  $acf_list = get_sub_field( 'list' ) ?: [];

?>

<div class="<?= $snippet_name; ?>" id="<?= $snippet_id; ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', 'container' ); ?>
    <div class="<?= $snippet_name; ?>__main">

      <?php if ( $acf_heading ) : ?>
        <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay += $aos_increment; ?>
        <h2 class="<?= $snippet_name; ?>__heading" <?= $aos_attrs; ?>><?= $acf_heading; ?></h2>
      <?php endif; ?>

       <?php if ( !empty($acf_list) ) : ?>
        <div class="<?= $snippet_name; ?>__list body-copy--primary body-copy--xs">
          <?php foreach ( $acf_list as $i => $item ) : ?>

            <?php
              $item_aos_id = "{$aos_id}--{$i}";
              $item_aos_delay = $aos_delay;
              $item_aos_increment = 150;
              $client = $item['client'] ?? '';
              $message = $item['message'] ?? '';
            ?>

            <?php if ( $client && $message ) : ?>
              <div class="<?= $snippet_name; ?>__item" id="<?= $item_aos_id; ?>">

                <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                <div class="<?= $snippet_name; ?>__message heading--3" <?= $aos_attrs; ?>><?= $message; ?></div>

                <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-up' ]); ?>
                <div class="<?= $snippet_name; ?>__client body-copy--2 body-copy--xs" <?= $aos_attrs; ?>>– <?= $client; ?></div>

              </div>
            <?php endif; ?>

          <?php endforeach; ?>
        </div>
      <?php endif; ?>

    </div>
  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>
