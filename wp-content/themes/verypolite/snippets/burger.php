<?php

  /**
  *
  *	Filename: burger.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Block
  $snippet_name = 'burger';
  $snippet_classes = $snippet_name;
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  $home = $THEME->get_theme_directory('home');
  $assets_dir = $THEME->get_theme_directory('assets');
  $theme_dir = $THEME->get_theme_directory();
  $disable_burger = true;
  if ( is_front_page() ) {
    $disable_burger = false;
  }

?>

<div class="<?= $snippet_name; ?>">
  <button class="<?= $snippet_name; ?>__icon" type="button" aria-label="Burger">
    <div class="<?= $snippet_name; ?>__icon-box">
      <span class="<?= $snippet_name; ?>__icon-stroke top"></span>
      <span class="<?= $snippet_name; ?>__icon-stroke bottom"></span>
    </div>
  </button>
</div>
