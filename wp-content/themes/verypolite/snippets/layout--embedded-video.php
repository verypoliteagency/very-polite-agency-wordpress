<?php

/*
*
*	Filename: layout--embedded-video.php
*
*/

$THEME = $THEME ?? new CustomTheme();

$args = [
  'aspect_ratio' => get_sub_field( 'aspect_ratio' ),
  'caption' => get_sub_field( 'caption' ),
  'id' => get_sub_field( 'id' ),
  'inset' => get_sub_field( 'inset' ),
  'margin_bottom' => get_sub_field( 'margin_bottom' ),
  'placeholder' => get_sub_field( 'placeholder' ),
  'type' => get_sub_field( 'type' ),
  'vimeo_id' => get_sub_field( 'vimeo_id' ),
];

echo $THEME->render_embedded_video( $args );

?>
