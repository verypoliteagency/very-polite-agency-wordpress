<?php

/*
*
*	Filename: layout--flexible-page.php
*
*/

if ( have_rows( "flex_content" ) ) {

  echo '<div class="flexible-content flexible-content--post">';

    while ( have_rows( "flex_content" ) ) {

      the_row();

      $row_index = get_row_index();
      $row_layout = get_row_layout();
      $margin_bottom = get_sub_field( 'margin_bottom' );

      echo '<section
        class="section section--' . $row_layout . '"
        data-row-layout="' . $row_layout . '"
        data-row-index="' . $row_index . '"
        ' . ( $margin_bottom ? 'data-margin-bottom=""' : '' ) . '
      >';

        switch ( $row_layout ) {
          case "button": {
            $data = [];
            $data["inset"] = true;
            $data["link"] = get_sub_field("link") ?: [];
            $data["image"] = get_sub_field("image") ?: [];
            get_template_part( "snippets/layout--{$row_layout}", null, $data );
        		break;
          }
          case 'days-left': {
            include( locate_template( './snippets/layout--days-left.php' ) );
        		break;
          }
          case 'heading': {
            include( locate_template( './snippets/layout--heading.php' ) );
        		break;
          }
          case 'media': {
            include( locate_template( './snippets/layout--media-row.php' ) );
        		break;
          }
          case 'video': {
            include( locate_template( './snippets/layout--embedded-video.php' ) );
        		break;
          }
        	case 'wysiwyg': {
        	  include( locate_template( './snippets/layout--wysiwyg.php' ) );
        		break;
          }
          default: {
            echo "<h2>Oops! It looks like there's no template for this content yet :(</h2>";
            break;
          }
        }

      echo '</section>';

    }

  echo '</div>';

}

?>
