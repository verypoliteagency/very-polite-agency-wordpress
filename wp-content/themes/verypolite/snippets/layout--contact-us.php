<?php

  /*
  *
  *	Filename: layout--contact-us.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'contact-us';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $acf_company = get_field( 'company', 'options' ) ?: [];
  $acf_company_email = $acf_company['email'] ?? '';
  $acf_heading = get_sub_field( 'heading' ) ?: '';
  $acf_locations = get_sub_field( 'locations' ) ?: [];
  $acf_locations_count = count(array_filter( $acf_locations, function( $item ) {
    return 'publish' === $item['location']->post_status;
  }));

?>

<div class="<?= $snippet_name; ?>" id="<?= $snippet_id; ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', 'container' ); ?>

    <?php if ( $acf_heading ) : ?>
      <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); $aos_delay += $aos_increment; ?>
      <h2 class="<?= $snippet_name; ?>__heading heading--primary heading--4" <?= $aos_attrs; ?>><?= $acf_heading; ?></h2>
    <?php endif; ?>

    <?php if ( !empty($acf_locations) ) : ?>
      <div class="<?= $snippet_name; ?>__locations">
        <?php foreach ( $acf_locations as $i => $item ) : ?>
          <?php if ( 'publish' === $item['location']->post_status ) : ?>

            <?php
              $item_aos_id = "{$aos_id}--{$i}";
              $item_aos_delay = $aos_delay;
              $item_aos_increment = 150;
              $location_id = $item['location']->ID ?? 0;
              $location_address = get_field( 'address', $location_id ) ?: [];
              $location_address_city = $location_address['city'] ?? '';
              $location_address_human = $THEME->render_address( $location_address, 'human-full' );
              $location_address_directions = $THEME->render_address( $location_address, 'directions' );
              $location_phone = get_field( 'phone', $location_id ) ?: '';
            ?>

            <div class="<?= $snippet_name; ?>__location body-copy--primary body-copy--xs" id="<?= $item_aos_id; ?>">

              <?php if ( $location_address_city ) : ?>
                <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                <strong class="<?= $snippet_name; ?>__location-city uppercase" <?= $aos_attrs; ?>><?= $location_address_city; ?></strong>
              <?php endif; ?>

              <?php if ( $location_address_human ) : ?>
                <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                <span class="<?= $snippet_name; ?>__location-address" <?= $aos_attrs; ?>>
                  <a class="<?= $snippet_name; ?>__location-link link" href="<?= $location_address_directions; ?>" target="_blank" title="Find Us"><?= $location_address_human; ?></a>
                </span>
              <?php endif; ?>

              <?php if ( $location_phone ) : ?>
                <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                <span class="<?= $snippet_name; ?>__location-phone" <?= $aos_attrs; ?>>
                  <a class="<?= $snippet_name; ?>__location-link link" href="tel:<?= $location_phone; ?>" target="_self" title="Call Us"><?= $location_phone; ?></a>
                </span>
              <?php endif; ?>

              <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-up' ]); ?>
              <span class="<?= $snippet_name; ?>__location-cta" <?= $aos_attrs; ?>>
                <a class='button button--primary button--black' href='mailto:<?= $acf_company_email; ?><?= rawurlencode("?subject=Hello {$location_address_city}"); ?>' target="_blank" title="Email Us">
                  <span class="button__title">Say Hi</span>
                </a>
              </span>

            </div>

          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>
