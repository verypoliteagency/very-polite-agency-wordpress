<?php

  /*
  *
  *	Filename: layout--services.php
  *
  */

 // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'services';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Content (ACF)
  $acf_heading = get_sub_field( 'heading' ) ?: '';
  $acf_list = get_sub_field( 'list' ) ?: [];
  $acf_list_count = count(array_filter( $acf_list, function( $item ) {
    return $item['enable'] ?? false;
  }));
  $acf_list_remainder = $acf_list_count % 2;

?>

<div class="<?= $snippet_name; ?>" id="<?= $snippet_id; ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', 'container' ); ?>
    <div class="<?= $snippet_name; ?>__main">

      <?php if ( $acf_heading ) : ?>
        <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-up' ]); $aos_delay += $aos_increment; ?>
        <h2 class="<?= $snippet_name; ?>__heading heading--primary heading--2" <?= $aos_attrs; ?>><?= $acf_heading; ?></h2>
      <?php endif; ?>

      <?php if ( !empty($acf_list) ) : ?>
        <div class="<?= $snippet_name; ?>__list">
          <?php foreach ( $acf_list as $i => $item ) : ?>

            <?php
              $item_aos_id = "{$aos_id}--{$i}";
              $item_aos_delay = $aos_delay;
              $item_aos_increment = 150;
              $item_enable = $item['enable'] ?? false;
              $item_heading = $item['heading'] ?? '';
              $item_image = $item['image'] ?? [];
              $item_message = $item['message'] ?? '';
            ?>

            <?php if ( $item_enable ) : ?>
              <div class="<?= $snippet_name; ?>__item" id="<?= $item_aos_id; ?>">

                <?php if ( $item_image ) : ?>
                  <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-up' ]); $item_aos_delay += $item_aos_increment; ?>
                  <div class="<?= $snippet_name; ?>__item-image" <?= $aos_attrs; ?>>
                    <?= $THEME->render_lazyload_image([ 'image' => $item_image ]); ?>
                  </div>
                <?php endif; ?>

                <?php if ( $item_heading ) : ?>
                  <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                  <strong class="<?= $snippet_name; ?>__item-heading heading--primary heading--4" <?= $aos_attrs; ?>><?= $item_heading; ?></strong>
                <?php endif; ?>

                <?php if ( $item_message ) : ?>
                  <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-left' ]); $item_aos_delay += $item_aos_increment; ?>
                  <div class="<?= $snippet_name; ?>__item-message body-copy--primary body-copy--xs" <?= $aos_attrs; ?>><?= $item_message; ?></div>
                <?php endif; ?>

              </div>
            <?php endif; ?>

          <?php endforeach; ?>

        </div>
      <?php endif; ?>

    </div>
  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>
