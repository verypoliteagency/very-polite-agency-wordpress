<?php

/*
*
*	Filename: layout--media-row.php
*
*/



$args = [
  'inset' => get_sub_field( 'inset' ),
  'media_row' => get_sub_field( 'media_row' ),
];

echo $THEME->render_media_row( $args );

?>
