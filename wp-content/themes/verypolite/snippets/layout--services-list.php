<?php

  /*
  *
  *	Filename: layout--services-list.php
  *
  */

 // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'services-list';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- Content (ACF)
  $acf_heading = get_sub_field( 'heading' ) ?: '';
  $acf_list = get_sub_field( 'list' ) ?: [];
  $acf_list_count = count(array_filter( $acf_list, function( $item ) {
    return '' !== ($item['item'] ?? '');
  }));

?>

<style>

  @media ( min-width: 576px ) {
    #<?= $snippet_id; ?> .<?= $snippet_name; ?>__list {
      grid-template-rows: repeat(<?= ceil($acf_list_count/2); ?>, 1fr);
      grid-auto-flow: column;
      grid-template-columns: repeat(1, 50%);
    }
  }

</style>

<div class="<?= $snippet_name; ?>" id="<?= $snippet_id; ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', 'container' ); ?>
    <div class="<?= $snippet_name; ?>__main">

      <?php if ( $acf_heading ) : ?>
        <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay += $aos_increment; ?>
        <h2 class="<?= $snippet_name; ?>__heading heading--primary heading--4" <?= $aos_attrs; ?>><?= $acf_heading; ?></h2>
      <?php endif; ?>

      <?php if ( !empty($acf_list) ) : ?>
        <div class="<?= $snippet_name; ?>__list body-copy--primary body-copy--xs" data-count="<?= $acf_list_count; ?>">
          <?php foreach ( $acf_list as $i => $item ) : ?>

            <?php
              $item = $item['item'] ?? '';
              $item_aos_id = "{$aos_id}--{$i}";
              $item_aos_delay = $aos_delay + ($i * 150);
              $item_aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $item_aos_id, 'delay' => $item_aos_delay, 'transition' => 'fade-right' ]);
            ?>

            <?php if ( $item ) : ?>
              <span class="<?= $snippet_name; ?>__list-item" id="<?= $item_aos_id; ?>" <?= $item_aos_attrs; ?>><?= $item; ?></span>
            <?php endif; ?>

          <?php endforeach; ?>
        </div>
      <?php endif; ?>

      <div class="<?= $snippet_name; ?>__extras d-none d-lg-inline-flex">
        <div class="<?= $snippet_name; ?>__extras-image">
          <img loading="lazy" src="<?= $THEME->get_theme_directory('assets'); ?>/img/VPA--about-us--services-list-worker.svg" alt="Very Polite Agency Services Worker" width="171" height="101" />
        </div>
      </div>

    </div>
  <?= $THEME->render_bs_container( 'closed' ); ?>
</div>
