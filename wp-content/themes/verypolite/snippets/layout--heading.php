<?php

/*
*
*	Filename: layout--heading.php
*
*/

$THEME = $THEME ?? new CustomTheme();

$args = [
  'heading' => get_sub_field( 'heading' ),
];

echo $THEME->render_heading( $args );

?>
