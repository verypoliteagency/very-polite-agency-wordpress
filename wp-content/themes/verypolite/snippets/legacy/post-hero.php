<?php

/*
*
*	Filename: post-hero.php
*
*/

// ---------------------------------------- Theme Data
$THEME = $THEME ?? new CustomTheme();

// ---------------------------------------- Posts Data
$id = get_queried_object_id() ?: 0;
$cats = $THEME->render_categories_by_id([ 'id' ]) ?? '';
$tags = $THEME->render_tags_by_id([ 'id' ]) ?? [];

// ---------------------------------------- ACF Data
$intro = get_field( 'intro', $id ) ?? [];
$intro_image = $intro['image_' . $intro['type'] ?? ''] ?? '';
$note = get_field( 'note', $id ) ?? '';

?>

<div class="post__hero body-copy--primary body-copy--xs">
  <?= $THEME->render_bs_container( 'open', 'col-12', 'container' ); ?>

    <?php if ( get_the_title() ) : ?>
      <h1 class="post__title heading--primary heading--title"><?= get_the_title(); ?></h1>
    <?php endif; ?>

    <?php if ( false ) : ?>
      <div class="post__categories"><?= $cats; ?></div>
    <?php endif; ?>

    <?php if ( $note ) : ?>
      <div class="post__note"><p><?= $note; ?></p></div>
    <?php else : ?>
      <?php if ( $tags ) : ?>
        <div class="post__tags"><?= $tags; ?></div>
      <?php endif; ?>
    <?php endif; ?>

    <?php if ( !empty($intro) ) : ?>
      <?php
        $type = $intro['type'] ?? 'not-set';
        $image = '';
        switch ( $type ) {
          case 'animated': {
            $image = $intro['image_animated'] ?? '';
            break;
          }
          case 'static': {
            $image = $intro['image_static'] ?? '';
            $image = $THEME->render_lazyload_image([ 'image' => $image ]);
            break;
          }
        }
      ?>
      <?php if ( $image ) : ?>
        <div class="post__intro-graphic post__intro-graphic--<?= $type; ?>">
          <?= $image; ?>
        </div>
      <?php endif; ?>
    <?php endif; ?>

  <?= $THEME->render_bs_container( 'closed', 'col-12', 'container' ); ?>
</div>

