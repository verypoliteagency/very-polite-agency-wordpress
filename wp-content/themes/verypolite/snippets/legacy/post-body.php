<?php

/*
*
*	Filename: post-body.php
*
*/

// ---------------------------------------- Theme Data
$THEME = $THEME ?? new CustomTheme();

// ---------------------------------------- Posts Data
$id = get_queried_object_id() ?: 0;

?>

<div class="post__body">
  <?php include( locate_template( "./snippets/layout--flexible-post.php" ) ); ?>
</div>
