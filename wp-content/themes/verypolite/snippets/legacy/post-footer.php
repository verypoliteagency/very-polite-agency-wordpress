<?php

  /*
  *
  *	Filename: post-footer.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'masthead';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 150;
  $aos_increment = 150;

  // ---------------------------------------- ACF Data
  $masthead = get_field( 'masthead', $id ) ?? [];
  $masthead_contributors = $masthead['contributors_wysiwyg'] ?? '';
  $masthead_heading = $masthead['heading'] ?? '';
  $masthead_special_thanks = $masthead['special_thanks'] ?? '';

?>

<div class="post__footer" id="<?= $snippet_id; ?>">

  <?php if ( $masthead_contributors || $masthead_heading || $masthead_special_thanks ) : ?>
    <div class="post__masthead masthead body-copy--primary body-copy--xs">
      <?= $THEME->render_bs_container( 'open', 'col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2', 'container' ); ?>

        <?php if ( $masthead_heading ) : ?>
          <?php
            $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, "offset" => 0, 'transition' => 'fade-up' ]);
            $aos_delay += $aos_increment;
          ?>
          <strong class="masthead__heading font-weight--400" <?= $aos_attrs; ?>><?= $masthead_heading; ?></strong>
        <?php endif; ?>

        <?php if ( $masthead_contributors ) : ?>
          <?php
            $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, "offset" => 0, 'transition' => 'fade-up' ]);
            $aos_delay += $aos_increment;
          ?>
          <div class="masthead__contributors" <?= $aos_attrs; ?>><?= $masthead_contributors; ?></div>
        <?php endif; ?>

        <?php if ( $masthead_special_thanks ) : ?>
          <?php
            $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, "offset" => 0, 'transition' => 'fade-left' ]);
          ?>
          <div class="masthead__special-thanks" <?= $aos_attrs; ?>>
            <p>Thanks: <?= $masthead_special_thanks; ?></p>
          </div>
        <?php endif; ?>

      <?= $THEME->render_bs_container( 'closed', 'col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2', 'container' ); ?>
    </div>
  <?php endif; ?>

</div>
