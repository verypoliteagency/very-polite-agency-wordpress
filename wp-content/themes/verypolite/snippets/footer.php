<?php

  /**
  *
  *	Filename: footer.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'footer';
  $template_id = $template;
  $is_frontpage = is_front_page();

  // ---------------------------------------- ACF Data
  $acf_company = get_field( 'company', 'options' ) ?: [];
  $acf_company_email = $acf_company['email'] ?? '';
  $name = $acf_company['name'] ?? '';
  $summary = $acf_company['summary'] ?? '';
  $locations = $acf_company['locations'] ?? [];
  $locations_count = count(array_filter( $locations, function( $item ) {
    return 'publish' === $item['location']->post_status;
  }));

?>

<footer class="<?= $template; ?><?php if ( $is_frontpage ) : ?> footer--front-page<?php endif; ?>" id="<?php echo esc_attr($template_id); ?>">

  <?php if ( $is_frontpage ) : ?>
    <div class="<?= $template; ?>__front-page body-copy--primary" data-grid-columns-desktop="2">

      <div class="<?= $template; ?>__front-page-projects">
        <span class="<?= $template; ?>__front-page-tagline d-none d-lg-block">Very Polite Agency Inc.</span>
        <span class="<?= $template; ?>__front-page-project-title"></span>
      </div>

      <?php if ( $summary ) : ?>
        <div class="<?= $template; ?>__front-page-summary d-lg-none">
          <?= $summary; ?>
          <p>Thank you very much.</p>
        </div>
      <?php endif; ?>

      <div class="<?= $template; ?>__front-page-icon-set">
        <img src="<?= $THEME->get_theme_directory('assets'); ?>/img/VPA--brand.supporting-graphics.black.svg" width="306" height="30" alt="Very Polite Icon Set" loading="lazy"/>
      </div>

    </div>
  <?php else : ?>
    <div class="<?= $template; ?>__main body-copy--primary" data-grid-columns-desktop="<?= 1 + $locations_count; ?>">

      <?php if ( $locations ) : ?>
        <?php foreach ( $locations as $i => $item ) : ?>
          <?php if ( 'publish' === $item['location']->post_status ) : ?>

            <?php
              $location_id = $item['location']->ID ?? 0;
              $location_address = get_field( 'address', $location_id ) ?: [];
              $location_address_human = $THEME->render_address( $location_address, 'human' );
              $location_address_directions = $THEME->render_address( $location_address, 'directions' );
              $location_phone = get_field( 'phone', $location_id ) ?: '';
            ?>

            <div class="<?= $template; ?>__main-location">
              <?php if ( $location_address_human ) : ?>
                <span class="<?= $template; ?>__main-location-address">
                  <a class="<?= $template; ?>__main-link link" href="<?= $location_address_directions; ?>" target="_blank" title="Find Us"><?= $location_address_human; ?></a>
                </span>
              <?php endif; ?>
              <?php if ( $location_phone ) : ?>
                <span class="<?= $template; ?>__main-location-phone">
                  <a class="<?= $template; ?>__main-link link" href="tel:<?= $location_phone; ?>" target="_self" title="Call Us"><?= $location_phone; ?></a>
                </span>
              <?php endif; ?>
            </div>

          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>

      <div class="<?= $template; ?>__main-contact">
        <?php if ( $acf_company_email ) : ?>
          <span class="<?= $template; ?>__main-contact-email">
            <a class="<?= $template; ?>__main-link link" href="mailto:<?= $acf_company_email; ?>" target="_self" title="Email Us"><?= $acf_company_email; ?></a>
          </span>
        <?php endif; ?>
        <span class="<?= $template; ?>__main-contact-copyright">&copy <?= date( 'Y' ); ?> <?= get_bloginfo( 'name' ); ?></span>
        <span class="<?= $template; ?>__main-contact-tagline">All The Best™</span>
      </div>

    </div>
  <?php endif; ?>

</footer>
