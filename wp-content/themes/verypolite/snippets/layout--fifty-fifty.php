<?php

  /*
  *
  *	Filename: layout--fifty-fifty.php
  *
  */

  // ---------------------------------------- Theme
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Snippet
  $snippet_name = 'fifty-fifty';
  $snippet_id = $THEME->get_unique_id("{$snippet_name}--");

  // ---------------------------------------- AOS
  $aos_id = $snippet_id;
  $aos_delay = 250;
  $aos_increment = 250;

  // ---------------------------------------- Content (ACF)
  $caption = get_sub_field("caption") ?: '';
  $container = get_sub_field("full_bleed") ? "full-width" : "container";
  $cta = get_sub_field("cta") ?: [];
  $cta_link_classes = "{$snippet_name}__content-cta-link";
  $cta_link_classes .= " button--" . $cta["style"] ?? "primary";
  $cta_link_classes .= " button--" . $cta["theme_colour"] ?? "black";
  $cta_link_title = $cta["title"] ?? '';
  $cta_link_url = $THEME->get_link($cta);
  $full_bleed = get_sub_field("full_bleed") ?: false;
  $grid_style = "grid";
  $image = get_sub_field("image") ?: [];
  $icon = get_sub_field("icon") ?: [];
  $message = get_sub_field("message") ?: '';

  // ---------------------------------------- Conditionals
  $grid_style .= !empty($image) && $message ? " grid--2" : "";
  $grid_style .= " grid--" . ( get_sub_field("justification") ?: "media-left" );
  $grid_style .= " grid--align-" . ( get_sub_field("vertical_alignment") ?: "center" );

?>

<div class="<?= esc_attr( $snippet_name ); ?>" id="<?= esc_attr( $snippet_id ); ?>">
  <?= $THEME->render_bs_container( 'open', 'col-12 col-xl-10 offset-xl-1', $container ); ?>
    <div class="<?= $snippet_name; ?>__main <?= $grid_style; ?>">

      <?php if ( !empty($image) ) : ?>
        <div class="<?= $snippet_name; ?>__item grid__item grid__item--media">
          <div class="<?= $snippet_name; ?>__media">
            <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay = $aos_delay + $aos_increment; ?>
            <div class="<?= $snippet_name; ?>__media-image" <?= $aos_attrs; ?>>
              <?= $THEME->render_lazyload_image([ 'image' => $image ]); ?>
            </div>
            <?php if ( $caption ) : ?>
              <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); $aos_delay = $aos_delay + $aos_increment; ?>
              <div class="<?= $snippet_name; ?>__media-caption caption--primary caption--xs" <?= $aos_attrs; ?>><p><?= $caption; ?></p></div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>

      <?php if ( $message ) : ?>
        <div class="<?= $snippet_name; ?>__item grid__item grid__item--content">
          <div class="<?= $snippet_name; ?>__content">

            <?php if ( !empty($icon) ) : ?>
              <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]); $aos_delay += $aos_increment; ?>
              <div class="<?= $snippet_name; ?>__content-icon" <?= $aos_attrs; ?>><?= $THEME->render_lazyload_image([ 'image' => $icon ]); ?></div>
            <?php endif; ?>

            <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-down' ]); $aos_delay += $aos_increment; ?>
            <div class="<?= $snippet_name; ?>__content-message body-copy--primary body-copy--xs" <?= $aos_attrs; ?>><?= $message; ?></div>

            <?php if ( $cta_link_title && $cta_link_url ) : ?>
              <?php $aos_attrs = $THEME->render_aos_attrs([ 'anchor' => $aos_id, 'delay' => $aos_delay, 'transition' => 'fade-left' ]); ?>
              <div class="<?= $snippet_name; ?>__content-cta" <?= $aos_attrs; ?>>
                <?= $THEME->render_nu_link([ 'classes' => $cta_link_classes, 'title' => $cta_link_title, 'url' => $cta_link_url ]); ?>
              </div>
            <?php endif; ?>

          </div>
        </div>
      <?php endif; ?>

    </div>
  <?= $THEME->render_bs_container( 'closed', 'col-12 col-xl-10 offset-xl-1', $container ); ?>
</div>
