<?php

  //////////////////////////////////////////////////////////
  ////  Theme Vars
  //////////////////////////////////////////////////////////

  $THEME = $THEME ?? new CustomTheme();
  $home = $THEME->get_theme_directory('home');
  $assets_dir = $THEME->get_theme_directory('assets');

  $radio_not_checked = $assets_dir . '/img/VPA--not-checked.svg';
  $radio_checked = $assets_dir . '/img/VPA--checked.svg';

?>

<form
  action="https://formspree.io/xjvaoyde"
  class="form form--with-uploads js--validate-me"
  data-form-type="brand-me"
  id="form--brand-me"
  enctype="multipart/form-data"
  method="POST"
>

  <div class="form__loading">

    <?php

      // defaults
      $spinner = $spinner_type = false;

      if ( have_rows( 'forms', 'options' ) ) {
        while( have_rows( 'forms', 'options' ) ) {

          // init data
          the_row();

          // get data
          if ( get_sub_field( 'spinner_type' ) ) {

            $spinner_type = get_sub_field( 'spinner_type' );

            switch ( $spinner_type ) {
              case "animated":
                if ( get_sub_field( 'spinner_animated' ) ) {
                  $spinner = get_sub_field( 'spinner_animated' );
                  echo '<div class="form__spinner form__spinner--animated">';
                    echo $spinner;
                  echo '</div>';
                }
                break;
              case "image":
                if ( get_sub_field( 'spinner_image' ) ) {
                  $spinner = get_sub_field( 'spinner_image' );
                  echo '<div class="form__spinner form__spinner--image">';
                    echo $spinner;
                  echo '</div>';
                }
                break;
            }

          }

        }
      }

    ?>

  </div>
  <!-- /.form__loading -->

  <div class="form__content">
    <div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1">

      <div class="form__intro">
        <h2 class="section__heading">Entry Form</h2>
        <div class="section__image" data-node="form">
          <img src="<?php echo $assets_dir; ?>/img/brand-me/VPA--web-assets--brand-me--form.svg" width="1680" height="126" alt="Brand Me Guy Sleeping" loading="lazy" />
        </div>
      </div>
      <!-- /.form__intro -->

      <div class="form__header">
        <div class="form__column" data-count="1">
          <span class="form__column-row">
            <span class="form__label-pattern"></span>
            <span>Data Entry</span>
          </span>
          <span class="form__column-row">
            <span class="form__label-pattern"></span>
            <span>Brand Me, Please</span>
          </span>
        </div>
        <div class="form__column" data-count="2">
          <span>Please Fill Out The Following Form To The Best Of Your Abilities</span>
        </div>
        <div class="form__column" data-count="3">
          <span>
            <img src='<?php echo $assets_dir; ?>/img/brand/VPA--brand--VP--black.svg' alt='VP Lettermark' />
          </span>
        </div>
        <div class="form__column" data-count="4">
          <span>For Official Use Only</span>
        </div>
      </div>
      <!-- /.form__header -->

      <div class="form__body">

    	  <div class="form__field form__field--input">
      	  <label class="form__label">Company</label>
      	  <input class="form__input required" type="text" name="company" rules="string" />
    	  </div>
    	  <!-- /.form__field -->

    	  <?php

          $fields_radio = array(
            'type' => 'checkbox',
            'label' => 'Industry',
            'name' => 'industry',
            'options' => array(
              'Apparel',
              'Product',
              'Hospitality',
              'Service',
              'Other'
            )
          );

          if ( isset( $fields_radio ) && !empty( $fields_radio ) ) {

            $type = $label = $name = $options = false;

            if ( isset( $fields_radio['type'] ) && !empty( $fields_radio['type'] ) ) {
              $type = $fields_radio['type'];
            }
            if ( isset( $fields_radio['label'] ) && !empty( $fields_radio['label'] ) ) {
              $label = $fields_radio['label'];
            }
            if ( isset( $fields_radio['name'] ) && !empty( $fields_radio['name'] ) ) {
              $name = $fields_radio['name'];
            }
            if ( isset( $fields_radio['options'] ) && !empty( $fields_radio['options'] ) ) {
              $options = $fields_radio['options'];
            }

            echo '<div class="form__field form__field--' . $type . '">';

              echo '<label class="form__label form__label--heading">' . $label . '</label>';

              foreach ( $options as $key => $value ) {

                echo '<label class="form__label form__label--' . $type . '">';
            	    echo '<input class="form__input form__input--' . $type . '" type="' . $type . '" name="' . $name . '" value="' . $value . '" tabindex="1">';
                    echo '<span class="' . $type . '">';
            	        echo '<span class="' . $type . '__box">';
                        echo '<span class="' . $type . '__image ' . $type . '__image--checked"><img src=' . $radio_checked . '></span>';
                        echo '<span class="' . $type . '__image ' . $type . '__image--not-checked"><img src=' . $radio_not_checked . '></span>';
                      echo '</span>';
                    echo ' <span class="' . $type . '__title">' . $value . '</span>';
            	     echo '</span>';
                 echo '</label>';

              }

            echo '</div>';
            echo '<!-- /.form__field -->';

          }

        ?>

      	<div class="form__field form__field--textarea">
        	<label class="form__label">
        	  <span class="form__label-pattern"></span>
        	  <span>Tell us a bit about your idea</span>
        	  <span>1000 Char. Max</span>
        	 </label>
      	  <textarea class="form__textarea required" name="idea" placeholder="" maxlength="1000"></textarea>
        </div>
        <!-- /.form__field -->

        <div class="form__field form__field--textarea">
        	<label class="form__label">
        	  <span class="form__label-pattern"></span>
        	  <span>Why do you think you deserve to be branded?</span>
        	  <span>1000 Char. Max</span>
        	 </label>
      	  <textarea class="form__textarea required" name="justification" placeholder="" maxlength="1000"></textarea>
        </div>
        <!-- /.form__field -->

        <div class="form__field form__field--input form__field--name">
      	  <label class="form__label">Name</label>
      	  <input class="form__input required" type="text" name="name" />
    	  </div>
    	  <!-- /.form__field -->

    	  <div class="form__field form__field--input form__field--phone">
      	  <label class="form__label">Phone</label>
      	  <input class="form__input required" type="tel" name="phone" />
    	  </div>
    	  <!-- /.form__field -->

        <div class="form__field form__field--input form__field--email">
      	  <label class="form__label">Email</label>
      	  <input class="form__input required" type="email" name="email" />
    	  </div>
    	  <!-- /.form__field -->

    	  <?php

          $fields_radio = array(
            'type' => 'checkbox',
            'label' => 'Country',
            'name' => 'country',
            'options' => array(
              'Canada',
              'USA',
              'Europe',
              'Asia',
              'Australia',
              'Other'
            )
          );

          if ( isset( $fields_radio ) && !empty( $fields_radio ) ) {

            $type = $label = $name = $options = false;

            if ( isset( $fields_radio['type'] ) && !empty( $fields_radio['type'] ) ) {
              $type = $fields_radio['type'];
            }
            if ( isset( $fields_radio['label'] ) && !empty( $fields_radio['label'] ) ) {
              $label = $fields_radio['label'];
            }
            if ( isset( $fields_radio['name'] ) && !empty( $fields_radio['name'] ) ) {
              $name = $fields_radio['name'];
            }
            if ( isset( $fields_radio['options'] ) && !empty( $fields_radio['options'] ) ) {
              $options = $fields_radio['options'];
            }

            echo '<div class="form__field form__field--' . $type . '">';

              echo '<label class="form__label form__label--heading">' . $label . '</label>';

              foreach ( $options as $key => $value ) {

                echo '<label class="form__label form__label--' . $type . '">';
            	    echo '<input class="form__input form__input--' . $type . '" type="' . $type . '" name="' . $name . '" value="' . $value . '" tabindex="1">';
                    echo '<span class="' . $type . '">';
            	        echo '<span class="' . $type . '__box">';
                        echo '<span class="' . $type . '__image ' . $type . '__image--checked"><img src=' . $radio_checked . '></span>';
                        echo '<span class="' . $type . '__image ' . $type . '__image--not-checked"><img src=' . $radio_not_checked . '></span>';
                      echo '</span>';
                    echo ' <span class="' . $type . '__title">' . $value . '</span>';
                  echo '</span>';
                echo '</label>';

              }

            echo '</div>';
            echo '<!-- /.form__field -->';

          }

        ?>

        <div class="form__field form__field--file">
          <label class="form__label form__label--heading">
            <span class="form__label-pattern"></span>
            <span>File Upload</span>
            <span>10mb Max</span>
          </label>
          <label class="form__label form__label--note">
            <span class="form__label-pattern"></span>
            <span>Please Upload Additional Information (One File Only, Image Or PDF) About Your Project</span>
          </label>
          <div class="form__upload">

            <input class="form__input form__input--file" type="file" name="file" id="file" accept="image/*,.pdf">

            <div class="form__message">
              <span class="default">Drag and Drop File Here</span>
              <span class="summary"><!-- Inject JS Summary Here --></span>
              <span class="error error--too-many-files">Only One File Allowed for Upload!</span>
              <span class="error error--file-size">File must be 10mb or less!</span>
            </div>

          </div>
        </div>
        <!-- /.form__field -->

        <div class="form__field form__field--submit">
          <?php if ( false ) : ?>
            <button class="form__button button button--fill button--primary" type="submit" form="form--brand-me" formenctype="multipart/form-data">Submit</button>
          <?php endif; ?>
        </div>
        <!-- /.form__field -->

        <div class="form__message">
          <span class="success" style="display: block;">
            <div class="success__top">
              <span class="form__label-pattern"></span>
              <span>Thank You Very Much</span>
            </div>
            <div class="success__bottom">
              <span class="form__label-pattern"></span>
              <span>Submission deadline is September 6th, 2020</span>
            </div>
          </span>
          <span class="error error--form-overview">Sorry! There are missing or invalid fields highlighted above.</span>
        </div>

      </div>
      <!-- /.form__body -->

    </div></div></div>
  </div>
  <!-- /.form__content -->

  <div class="form__thank-you">
    <div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1">
      <h2 class="section__heading">Thank You!</h2>
    </div></div></div>
  </div>
  <!-- /.form__thank-you -->

</form>
