<?php

/*
*
*	Filename: layout--flexible-page.php
*
*/

if ( have_rows( "flex_page" ) ) {

  echo '<div class="flexible-content flexible-content--page">';

    while ( have_rows( "flex_page" ) ) {

      the_row();

      $row_index = get_row_index();
      $row_layout = get_row_layout();

      echo '<section class="section section--' . $row_layout . '" data-row-layout="' . $row_layout . '" data-row-index="' . $row_index . '">';

        switch ( $row_layout ) {
          case 'contact-us': {
            include( locate_template( "./snippets/layout--contact-us.php" ) );
            break;
          }
          case 'fifty-fifty': {
            include( locate_template( "./snippets/layout--fifty-fifty.php" ) );
            break;
          }
          case 'services': {
            include( locate_template( "./snippets/layout--services.php" ) );
            break;
          }
          case 'services-list': {
            include( locate_template( "./snippets/layout--services-list.php" ) );
            break;
          }
          case 'testimonials': {
            include( locate_template( "./snippets/layout--testimonials.php" ) );
            break;
          }
          default: {
            echo "<h2>Oops! It looks like there's no template for this content yet :(</h2>";
            break;
          }
        }

      echo '</section>';

    }

  echo '</div>';

}

?>
