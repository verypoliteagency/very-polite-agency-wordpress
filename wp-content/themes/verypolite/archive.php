<?php
	
/*
*	
*	Filename: archive.php
*
*/

get_header();
 
//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();

?>

<div id="archive" class="archive" role="main">
	
	<?php if ( have_posts() ) : ?>	
		<?php while ( have_posts() ) : the_post(); ?>
		
			<article>
				
				<div class="article__header">	
					<?php if ( get_the_title() ) : ?>				
						<h2 class="headline headline--beta"><?php the_title(); ?></h2>
					<?php endif; ?>
				</div>
				<!-- /.article__header -->
					
				<div class="article__main">
					<?php if ( get_the_excerpt() ) : ?>
						<div class="exceprt rte">
							<?php the_excerpt(); ?>	
						</div>
					<?php endif; ?>
				</div>
				<!-- /.article__main -->
					
				<div class="article__footer"></div>
				<!-- /.article__footer -->
					
			</article>
		
		<?php endwhile; ?>
	<?php else : ?>
		<!-- NO EVENT POSTS!!! -->
	<?php endif; wp_reset_postdata(); ?>
				
</div>
<!-- /#archive -->

<?php get_footer(); ?>
