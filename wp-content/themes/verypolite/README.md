# We Are Very Polite | WordPress

## Site URL
https://weareverypolite.com/
https://vpolitedev.wpengine.com/

## General/On-Going
- [ ] Remove Axios
- [ ] Minimize page templates down to one template
- [ ] Fix/Update button--outlined styles
- [ ] Add render_media_grid_item to theme class
- [x] Integrate Gutenberg blocks with ACF to theme
- [x] Convert current posts to 'work' custom post type
- [x] Refactor JavaScript where possible
- [ ] Remove unused render classes from theme class
- [ ] Reduce overall site payload
- [ ] Rebuild SCSS structure for WordPress site to then be shared with Boilerplate
- [x] Add hero block to ACF blocks
- [x] Add masthead block to ACF blocks
- [x] Add layout options for longevity club
- [x] Add masonry grid block to ACF blocks
- [x] Remove jQuery

## Job Updates, 2023
### Todo
- [x] Refactored JS to ESM
- [x] Update to the Careers page
- [x] Addition of Graphic Designer job posting

## Amazon Update November, 2022
### Todo
- [x] Add links
- [x] Add, commit, push changes
