<?php
	
/*
*	
*	Filename: tag.php
*
*/

get_header();
 
//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
$tag = get_queried_object();

?>

<div id="tag" class="tag tag--<?php echo $tag->slug; ?>" role="main">
  
  
  <div class="section section--404">
		
		<div class="section__main">
			
			<div class="container"><div class="row"><div class="col-12">
		
				<h1>Error 404</h1>
				<h2>Page or Content Not Found.</h2>
				<p>Let us take you <a href="<?php echo $home; ?>">home</a>.</p>
			
			</div></div></div>
			<!-- /.wrapper .row .col -->
		
		</div>
		<!-- /.section__main -->
		
	</div>
	<!-- /.section--404 -->
  
	<?php 
  	
  	if ( false ) {
  	
    	if ( have_posts() ) {
      	
      	while ( have_posts() ) {
        
          the_post();
          
          echo '<article>';
  				
    				echo '<div class="article__header">';
    				  if ( get_the_title() ) {
      				  echo '<h2 class="headline headline--beta">' . get_the_title() . '</h2>';
    				  }			
    				echo '</div>';
    				echo '<!-- /.article__header -->';
    					
    				echo '<div class="article__main">';
    				  if ( get_the_excerpt() ) {
      				  echo '<div class="exceprt rte">';
    							echo get_the_excerpt();
    						echo '</div>';
    				  }
    				echo '</div>';
    		  					
    			echo '</article>';
          
        }
      	
    	}
  	
  	}
  	  	
  ?>
				
</div>
<!-- /#tag -->

<?php get_footer(); ?>
