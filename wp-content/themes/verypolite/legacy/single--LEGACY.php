<?php

/*
*
*	Filename: single.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Theme Vars
//////////////////////////////////////////////////////////

$THEME = $THEME ?? new CustomTheme();
$home = $THEME->get_theme_directory('home');
$assets_dir = $THEME->get_theme_directory('assets');
$theme_dir = $THEME->get_theme_directory();
$theme_classes = $THEME->get_theme_classes();
$post_type = $THEME->get_theme_info('post_type');
$post_ID = $THEME->get_theme_info('post_ID');

$carousel_nav_next = $assets_dir . '/img/ui/VPA--icon--carousel-next--white.svg';

?>

<div id="post" class="post post--<?php echo $post_type; ?>">

  <?php include( locate_template( "./snippets/post--hero.php" ) ); ?>

  <div class="post__body">

    <?php

      //////////////////////////////////////////////////////////
  		////  Flexible Content
  		//////////////////////////////////////////////////////////

      include( locate_template( "./snippets/layout--flexible-post.php" ) );

    ?>

  </div>
  <!-- /.post__body -->

  <div class="post__footer">

    <?php

      //////////////////////////////////////////////////////////
  		////  Masthead
  		//////////////////////////////////////////////////////////

      if ( have_rows( 'masthead' ) ) {

        echo '<div class="post__masthead">';
          echo '<div class="container"><div class="row"><div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">';

            while ( have_rows( 'masthead' ) ) {

              // init data
              the_row();

              // default data
              $heading = $special_thanks = false;
              $contributors_wysiwyg = false;

              if ( get_sub_field( 'heading' ) ) {
                $heading = get_sub_field( 'heading' );
              }
              if ( get_sub_field( 'special_thanks' ) ) {
                $special_thanks = get_sub_field( 'special_thanks' );
              }
              if ( get_sub_field( 'contributors_wysiwyg' ) ) {
                $contributors_wysiwyg = get_sub_field( 'contributors_wysiwyg' );
              }

              if ( $contributors_wysiwyg ) {

                echo '<div class="contributors">';

                  if ( $heading ) {
                    echo '<h3 class="contributors__heading masthead__heading">' . $heading . '</h3>';
                  }

                  echo '<div class="contributors__main message rte">';
                    echo $contributors_wysiwyg;
                  echo '</div>';

                  if ( $special_thanks ) {
                    echo '<div class="contributors__special-thanks message rte">';
                      echo '<p>Thanks: ' . $special_thanks . '</p>';
                    echo '</div>';
                  }

                echo '</div>';
                echo '<!-- /.contributors -->';

              }

            }

          echo '</div></div></div>';
          echo '<!-- /.wrapper .row .col -->';
        echo '</div>';
        echo '<!-- /.post__masthead -->';

      }

    ?>

  </div>
  <!-- /.post__footer -->

</div>
<!-- /#post.<?php echo $post_type; ?> -->

<?php get_footer(); ?>
