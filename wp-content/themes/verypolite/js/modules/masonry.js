const config = { debug: false, name: 'masonry.js', version: '1.0' };
const instances = [];
const update = { limit: 3, count: 0 };

const createMasonryFromElement = ( element = false ) => {
  if ( element ) {

    let elementContainer = element.closest(".masonry-grid") || false;
    let filterButtons = elementContainer ? elementContainer.querySelectorAll(`.js--masonry-filter-button`) : [];
    let intervalCount = 1;
    let iso = new Isotope( element, getOptions() );

    iso.layout();
    instances.push( iso );

    let refresher = () => {
      if ( intervalCount < 5 ) {
        iso.layout();
        intervalCount++;
      } else {
        clearInterval(interval);
      }
    };

    filterButtons.forEach((button) => {
      button.addEventListener("click", (event) => {

        filterButtons.forEach((button) => {
          button.classList.remove("is-selected");
        });

        button.classList.add("is-selected");

        let filter = button.dataset.filter || "";

        iso.arrange({
          filter: function( itemElem ) {
            if ( "all" === filter ) {
              return itemElem;
            }
            return itemElem.classList.contains(filter);
          }
        });

        iso.layout();

      });
    });

    let interval = setInterval(refresher, 300);

  }
};

const getOptions = ( custom = {} ) => {

  let standard = {
    layoutMode: 'masonry',
    itemSelector: '.masonry-grid__item',
    percentPosition: true,
    masonry: {
      columnWidth: '.masonry-grid__sizer',
      horizontalOrder: true,
      gutter: 0
    }
  };

  return { ...standard, ...custom };

};

const updateInstances = () => {
  if ( instances.length && (update.count < update.limit) ) {
    instances.forEach( instance => {
      instance.layout();
      update.count++;
    });
  }
};
//
//     let intervalCount = 1;
//     let iso = new Isotope( element, getOptions() );
//
//     let refresher = () => {
//       if ( intervalCount < 5 ) {
//         if ( config.debug ) console.log(`createMasonryFromElement refreshed ${intervalCount}`);
//         iso.layout();
//         intervalCount++;
//       } else {
//         clearInterval(interval);
//       }
//     };
//
//     let interval = setInterval(refresher, 500);

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    ( document.querySelectorAll( '.masonry-grid__container' ) || [] ).forEach( element => {
      createMasonryFromElement( element );
    });
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
}

export default { init, updateInstances };
