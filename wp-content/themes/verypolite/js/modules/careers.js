const config = { debug: false, name: 'careers.js', version: '1.0' };

const openJobFromHash = () => {

  let hashID = window.location.hash ? window.location.hash.substring(1) : '';

  if ( hashID ) {
    ( document.querySelectorAll('.job-postings__item .job-postings__body') || [] ).forEach( element => {
      let parentID = element.closest('.job-postings__item').id || '';
      let collapseEl = bootstrap.Collapse.getOrCreateInstance( element, { toggle: false } );
      if ( hashID === parentID ) {
        collapseEl.show();
      } else {
        collapseEl.hide();
      }
    });
  }

};

const jobPostingCollapseEventWatcher = () => {
  ( document.querySelectorAll( '.job-postings__body' ) || [] ).forEach( element => {
    element.addEventListener( 'show.bs.collapse', event => {
      element.closest('.job-postings__item').classList.add('show');
    });
    element.addEventListener( 'hidden.bs.collapse', event => {
      element.closest('.job-postings__item').classList.remove('show');
    });
  });
};

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    jobPostingCollapseEventWatcher();
    openJobFromHash();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
