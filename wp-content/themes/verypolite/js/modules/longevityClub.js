import Breakpoints from 'breakpoints';

const config = { debug: false, name: 'longevityClub.js', version: '1.0' };

const setPostDateWidth = () => {

  ( document.querySelectorAll('.js--post-date-published') || [] ).forEach( element => {
    let image = element.closest('.image') || false;
    if ( image ) {
      if ( window.innerWidth >= Breakpoints.sizes.lg ) {
        element.style.width = image.offsetHeight + 'px';
      } else {
        element.style.width = '100%';
      }
    }
  });

};

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    setPostDateWidth();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
