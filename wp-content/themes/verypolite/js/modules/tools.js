import Breakpoints from 'breakpoints';

const addClass = ( $class = '', $elements = [] ) => {
  if ( $class && $elements.length ) {
    for ( let i = 0; i < $elements.length; i++ ) {
      if ( $elements[i] ) {
        $elements[i].classList.add( $class );
      }
    }
  }
};

const debounce = (func, delay) => {
  let inDebounce;
  return function() {
    const context = this;
    const args = arguments;
    clearTimeout(inDebounce);
    inDebounce = setTimeout(() => func.apply(context, args), delay);
  };
};

const getArrayOfElementsByTag = ( $elements = [ 'body', 'footer', 'header', 'main' ] ) => {
  let filteredElements = $elements.filter( tag => { return document.getElementsByTagName( tag )[0] } ) || [];
  return filteredElements.map( tag => document.getElementsByTagName( tag )[0] ) || [];
};

const getElementHeightByTag = ( $tag = '' ) => {
  return document.getElementsByTagName( $tag )[0].offsetHeight || 0;
};

const getLocalStorageDataByKey = ( $key = '' ) => {
  return localStorage.getItem( $key );
};

const getTimeStamp = () => {
  let d = new Date();
  return d.getTime();
};

const isMobileDevice = () => {
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    return true;
  }
  return false;
};

const removeClass = ( $class = '', $elements = [] ) => {
  if ( $class && $elements.length ) {
    for ( let i = 0; i < $elements.length; i++ ) {
      if ( $elements[i] ) {
        $elements[i].classList.remove( $class );
      }
    }
  }
};

const setCSSVariable = ( $id = '', $value = '' ) => {
  if ( $id && $value ) document.documentElement.style.setProperty( '--' + $id, $value );
};

const setElementHeightWithHeaderAndFooterOffset = () => {

  let elements = document.querySelectorAll( '.js--auto-height' ) || [];

  if ( elements.length ) {

    let headerHeight = document.getElementById('header').offsetHeight;
    let footerHeight = document.getElementById('footer').offsetHeight;
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let heightOffset = windowWidth >= Breakpoints.sizes.lg ? headerHeight + footerHeight : headerHeight;

    elements.forEach( element => {
      element.style.height = windowHeight - heightOffset + 'px';
    });

  }

};

const setElementsHeightToCSSVariable = () => {

  [{ var_id: 'theme-header-height--total', element_id: 'shopify-section-header' }].forEach( item => {
    let { var_id, element_id } = item;
    let value = document.getElementById( element_id ) ? document.getElementById( element_id ).offsetHeight : 0;
    document.documentElement.style.setProperty( `--${var_id}`, `${value}px` );
  });

};

const setViewportHeightCSSVariable = () => {
  setCSSVariable('theme-viewport-height', window.innerHeight + 'px'  );
};

const setLocalStorageDataByKey = ( $key = '', $value = '' ) => {
  localStorage.setItem( $key, $value );
}

const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;
  return function() {
    const context = this;
    const args = arguments;
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function() {
        if ((Date.now() - lastRan) >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
};

const toggleClass = ( $class = '', $elements = [] ) => {
  if ( $class && $elements.length ) {
    for( let i = 0; i < $elements.length; i++ ) {
      if ( $elements[i] ) {
        $elements[i].classList.toggle( $class );
      }
    }
  }
};

export default {
  addClass,
  debounce,
  getArrayOfElementsByTag,
  getElementHeightByTag,
  getLocalStorageDataByKey,
  getTimeStamp,
  removeClass,
  setCSSVariable,
  setElementHeightWithHeaderAndFooterOffset,
  setElementsHeightToCSSVariable,
  setViewportHeightCSSVariable,
  setLocalStorageDataByKey,
  throttle,
  toggleClass
};
