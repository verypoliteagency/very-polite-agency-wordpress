const info = { company : 'Very Polite Agency', tagline : '"All the Best"',  version : '2.1.5' };

const init = () => {
  console.log( `${info.company} - ${info.tagline} - Version ${info.version}` );
  console.log( 'Site by Very Polite Agency – https://weareverypolite.com/' );
};

export default { init }

