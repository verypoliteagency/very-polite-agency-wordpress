const config = { debug: false, name: 'daysLeft.js', version: '1.0' };
const days = {
  difference: 0,
  used: 0,
  left: 0,
  max: {
    chip: 40000,
    real: 27375
  }
};
const elements = {
  container: '.days-left',
  submit: '.days-left .days-left__button--submit',
  reset: '.days-left .days-left__button--reset',
  calculator: '.days-left .days-left__calculator',
  result: '.days-left .days-left__result'
};
const today = new Date();

const calculateDaysLeft = () => {
  ( document.querySelectorAll( elements.container ) || [] ).forEach( container => {
    ( container.querySelectorAll( '.days-left__button-submit' ) || [] ).forEach( button => {
      button.addEventListener( 'click', event => {

        let birthday = {};
        let message = '';

        ( container.querySelectorAll( 'input' ) || [] ).forEach( input => {

          let value = input.value || false;
          let dateType = input.dataset.date || false;

          switch( dateType ) {
            case 'day': {
              birthday.day = sanitizeDate( value, 1, 31 );
              break;
            }
            case 'month': {
              birthday.month = sanitizeDate( value, 1, 12 );
              break;
            }
            case 'year': {
              birthday.year = sanitizeDate( value, 0, 9999 );
              break;
            }
          }

        });

        if ( birthday.day && birthday.month && birthday.year ) {

          birthday.date = new Date( birthday.year, birthday.month - 1, birthday.day );

          days.difference = today.getTime() - birthday.date.getTime();
          days.used = Math.ceil(days.difference / (1000 * 3600 * 24));
          days.left = days.max.chip - days.used;

          if ( days.left < 0 ) {
            message = `Congratulations on living to ${(days.used/365).toFixed(2)} years old!`;
          } else {
            message = `You have ${days.left} days left to live`;
          }

          ( container.querySelectorAll( '.days-left__result-heading' ) || [] ).forEach( heading => heading.innerHTML = message );

          if ( container.querySelector( '.days-left__calculator' ) ) {
            container.querySelector( '.days-left__calculator' ).style.display = 'none';
          }

          if ( container.querySelector( '.days-left__result' ) ) {
            container.querySelector( '.days-left__result' ).style.display = 'block';
          }

        }

      });
    });
  });
};

const resetDays = () => {
  days.difference = 0;
  days.used = 0;
  days.left = 0;
};

const resetDaysLeft = () => {
  ( document.querySelectorAll( elements.container ) || [] ).forEach( container => {
    ( container.querySelectorAll( '.days-left__button-reset' ) || [] ).forEach( button => {
      button.addEventListener( 'click', event => {

        resetDays();

        ( container.querySelectorAll( 'input' ) || [] ).forEach( input => input.value = '' );

        if ( container.querySelector( '.days-left__calculator' ) ) {
          container.querySelector( '.days-left__calculator' ).style.display = 'block';
        }

        if ( container.querySelector( '.days-left__result' ) ) {
          container.querySelector( '.days-left__result' ).style.display = 'none';
        }

      });
    });
  });
};

const sanitizeDate = ( value = 0, min = 1, max = 31 ) => {

  value = parseInt( value );

  if ( value >= min && value <= max ) {
    return value;
  }

  return false;

}

const init = () => {
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} initialized ]`);
    calculateDaysLeft();
    resetDaysLeft();
  if ( config.debug ) console.log(`[ ${config.name} v.${config.version} complete ]`);
};

export default { init };
