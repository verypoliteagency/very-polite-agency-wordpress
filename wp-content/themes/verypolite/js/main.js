import Careers from './modules/careers';
import Credits from './modules/credits';
import DaysLeft from './modules/daysLeft';
import Forms from './modules/forms';
import Gliders from './modules/gliders';
import LongevityClub from './modules/longevityClub';
import Masonry from './modules/masonry';
import PushMenu from './modules/pushMenu';
import Sizing from './modules/sizing';
import Tools from './modules/tools';

AOS.init();
Careers.init();
Credits.init();
DaysLeft.init();
Forms.init();
PushMenu.init();

window.addEventListener( 'load', function (e) {
  AOS.refresh();
  Gliders.init();
  LongevityClub.init();
  Masonry.init();
  Sizing.init();
  Tools.setElementHeightWithHeaderAndFooterOffset();
  Tools.setElementsHeightToCSSVariable();
});

window.addEventListener( 'resize', Tools.debounce(() => {
  LongevityClub.init();
  Sizing.init();
  Tools.setElementHeightWithHeaderAndFooterOffset();
  Tools.setElementsHeightToCSSVariable();
}, 250));

window.addEventListener( 'scroll', Tools.debounce(() => {
  Masonry.updateInstances();
}, 250));

window.addEventListener( 'resize', Tools.throttle(() => {
  Sizing.init();
}, 250));
