<?php

  /**
  *
  *	Filename: page.php
  *
  */

  // ---------------------------------------- Mount WP Header
  get_header();

  // ---------------------------------------- Data
  $THEME = $THEME ?? new CustomTheme();
  $id = get_queried_object_id() ?: 0;

  // ---------------------------------------- Template Data
  $template = 'page';
  $template_id = $THEME->get_unique_id("{$template}--");

  // ---------------------------------------- Template
  echo '<div class="' . $template . '" id="' . $template_id . '">';

    if ( have_posts() ) {
	    while ( have_posts() ) {

		    // init post content
		    the_post();

        // post content
        the_content();

	    }
    }

  echo '</div>';

  // ---------------------------------------- Mount WP Footer
  get_footer();

?>
